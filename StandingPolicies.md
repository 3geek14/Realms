---
title: 'Realms Standing Policies'
documentclass: book
geometry: margin=1in
pagestyle: headings
papersize: letter
fontsize: 12pt
toc-depth: 1
lang: en-us
...

# COVID Rules At Events # {#covid-rules-at-events}

Enacted on June 12th, 2021 \
Amended on February 17th, 2024 \
Lasts until further notice

## Definitions ## {#definitions}

For the purposes of this Standing Policy, "attendees" shall refer to any
in-person attendee of an event, including but not limited to EHs, staff,
players, and spectators. If the event is held in a location open to the public,
passing members of the public are not attendees unless they substantially engage
with regular attendees. Attendees are to keep clear of non-attendee individuals
to the best of their ability.

Additionally, "regulations" shall refer to any and all federal, state, county,
and local laws, regulations, policies, etc. relating to personal
health and safety in the COVID-19 pandemic, which are in effect when a gathering
takes place, and which are applicable to the nature and location(s) of the
event. This definition is expanded when referring to attendees traveling between
jurisdictions prior to, during, or after a gathering to include the relevant
regulations for travel in said jurisdictions and for the full duration of their
travel.

* If melee combat is involved, this includes regulations for "Higher Risk"
  sports and recreational activities.
* If food or drinks are involved, this includes regulations for food service.
* If indoor facilities are involved, this includes regulations for indoor
  gatherings and recreation.

Finally, "event protocols" shall refer to any procedures put in place at an
event to comply with relevant regulations and/or further ensure the health and
safety of attendees.

## For Events and Attendees ## {#for-events-and-attendees}

Every Realms Event must comply with regulations, at a bare minimum. The EH or
designated staff is personally responsible for ensuring compliance.

An EH is within their rights to enact and enforce stricter event protocols than
are required by regulations. Any event must have advertisements that list
or link to any event protocols by at least 2-weeks prior to the event.

Event protocols can be adjusted as needed by EHs up to 72 hours prior to the
event, as regulations change. EHs must state in their
event protocols whether they will enforce a one-warning or zero tolerance policy
for attendees who fail to comply with event protocols or this Standing Policy.

Each attendee is personally responsible for knowing and following all event
protocols for an event, and all travel regulations applicable to their
circumstances. In the case of a minor, their parent, guardian, or a designated
chaperone at the gathering is also personally responsible for their compliance.
Any failures to follow regulations and event protocols must be reported to an
EH or their designated staff member. With the conclusion
of the event, all such reports must be passed along to the CRB by the
gathering's EH.

When any attendee arrives on site, an EH for that event or their designated staff
member must verify the attendee understands and is equipped to follow all event
protocols. If so, the attendee will sign a form stating that they understand,
have followed, and will continue to follow all regulations and event protocols.
No attendee is allowed to participate in or spectate the event until said
form is signed. If any attendee is unable or unwilling to follow any required
regulations or event protocols, they are not allowed to attend the event and
must leave the site. Any such attendee's failure to leave the site promptly must
be reported to the CRB.

The following criteria each prevent someone from attending a Realms event:

* The person has tested positive for COVID-19 in the 5 days before the event and
  does not have 2 negative rapid antigen tests 24 hours apart after last testing
  positive.
* The person is required to self-quarantine or self-isolate during the event
  under local health guidelines or as directed by a medical professional.
* The person suspects they have COVID-19 based on any of the following symptoms
  prior to the event: fever (> 100.4F) or feeling feverish, new cough,
  difficulty breathing, excessive fatigue, unexpected muscle aches or body
  aches, new loss of smell or taste, sore throat, and diarrhea or vomiting.

# Backing Janna's Items for a Few Years # {#backing-janna-s-items-for-a-few-years}

Enacted on March 1st, 2025 \
Expires at the EHC which takes place in 2028

All magic items which were legally backed by Janna Oakfellow-Pushee in the calendar year 2024 will retain their legal backing until the EHC in 2028 while Janna is listed as the backing EH. If a marshal is needed for these items, such as if an item is being rebuilt or Reforged, the Minister of Magic Items will act as the relevant marshal.

# Temporary Trial of Modified Non-Com Rules # {#temporary-trial-of-modified-non-com-rules}

Enacted on March 1st, 2025 \
Expires at the EHC which takes place in 2026

At any time during an event, a player may declare themself to be a non-combatant. They do this by informing an EH or designated marshal and by putting on a player-provided yellow armband on each arm (or on their head, if their arms would otherwise be covered). Non-Combatant players may also choose to wear both an armband, headband and/or full body sash. Only players who declare themselves to be non-combatants may wear a yellow armband/headband. Once a player declares themself to be a non-combatant, they will stay a non-combatant until the end of an event (unless given approval by an EH). A player declaring themself to be a non-combatant takes on a few unique rights and responsibilities:

* First, and most importantly, never hit a non-combatant with a weapon. Intentionally doing so is cheating. Instead, to kill a non-combatant, you may simply point at them and say “die”, provided either you are wielding a melee weapon or a missile weapon and are within 10 unobstructed feet of them. "Unobstructed" in this sense means that you would reasonably be able to close the distance between you and the noncombatant in a normal combat scenario and strike them with a melee weapon, or believe that a ranged attack you make would go unblocked. This call can not be resisted unless the non-combatant uses a spell that would prevent a killing blow or by call of “Knight”.
  
  If a non-combatant is regenerating, you can point at them with a melee weapon and say “impale” and, while remaining within 10 feet of the body and wielding a melee weapon, they will be considered impaled. If you are “impaling” a non-combatant and use your weapon to fight someone else, the non-combatant will no longer be impaled. They may be rendered soulless as normal.
* Second, a non-combatant may never wield a weapon or shield and must take reasonable precautions to avoid looking like they are doing so. They may carry weapons and shields for others, but shall do so in such a manner that makes it clear that they are not being wielded. In addition, non-combatant spellcasters may not use the spells Magic Missile or Lightning Bolt.
* Third, a non-combatant shall do their best to stay out of the flow of combat. Should a non-combatant die, and combat is near to them, they may walk while dead to an OOC-safer place for them.
* Fourth, a non-combatant must make sure their yellow armband/headband is visible at all times. If it is not, they must remove themselves from any combat situations until they have a visible armband/headband.
* Finally, a non-combatant is encouraged, but not required, to tell the EH of an event the reason for them to be a non-combatant, if the non-combatant feels that the information should be known by a member of staff.

A yellow armband/headband must meet the following requirements:

* It must be a dayglo yellow or other similar fluorescent yellow color with the words “Non-Com” on it.
* It must be at least 2 inches wide. At night it still must be visible, including being lit up or glowing, to make sure it is seen.
* If a non-combatant’s arms are covered, a headband matching these criteria must be worn.