---
title: '2025 Realms Omnibus'
documentclass: book
geometry: margin=1in
pagestyle: headings
papersize: letter
fontsize: 12pt
toc-depth: 1
lang: en-us
...

# General Player Information # {#general-player-information}

## What is the Realms? ## {#what-is-the-realms}

The Realms is a medieval fantasy Live Action Role Playing (LARP) system. This
game has been in existence since the late 1980s. The Realms has evolved
considerably since its inception through the participation and imagination of
more people than can be credited. Unlike most LARPs today, the Realms has a
skill-based combat system, and is community-based rather than run by a single,
static council or corporation.

For those who are not familiar with what playing in a LARP entails, it is a lot
like acting. The difference is that while the event staff sets the stage, the
lines are all yours. You decide what "part" you want to play. Are you a mage, a
warrior, a noble, or all of the above? Once you arrive at the event site you
slip into your character. You stop acting like yourself and begin acting like
your new persona. The Realms uses foam-padded "weapons" to simulate combat and
has an intricate magic system to represent all sorts of things we wouldn't be
able to do in real life. The system is designed to help resolve situations that
we cannot practically resolve on our own. Specifics of the Realms system will be
discussed in more detail later on. You're in the Realms now and you'll never be
quite the same again.

## What is This Book? ## {#what-is-this-book}

The document you are reading is the official rulebook to the Realms. Covered
here you will find what you need to know about combat, magic, and other aspects
of playing the game.

Be sure to know the 2025 Omnibus rules, as well as any specific rules at each
event you attend. At their events, an Event Holder (a player who volunteers to
run an event, hereafter referred to as an EH) may choose to change some of the
rules, including (among other things) announcing special weapon calls, defining
special event-specific spells (Regional Magic), and allowing play-testing of new
rules.

The game rules and the rulebook are updated once a year by a council of EHs in
order to make the game more fun, make the game safer, clarify existing rules,
and encourage increased, improved role-playing. Additionally, the council
maintains a [list of Standing
Policies](./StandingPolicies.html){.compatibility}, where you can find any
temporary rules currently in place, and [rules and procedures for handling
Complaints](./ComplaintReviewBoard.html){.compatibility}. For more information
on events and the Realms, visit [realmsnet.net](https://www.realmsnet.net/).

## How Do I Start Playing? ## {#how-do-i-start-playing}

First off, read this rulebook. Read the whole thing. It is also a good idea to
re-read the Omnibus each year so you can be up-to-date on rules modifications
and changes that occur during the Event-Holders Council.

Next, you will need to make a character (the role you play while at events).
Characters are often referred to as Player Characters or PCs. This role will be
the vehicle you experience the Realms through. Creating a character is old hat
to many who have played tabletop role-playing games or other LARPs. If you are
unfamiliar with creating a character or want some tips, see [Character
Creation.](#character-creation)

After creating a PC, the best way to become acquainted with the Realms is to
simply go to an event. Most people will be happy to explain to you what's going
on. There is so much that can happen at an event that it is better experienced
than explained. Attending practices is also helpful, as you not only become
acquainted with the combat system, but also get introduced to other players.

### Realms Events ### {#realms-events}

An event is where the game is played. There are basically three kinds of events
in the Realms: Feasts, Tournaments, and Quests. Some events take on qualities of
all the types, but are predominantly one of the three. Here is what to expect at
each type:

Feasts
: Feasts, as the name implies, revolve around food. Political posturing and
  court are often held at feast events. There is generally little combat at a
  feast event, and they are usually held indoors in the wintertime. Bardics,
  games of chance or skill, and other such activities can usually be found at
  feasts. Feasts are often a good starting point for some players, particularly
  those more interested in role-playing than in combat.

Tournaments
: Tournament events usually feature contests of both individual and team martial
  skill. These are usually held as yearly events, and are generally a social
  occasion. Players more interested in combat and less interested in
  role-playing and magic find tournament events the best starting point for
  their Realms careers.

Quests
: Quest events are the traditional style of event for saving damsels in
  distress, finding and killing evil demons, and many other tasks. Nearly
  anything can happen at a quest event.

## Attending an Event ## {#attending-an-event}

### Registration ### {#registration}

A list of events can be found on realmsnet.net under the Realms Calendar or the
Events tab. Whenever you plan on attending an event, you are encouraged to
register for it through [realmsnet.net](https://www.realmsnet.net/) as some
events may have caps on how many players can attend, while others may have
different registration requirements or pre-event information to disperse to
players who are planning on attending. Registering for events also helps give
the event staff an idea of how many players they might expect, and helps them to
plan accordingly. It is also important to register any dietary or medical
restrictions you may have.

### Check In ### {#check-in-section}

When you arrive at an event, there are a few things you must do before beginning
play:

### Event Fees ### {#event-fees}

You must pay any event fees; failure to do so means you will not be able to
participate in the event and may be asked to leave site. Some events will waive
event fees for staff or NPCs (Non Player Characters) however, you must always
check with the event holder first. As event fees help pay for the site, props,
prizes, food, and costuming that goes into throwing events, without them future
events would not be able to occur.

### Waivers ### {#waivers}

You may be required to sign a waiver. Waivers serve two purposes: protection for
the EH(s) or the land owners from legal action, and to keep track of how many
people attended an event. Monitoring event attendance is the way for the Event
Holders' Council to determine [whether or not an event is
legal](#rules-for-event-holders).

### Magic ### {#magic-check-in}

An Event Holder may designate specific marshals to handle magic questions. These
are referred to as Magic Marshals (MMs). They may handle spellbook and magic
item check-in, spells that require active marshaling, and other magic-related
questions during the event. Individual sections of an event may have different
MMs, or MMs who handle different aspects of the magic system. Several spells
refer to "EH or MM", meaning a spellcaster needs official permission or response
to use that spell.

Spellcasters must check their spellbook with the EH or MM to find
out whether any of their spells work differently at the event before they cast
any of their spells. Spellcasters with [Regional Magic](#regional-magic) also
generally find out what their magic of the day is at check-in. Using spells at
an event without first gaining approval by the EH or MM is cheating.

### Magic Items ### {#magic-items-check-in}

Magic items must be checked in with the EH or MM
before the start of the event. EHs and MMs must know about every magic item at
an event, so that the event can be adjusted, if necessary. EHs and MMs have the
right to fail or disallow any magic item at any time. Using a magic item at an
event without first gaining approval by the EH or MM is cheating.

### Weapon Inspection ### {#weapon-inspection-check-in}

You must inspect your own weapons before they are used. If you are unsure of a
weapon's safety, ask a marshal to inspect it for you. EHs may require a
designated marshal (referee) to inspect all weapons to make sure they are safe
for use. There must always be someone at an event who can be asked to inspect
weapons in case anyone does not feel comfortable inspecting their own weapons.
If a weapon is deemed unsafe, it is to be removed from play or repaired.

## Alcohol Policy ## {#alcohol-policy}

Alcohol is not permitted unless explicitly allowed by the Event Holder. An event
advertised as damp or wet is informing attendants that alcohol is allowed,
subject to state and federal laws.

# Core Rules # {#core-rules}

## Participant Rights ## {#participant-rights}

The Realms is committed to providing a space that feels as safe and inclusive as
possible. Participants in the Realms have the out-of-character rights to
personal safety and the expectation they will be treated with courtesy and
respect by all other participants in the Realms. Participants have the right to
bring concerns to a marshal or other responsible staff member with the
expectation of having them investigated and appropriately addressed.

Every player has an obligation to read and follow the rules of the Omnibus. EHs
understand that the game can be complex and the first step in dealing with
behavioral issues or cheating in most instances involves ensuring that the
player understands the rules.

However, if a rule is broken, or if there are repeated infractions, an EH may
impose consequences on any participant(s) at their event. These may include a
formal warning; removal from specific sections of an event; removal from an
event; not being allowed to use specific weapons, spells, or items; or
prosecution for violations of Federal and/or State law. If you are removed from
an event for rules violations, the EH is not obligated to refund your event fee.

If an EH subjects a player to a consequence at their event they are encouraged
to notify all other EHs of this via the Event Holders' List. The player involved
must receive a copy of this notification and may send a formal response. People
who want to bring formal charges can do so by reporting such charges to the
[Complaint Review Board](#complaint-review-board)
(<complaints@lists.realmsnet.net>). In addition, any participant that feels
wrongful action was taken against them can bring the issue to the Complaint
Review Board.

An EH may opt to not allow a player who has a history of breaking rules to
attend their events, provided that player is notified in advance. The EH must
seek confirmation from the banned player that they are aware of the ban. The ban
will be in effect even if the banned player does not acknowledge it. The EH must
notify the Event Holders and Announcements mailing lists of the name of the
banned player without any further details about the ban circumstances. If a
notification by another EH has previously been sent for the banned player, the
EH may instead reference it in their event announcement. The EH may discuss the
ban further in narrower formats, such as private messages or verbal
conversation. Should an EH or Group of EHs decide a ban has been lifted,
notification must be sent to the player and the Event Holders and Announcements
mailing lists.

Event Holders have an obligation to take reports of rules violations at their
events seriously. Having an additional person present during the reporting is
strongly encouraged. If you report a violation to the EH of an event and are
ignored or no reasonable action is taken, bring the situation to the attention
of the Complaint Review Board. Event Holders who have a history of disregarding
reports of rules violations, or who do not address especially serious reports
may face penalties including, but not limited to, temporary loss of EH status,
un-backing of that EH's magic item(s), revocation of an event's legal status,
and/or being banned from holding future events.

Reports of rules violations to be submitted after the conclusion of an event can
be submitted to the Complaint Review Board.

## Code of Conduct ## {#code-of-conduct}

Anyone can report harassment or abuse. If someone's behavior towards you has
violated your rights as a participant, or if you witness the same happening to
someone else, immediately report the incident to the EH.

If you have reason to believe rules regarding the Code of Conduct may be
violated at an event, notify the Event Holder prior to the event occurring.

The following list is not meant to be exhaustive, and any non-consensual
violations of a participant's rights are considered violations of the rules.

Any of the following constitute grounds for punitive action:

* Violating any federal, state, or local laws, facility rules, or event
  policies;
* Failure to comply with the instructions of the Event Holder or designated
  marshal;
* Using anything in an out-of-character threatening or destructive manner
  against person or property;
* Endangering the safety of oneself or others;
* Stealing (People's tents, bags, packs, pouches, and possessions are completely
  off-limits and out-of-play. There are no exceptions, even if officially
  in-play items are concealed therein);
* Drinking alcohol under the age of 21, or providing alcohol to those under age
  21;
* Physical or verbal intimidation or abuse;
* Stalking;
* Inappropriate physical contact or proximity. Any form of unwanted physical
  contact is strictly forbidden;
* Non-consensual sexual acts (a person under the age of consent cannot consent
  to sexual acts);
* Inappropriate sexual attention, whether verbal or physical, or continued
  attention after being asked to stop (sexual attention towards those legally or
  physically unable to consent is always inappropriate; this includes people
  under the age of consent);
* Harassment for any reason, including out-of-game attributes such as race,
  weight, sexual orientation, gender identity, religion, or disability.

Role-play can never be used as an excuse for any of these behaviors. If you are
informed that your role-playing is OOC unsafe, threatening, or is not consented
to by other participants, you must stop immediately and find another way to
play out the scene.

## The Safety Rules ## {#the-safety-rules}

The safety rules are out-of-character (hereafter referred to as OOC). They are
for our safety, and they provide the guidelines that we all must play by.

### The Rules We Play By ### {#the-rules-we-play-by}

1. We should all be doing this to have fun. If you get mad or uncontrolled, it
   is up to you to remove yourself from the game until you have regained your
   composure.
#. This is a sport of personal honor; treat it as such. You are responsible for
   keeping track of many aspects of this game, such as weapon blows and known
   spells. Failure to report or accurately respond to any of these aspects is
   cheating and a stain on your personal honor.
#. You must listen to the marshals at all times; they are the referees. If you
   are caught breaking the rules, a marshal may remove you from all or part of
   that event.
#. Report any safety concerns to a marshal immediately. If you have any
   questions, it is up to you to ask.
#. HOLD: If you see a harmful or unsafe situation (e.g. someone is about to run
   into a tree, gets their glasses knocked off, has had their weapon really
   broken in combat, is about to fall off a cliff, etc.) yell the word "Hold"
   three times (i.e. "Hold hold hold!"). If someone is injured, it is the primary
   responsibility of the person who is hurt to call a Hold. Before calling a
   Hold for someone else in an otherwise safe situation, you must first ask if
   they are all right. Holds are only to be called in the event of a dangerous
   situation, and they may never be used to discuss the rules. If you hear the
   phrase "Hold hold hold," stop immediately, then repeat "Hold hold hold" until
   everyone else has stopped moving. Once the emergency has been dealt with, a
   "Lay-On" (continue play) will be called either by a qualified marshal, or the
   person who originally called the Hold. Do not resume play until a Lay-On has
   been called. If you are physically incapable of speaking to call the Hold for
   any reason, you may instead make a timeout sign over your head with both
   hands (a capital T shape) to get the attention of other players to verbally
   call the Hold for you.
#. Only weapons and shields that have been made following the [construction
   guidelines](#weapon-construction) are to be used. Weapons need to be checked
   between combat situations to ensure continued safety. A qualified marshal may
   be requested to check the safety of any weapons or shields at any time. Any
   new designs or materials must be inspected and approved by the EH or a
   designated marshal before use.
#. There is to be NO real steel in any combat situation, or in any potential
   combat situation, at any time. Real steel is defined as metal knives, swords,
   axes, darts, spikes, spurs, firearms, etc. - anything that could really hurt a
   participant. Real steel that is sheathed is still a potential hazard.
#. Acts considered unlawful in the real world, such as theft of non-game items
   or assault, are also unlawful in the Realms. For the safety of all
   participants, any form of unwanted physical contact is strictly forbidden.
#. Any spell props, weapon props, or materials not specifically allowed within
   the Omnibus must be inspected and approved by the EH or a designated marshal
   before they are used.
#. While the wording of the game rules may occasionally be less than clear,
   players may not use any such confusion to their own advantage. A simple
   guideline is to not assume any benefits unless they are specifically granted
   by a rule. If you feel that the way a rule is written grants you an advantage
   by omission of a statement to the contrary, you must review that rule with a
   marshal or EH prior to utilizing that advantage. While this document has a
   lot of general rules (such as [Basic Magic Effects Everyone Should
   Know](#basic-magic-effects-everyone-should-know)), any specific rule will
   always override these general rules.

### The Rules We Fight By ### {#the-rules-we-fight-by}

11. This is a lightest touch sport. ANY contact with a weapon to a body is to be
    taken as a hit. Ignoring a "light" blow is cheating and a marshal may remove
    you from the fight. There are to be NO full-strength swings. A marshal may
    remove you for excessive blow strength. Weapons, melee and missile, must be
    used with the minimum force necessary to score a successful hit.
#.  The face (eyebrows to chin) and throat area are "off-target." Do not aim for
    these areas. You may choose not to accept a blow that hits you solely in
    these areas, but you must call the location that was hit. For instance, if
    you are hit in the face, call "Face," and keep fighting if you wish. This
    alerts both the other combatant and the marshal that you have taken an
    illegal blow. It is not unusual for shots to an off-target area to also
    strike legitimate areas in the same strike. You must take all legal blows,
    even if an off-target area was hit by the same strike. For example, a shot
    thrown at your head may hit your face if you pull back, but if it touches
    your forehead in the process, that is a legal blow.
#.  In combat, there is to be contact with weapons only (i.e. weapons hitting
    weapons, weapons hitting bodies, and weapons hitting shields only). There is
    to be NO body contact of any kind (e.g. no shoving, punching, kicking,
    biting, grabbing, etc.). Do NOT grab an opponent's weapons and/or shield.
#.  Do NOT charge. Charging is defined as running at someone so that they have
    to get out of your way to avoid illegal contact.
#.  Shields are for blocking ONLY. Your shield must never be used as a weapon.
    Shield-bashing or other shield contact with another person is unsafe.
#.  Pommels, no matter how padded, are not legal striking surfaces. Do not punch
    or thrust with the pommel of a weapon.
#.  Do NOT ever throw a weapon at a participant, unless that weapon is of a type
    (magic missile, javelin, or lightning bolt) sanctioned by the rules for
    throwing. No thrown weapon or missile weapon may ever be targeted at your
    opponent's head or neck.
#.  Arrows must be drawn with minimal pull necessary to score a successful
    hit. Bows may not be used to parry an attack. As with thrown weapons,
     arrows must not be targeted at your opponent's head or neck.
#.  Non-Combatant players are people who wear a yellow armband or a yellow
    headband. These players are not to be struck by weapons and may be called
    dead by anyone wielding a weapon within 10 feet of them.

# Combat in the Realms # {#combat-in-the-realms}

## The Combat System ## {#the-combat-system}

The Realms uses a lightest-touch system for its combat system. That means that
no matter how lightly your opponent may touch you with their weapon, you have to
take the shot. This is to keep the sport friendly, so that everyone can play.
This system does not allow for "scratches" or "light blows" - you must take
these shots as normal blows. In all cases, the phrase, "if you are struck,"
refers to any time you are struck by the padded surface of any weapon or spell
prop that does damage as a weapon.

### Hit Locations ### {#hit-locations}

Your body is separated into seven locations of which there are two kinds. Your
arms and legs are "limbs," while your head and the front and back of your torso
are "kill locations." If you become confused about exactly where one location
stops and another begins, thinking of a Barbie™ doll may help. The buttocks of a
person are considered leg shots. If you are hit in the buttock(s), you lose the
appropriate leg(s).

### Killing Blows ### {#killing-blows}

If you are struck in a kill location your PC is [dead](#character-death).

If you are hit on the top of the shoulder, your PC is dead. If you are wearing
armor, a blow to the top of the shoulder is considered a blow to the front or
back depending on whether your opponent is in front of you or behind you.

### Limb Shots ### {#limb-shots}

If you are struck in a limb, your PC loses the use of that entire limb. If you
are struck in a limb that has already been lost, and that limb blocked what
could have possibly been a legal shot to another location, then that location
is considered to be hit. You cannot protect the side of your PC's body with a
disabled arm or by lifting a disabled leg up to block. Once your PC's limb has
been disabled, it should be put behind you. This keeps it out of the way as well
as provides a visual cue to other players that your PC is hurt. Once you have
lost a limb, your PC cannot use that limb at all. Don't limp on a damaged leg.

### Hand-on-Weapon ### {#hand-on-weapon}

If you are struck on a hand that is holding anything that is legal to block or
parry with (weapons and shields, but not bows, javelins or arrows), it is
considered gauntleted and immune to damage. When this happens, call out "Hand"
or "Hand-on-weapon." If you are struck on a hand that is not holding anything
you can parry with, even if you have just taken it off for a second, it is
considered a legal limb shot and your PC has to suffer the consequences. Your
hand is considered everything below the wrist bone.

### How to Take Multiple Hits ### {#how-to-take-multiple-hits}

Should an opponent's weapon (be it a melee weapon, missile weapon, or spell
prop) hit you in more than one location with the same swing, all points of
contact count as hits.

## Combat Etiquette ## {#combat-etiquette}

Combat is an important part of the game. In order to make the game more fun for
everyone, combat etiquette, which is OOC, has been developed. Using combat
etiquette helps avoid confusion and promotes the same kind of behavior when
someone else is fighting you.

### Calling Hits ### {#calling-hits}

It is your responsibility to call where you were hit in combat. This is to let
the other participant(s) and the marshals know that you were hit and where you
were hit. Calling armor, protections, and other effects is also your
responsibility. Armor must always be called, even when a PC is dead. It is not
acceptable to call another player's hits for them. If you feel that another
participant is consistently miscalling their hits, missing shots, or is cheating
in some other form, please alert a marshal. It is acceptable to ask your
opponent if they were hit, calling their attention to the location.

### Late Shots ### {#late-shots}

Sometimes you will strike another participant immediately after receiving a hit
that injures or kills your PC. For example, just before your sword makes contact
with an opponent, your attacking arm is struck by another weapon. Even though
you are incapable of physically halting your attack, the injury your PC received
renders the hit ineffective. There is no "follow through" effect that allows
your attack to be successful. This is called a late shot. If you deliver a late
shot, it is your responsibility to inform your opponent to not take the blow.
Common phrasing includes "Don't take that!" or "Late, on your arm!" Like other
shots, you may not declare that someone else hit you late. If you think someone
is failing to call their late shots, question them after combat ends, or bring
your concern to a marshal.

### Illegal Hits ### {#illegal-hits}

Sometimes you may hit someone with a part of your weapon that doesn't actually
damage their PC, such as the pipe of a weapon, the side of a [thrust-only
weapon](#thrust-only-weapons), or the shaft, fletching or nock of an arrow or
javelin. If you do this, be sure to call "Don't take that!" to alert them that
it wasn't a legal hit, and that they may resume play as if the hit never
occurred. A player may also call "shaft" if they received an illegal hit from an
arrow or javelin, or call "slash" if they received an illegal hit from a
thrust-only weapon.

### Missing Shots ### {#missing-shots}

Sometimes in the thick of battle people miss shots, because of adrenaline or
focus. This is generally unintentional and accidental. It often stops once a
fighter has more experience in calling and feeling their shots. Repeatedly
missing shots or intentionally missing shots is different and is cheating. If
you repeatedly miss shots, you may be asked to get retrained in fighting, or may
be asked to stop playing.

### Off-Target Areas ### {#off-target-areas}

The face and the front of the throat are off-target; never aim attacks there. If
you are hit in either location, you need to announce it, even though the hit has
no game effect. Face is considered the area on your head below your eyebrows, in
front of your ears. Throat is considered the forward-facing section of your
neck, above the sternum. The forehead from the eyebrows up, the back and top of
your head, and the sides and back of your neck are all legal targets.

Should an opponent's weapon hit you in more than one location with the same
swing and one or more of those blows land in an off-target area (such as your
face or your throat), you still have to accept those blows which did land in
legal locations.

Breast shots and groin shots are legal and considered killing shots, but such
shots are highly discouraged. Everyone is encouraged to wear protective gear to
avoid injury to sensitive areas.

Never deliberately aim for an off-target area (face with any weapon, or head
with a ranged weapon) or the groin or breasts. Accidents happen, but if you
frequently target these sensitive areas it will indicate to others that you are
not a safe and controlled fighter, and you may be asked to sit out by a marshal.

## Armor ## {#armor-section}

### Types of Armor ### {#types-of-armor}

All PCs can wear armor. Some characters may have limitations on calling the
armor they wear based on their armor restriction. Armor allows a PC to take
blows without taking injuries. There are two kinds of armor: heavy and light.
Heavy armor will absorb two blows per hit location before you have to take the
shot. Light armor will absorb one blow per hit location.

If you are able to call light armor based on your restriction, it must be
represented by wearing garb taking the appearance of quilting, lightweight
leather, or some other protective material. You may also wear garb taking the
appearance of heavier armor to satisfy this requirement (even if you cannot wear
and call 2-point armor based on your restriction).

If you are able to call heavy armor based on your restriction, it must be
represented by wearing garb taking the appearance of armor that is, in general,
bulkier, rigid, and more cumbersome than light armor.

Armor can be made of any safe wearable material, but it must allow you to feel
blows through it and it should look like it would fit in a medieval or fantasy
setting.

Armor protects by hit location. The armor hit locations are divided up by the
hit locations for taking wounds and kills. One piece of armor may cover more
than one hit location (e.g., a chain mail shirt covering your arms, chest, and
back), and multiple pieces of armor may cover the same hit location to meet the
coverage requirement. As long as you are wearing at least one piece of garb that
is obviously meant to be armor on a hit location, and that location appears at
least 75% covered, you may call armor on the whole of that location.

Additionally, if part of a location is covered by armor but not 75% covered as
stated above, you may still call armor on that location if you are hit in the
part of that location that is covered by the armor.

### Calling Your Armor ### {#calling-your-armor}

Light armor allows you to call out "Armor 1" on that hit location and the next
blow will do damage. If you are wearing heavy armor you can call out "Armor 1"
and then "Armor 2" before the hit location takes damage. When you call your
armor, you must also indicate the hit location where the armor was damaged. This
helps your opponent to know where they hit you, and saying it aloud also helps
you to remember where your armor is damaged.

For example, if you are wearing full heavy armor, and if you are hit in your
head, then your chest, and then your head again, the calls would be "Armor 1
Head", "Armor 1 Chest", and "Armor 2 Head". The next blow to your head would
kill you.

However, if a hit location is covered by both light and heavy armors, you must
call your armor based on whichever type of armor is struck by an attack.

For example, Jimmy is wearing a light armor gambeson that fully covers his arm
that is accented with a heavy armor bracer that covers his forearm. Jimmy is
struck on the sleeve of the gambeson twice. Jimmy would lose the one point of
armor on his arm in the struck location and then lose his arm. If the second
strike instead hit the heavy armor bracer, Jimmy would call his second point of
armor.

## Character Death ## {#character-death}

As players experience the game through the eyes of their characters, PC death
becomes an important aspect of the game.

### Death ### {#death}

Death occurs in many ways. Usually, death of a character happens when something,
such as a weapon, hits a character in an unarmored kill location. Certain spells
may also kill a character, from magic missiles to ingesting poisons or other
more esoteric means. Death renders the character incapable of any action until
such time as a spellcaster or item, with the power to heal the dead, raises the
character, or magic that regenerates or animates them takes effect.

Death can be repaired by many spells, abilities, and items. Be sure to go over
[Combat in the Realms](#combat-in-the-realms) and [Basic Magic Effects Everyone
Should Know](#basic-magic-effects-everyone-should-know) for more details on what
causes and cures death.

In the Realms there are two states of death. They are death (or dead), and
[soulless](#soul-loss).

If your PC is dead or soulless, do your best to role-play a corpse. It is
important to be aware of your surroundings for safety reasons, but try to lie or
sit still, avoid talking, and avoid paying attention to things happening that
your character won't be aware of. Try not to look around or talk. Do your best
to role-play a corpse. Don't get upset if someone hits you with a killing blow
when you are already dead. If somebody does this just say, "Dead." They are
making sure that you really are dead.

In tournaments, or other high combat situations, it is acceptable for a
character to move out of the way to avoid being stepped on. They may resume
their death act in a safer place. They may also sit or kneel to avoid injuries.
If you are role-playing death in any of these forms, try to keep your weapon
over your head to signify your character is dead. If you do not have a weapon,
putting your fist on top of your head also works.

If someone looks at you and asks you to describe your wounds, do your best to
comply. If you were backstabbed and you're lying on your back, tell them they
don't see any wounds. Then, if they roll you over to look at your back, tell
them they see a deep wound in your back.

When your PC is killed (dead, but not soulless) and then raised, they may
remember everything up until the point of their death.

### Playing Dead ### {#playing-dead}

It is legal for characters to lie down and pretend they are dead, but they may
not put their weapons over their head. If someone asks a player if their
character is dead, the player and the character are not obligated to answer, but
if a player is asked to describe their character's wounds, they must do so as
accurately and honestly as possible. If their character is not really dead and
someone comes close to them to loot their character's body, they are free to
attack the unsuspecting looters. If you suspect a character is not dead and
would like them to be, you may gently tap them with a weapon in a kill location.

## Soul Loss ## {#soul-loss}

Soul loss is the removal of a character's life essence. It is a more serious
form of death that requires more than a simple spell to repair. A character who
has lost their soul cannot be raised or animated until their soul has been
restored.

### Destroying a Body ### {#destroying-a-body}

In order for a player to separate a soul from a body, they must destroy the
body. This is done by striking 200 blows with a weapon beside the body being
destroyed. More than one person may destroy a body at a time. More than one
weapon may be used to destroy a body as well. This effectively divides the
number of blows to be struck between the number of participants and number of
weapons used. Some monsters and characters under certain spells may require more
blows to completely destroy. If you strike 200 blows and the victim says "The
job is not yet done," then the body is not yet destroyed. Some spells, such as
[Strange Brew: Potion of Acid](#potion-of-acid) and [Assassin's
Blade](#assassins-blade), may accelerate the act of destroying a body.

After the body is destroyed, the character whose body has been destroyed is
rendered soulless. It is then the responsibility of the player who destroyed the
body to inform an EH or an appointed marshal. The EH must be informed
immediately of the body's destruction, thus allowing the EH time to prepare for
those who might wish to return the soulless character to life.

If your character is pretending to be dead, interpret any body destroying
blows as blows to the closest kill location. So, if your PC is pretending to be
dead and someone starts to destroy their body, the first blows that are struck
on the ground next to you count as if they were striking your PC on the nearest
kill location, destroying any undamaged armor, and then killing them.

If your PC is killed and their body is destroyed, they will not remember
anything about how they died should they manage to be raised.

Should your PC be dead at the end of an event without being raised, even if the
character's body was not actively destroyed, the PC will be considered soulless.

### Fixing a Soulless Character ### {#fixing-a-soulless-character}

To restore someone to life after they have lost their soul, characters must
first have the body of the person needing to be raised. Then characters must
either cast a [Call the Soul](#call-the-soul), an [Intervention](#intervention)
spell, or administer a [Potion of Soul Snare](#potion-of-soul-snare) to summon
and reattach the soul. If characters lack the body, only an
[Intervention](#intervention) spell will be able to return the soulless PC back
to life. When the body and soul are reunited, characters must cast one [Raise
Dead](#raise-dead) spell for each event (including the first) since the soulless
PC was rendered soulless in order to raise them. A different spellcaster must
provide each [Raise Dead](#raise-dead) spell used for this purpose.

## Permanent Death ## {#permanent-death}

When a PC is dead and soulless at the end of an event at which they were at some
point alive, they get a "tick." PCs may also receive ticks due to use of certain
magic items or plot interactions. A PC is only obligated to accept one
involuntary tick per event. A PC that accumulates three or more ticks is
permanently dead, as their soul can no longer be restored by any means. On
January 1st of each new year, one tick is removed from each PC that has any,
unless they are already permanently dead.

If a PC is killed and rendered soulless during an event, but returned to life
before the end of the event, they do not get a tick.

For a body's complete destruction to be official, it must be brought to the EH's
attention. The EH must provide the Death Marshal with this information from
their events. A tick may only be issued or reported by a legal EH of the event
where the tick was incurred or by the player whose PC received the tick. The
[Death Marshal](#death-marshal) will keep track of this information.

## Weapon Rules ## {#weapon-rules}

Players are responsible for being safe with the weapon(s) they are using. Before
using a weapon style in-game, players must be properly trained by a marshal or
someone who is safe and proficient with that weapon style.

### Wielding Weapons ### {#wielding-weapons}

For the purposes of the rules, you are wielding a weapon or shield if you are
holding it in your hand(s) and you attack, parry, or block with it. You are
wielding a combination of weapons and/or a shield if you attack, parry, or block
with either of them. Simply holding a weapon, or menacing with it, does not
count as wielding unless contact is made. You cannot wield more than one item in
a hand at once.

If you are wielding an illegal weapon or combination and you attack, then you
must tell the target "Don't take that." If something you are not wielding
blocks a shot, then you must treat the blow as if it had landed. If you are not
sure where that blow would have landed, then assume it would hit the location
that would cause you the most harm (e.g. an unarmored kill location). There are
special restrictions for spellcasters in terms of wielding and blocking with
different weapons, and specific consequences for doing so outside of the
spellcaster's restriction. See [Breaking Weapon
Restriction](#breaking-weapon-restriction) for more information.

The size of a weapon or shield, as measured by the longest dimension, dictates
how it may be wielded. Weapons and shields cannot be any smaller than 12", and
cannot exceed 8' in length.

+-------------+-------------------------+
| Length      | Weapon Type             |
+============:+:========================+
| 12" to 3'8" | One-handed              |
+-------------+-------------------------+
| 3'8" to 5'  | Hand-and-a-half         |
+-------------+-------------------------+
| 4' to 5'    | One-handed, no          |
|             | Florentine, thrust-only |
+-------------+-------------------------+
| 5' to 6'6"  | Two-handed              |
+-------------+-------------------------+
| 6'6" to 8'  | Two-handed, thrust-only |
+-------------+-------------------------+

Table: Weapon Length Quick Guide

### One-Handed Weapons ### {#one-handed-weapons}

You can wield a one-handed weapon or shield in one hand and still use another
one-handed weapon or shield in the other hand. Using two one-handed weapons (up
to 3'8" each) together, one in each hand, is commonly called a "Florentine"
combination. [Magic Missiles](#magic-missile) are also considered one-handed
weapons, but follow special rules.

### Hand-and-a-Half Weapons ### {#hand-and-a-half-weapons}

If a weapon is considered hand-and-a-half, then you can use the weapon with one
hand, but your other arm or hand cannot be holding a weapon or a shield.
Lightning bolts are also considered hand-and-a-half weapons. [Bows](#bows) are
wielded as hand-and-a-half weapons, but follow special rules.

### Two-Handed Weapons ### {#two-handed-weapons}

If a weapon is two-handed, you may only wield the weapon with two hands. Every
blow you strike must be started with two hands. If you lose an arm while
wielding a two-handed weapon, you may not attack with that weapon. You may parry
with a two-handed weapon with only one hand, but only if you are not wielding
something else with your other hand.

Every blow you strike with a two-handed weapon must begin with both hands on the
weapon. If you let go with one hand during the swing, the swing is still legal.
Once the initial swing has ended, the attacker will have to grab the weapon with
two hands again before making another attack.

### Thrust-Only Weapons ### {#thrust-only-weapons}

Thrust-only weapons cannot strike an opponent with a "slashing" or side-to-side
motion. If you slash at an opponent with a thrust-only weapon, you must tell
them "Don't take that." As long as the tip of the weapon strikes with a forward
motion the blow must be taken. If you are not certain that a thrust-only strike
landed properly, you must assume it was a slash and tell your opponent not to
take the shot.

### Spears (Thrust-only) ### {#spears}

A spear is a thrust-only weapon between 4' and 5' in length. These weapons may
be used as a single-handed weapon if used alone or with a shield. Spears may not
be used with other weapons. Their length must be between 1/3 to 1/2 covered in
foam and they must be clearly labeled with the word "Spear" on the blade. Once
being labeled as a spear the weapon must stay thrust-only even if it is legally
made to regular slashing hand-and-a-half standards.

### Missile Weapons ### {#missile-weapons}

Projectile weapons, such as arrows, javelins, and spells with missile props
(like [Magic Missile](#magic-missile) and [Lightning
Bolt](#lightning-bolt-spell)), cannot be targeted at your opponent's head. Head
and neck shots from projectile weapons, even if they did not hit the face or the
throat, do not have to be taken as legal shots.

Magical missile weapons granted by spells with missile props are live and
inflict damage on any target they hit until they come to rest, regardless of
whether they hit the ground, a wall, a weapon, a tree, or any other obstacle
along the way.

Arrows and javelins are live and inflict damage on any target they hit until
they hit the ground, regardless of whether they hit a wall, a weapon, a tree, or
any other obstacle along the way. Only being struck with any portion of the foam
head of the arrow or javelin will cause damage. The shaft, fletching, and nocks
of arrows and javelins do not cause damage.

It is the responsibility of the player wielding the bow or javelin to tell you
not to take the blow. A player may also call "shaft" if they received an illegal
hit from an arrow or javelin. The length of javelins and arrows are not subject
to a spellcaster's weapon restriction.

### Arrows ### {#arrows}

An arrow does not take any hands to wield and can be wielded with a bow.
However, they are considered weapons for the purposes of spells and will cause
some spells to fail. An arrow is only considered live when it is shot from a
bow.

### Bows ### {#bows}

A bow is wielded as if it were a hand-and-a-half weapon. This means that it may
not be held when a weapon or shield is being wielded in your other hand, but it
is legal to fire it with one hand --- if you can! A bow is not considered a
"weapon" for purposes of determining whether a spell fails, unlike the arrows
it fires.

Bows must have a full draw weight of 30 pounds or less. Just like melee weapons,
be careful about how hard your arrows are striking your opponent. Fire arrows
with the minimal pull necessary to score a successful hit.

If a weapon hits a wielded bow or nocked arrow, the bow is "broken" and may no
longer be used in combat. Anyone can fix a broken bow by holding the bow with
both hands, then counting to 120 seconds. You cannot actively parry with a bow
or an arrow.

### Javelins ### {#javelins}

A javelin is wielded as if it were a single-handed weapon. This means that it
may be held when a weapon or shield is being wielded in your other hand. A
javelin is considered a weapon for the purposes of spells. A javelin is only
considered live when it is thrown.

## Weapons Construction ## {#weapon-construction}

There are several ways to make weapons in the Realms. If you are playing for the
first time, it is a better idea to borrow weapons than to try to make any of
your own. Once you see what other weapons look like and ask people about how
they made their weapons, you will be better prepared to construct your own.

Be sure to follow these guidelines when constructing a weapon:

* All non-missile melee weapons must be made out of lightwall PVC, fiberglass,
  or bamboo cores as noted in the table below.
* There can be nothing in the core, and both ends must be capped with a rigid
  material.
* All weapons must have a thrusting tip (the pipe foam that extends beyond the
  tip of the pipe) as noted below.
* A weapon's striking surface must be made from closed-celled foam pipe
  insulation, which must be at least 5/8" thick and which must be firmly
  strapping-taped in place.
* All striking surfaces must be covered in either duct tape or fabric.
* Only magic weapons may be made with a blue-colored striking surface, and you
  may only make a magic weapon through the use of spells in the magic system, or
  if you are releasing one as an EH.
* Closed-cell foam, as referenced in this omnibus, refers to foams such as
  camping mat foam, yoga mat foam or EVA foam. Some foams are significantly more
  dense or less dense than others. If you are unsure of about your choice of
  foam, check with members of the community.

+--------------------+-----------------+-----------+-------------+-----------+---------------+
| Length             | Core            | Thrusting | Closed-cell | Squishy   | Minimum Foam  |
|                    |                 | Tip       | Foam Disk   | Foam Tips | Coverage      |
+:==================:+:===============:+:=========:+:===========:+:=========:+:=============:+
| 12" - 44"          | 1/2", 3/4",     | 1 3/4"    | Not         | Not       | Half of       |
|                    | or 1" PVC       |           | Required    | Required  | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| 12" - 44"          | 3/8" bamboo or  | 1 3/4"    | 1/4"        | Not       | Half of       |
|                    | 1/2" fiberglass |           |             | Required  | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| \>44" - 60"        | 3/4" or 1"      | 1 3/4"    | Not         | Not       | Half of       |
|                    | PVC             |           | Required    | Required  | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| \>44" - 60"        | 1/2" bamboo or  | 1 3/4"    | 1/4"        | Not       | Half of       |
|                    | 3/4" fiberglass |           |             | Required  | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| Spears\            | 3/4" or 1" PVC  | 2 1/4"    | 1/4"        | 2"        | One-third     |
| (48" - 60")        |                 |           |             |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| Spears\            | 1/2" bamboo     | 2 1/2" *  | 1/4"        | 3"        | One-third     |
| (48" - 60")        |                 |           |             |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| \>60" - 78" \      | 3/4" or 1" PVC  | 2 1/4"    | Not         | 2+"       | Half of       |
| (6'6")             |                 |           | Required    |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| \>60" - 78" \      | 3/4" bamboo     | 2 1/4"    | 1/4"        | 2+"       | Half of       |
| (6'6")             | or fiberglass   |           |             |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| Pikes\             | 1" PVC          | 2 1/4"    | 1/4"        | 2"        | One-third     |
| (>78" - 96")       |                 |           |             |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+
| Pikes\             | 3/4" bamboo     | 3" *      | 1/4"        | 3"        | One-third     |
| (>78" - 96")       |                 |           |             |           | weapon length |
+--------------------+-----------------+-----------+-------------+-----------+---------------+

\* add 5" closed-cell foam sleeve starting beneath the foam disk extending away
from the tip

### Double-Bladed Weapons (Including Marns) ### {#double-bladed-weapons}

* No surface of a double-bladed one-handed slashing weapon may be shorter than
  6".
* The overall (combined) foam length must still be at least half the length of
  the weapon.
* Each striking surface of a double-bladed slashing weapon of hand-and-a-half
  length or longer must be at least 1/3 the total weapon length.
* Weapons with a non-bladed portion longer than the shorter of the two blades
  must provide non-damaging covering foam or other padding as a courtesy to
  shield the exposed core.

### Arrows, Javelins, and other Missile Weapons ### {#arrows-javelins-and-other-missile-weapons}

__T8, golf tube, and Aqua Tube Arrows__

* All arrows must have 2" thrusting tips, 4" of pipe foam, and 2" squishy-foam
  tips to be considered safe.
* All arrows must be made from 3/4" or 1" diameter tubing.
* All arrow tube shafts must be capped on the arrowhead end of the shaft with a
  rigid material. Examples of acceptable cap materials include, but are not
  limited to, rigid plastics (such as milk jug caps or film canister caps) or a
  single washer (non-metal washer) or coin. Any such rigid materials must be
  appropriately sized to match the arrow shaft in diameter.
* Arrows may not be weighted in any way beyond the materials necessary for safe
  construction. Weighting includes the attachment of multiple rigid caps.
* All aqua tube shafts must, at minimum, be covered by two lengths of
  strapping tape.
* Lightning Bolt props are the only "arrow" props that may be made solely from
  white duct tape. Arrows may have a white head or shaft, but not both.
* Whether custom-designed arrows are allowed is up to the individual EH's
  discretion.

__LARP Arrows__

* Head must be permanently affixed, commercially manufactured, designed to be
  "larp safe" or "archery tag safe" with squishy foam, and have a minimum
  diameter of 2 1/8" on the striking surface.
* Shaft must be made from either unbreakable or safety fiberglass, covered in a
  minimum of 2 layers of fiberglass reinforcing tape (strapping tape), and be no
  longer than 29" measured from the end of the nock to the base of the arrow
  head.
* Nocks must be permanently affixed and must be manufactured and designed to be
  "larp safe" or "archery tag safe".

[__Lightning Bolts__]{#lightning-bolt-construction}

A Lightning Bolt prop is a T8, golf tube, or aqua tube arrow, or a javelin, made
with the following additional details:

* It must be primarily white.
* It must be between 2'6" and 3'6" in length. The prop is not subject to the
  spellcaster's weapon length restriction.
* It must have the words "Lightning Bolt" and the spellcaster's name written
  visibly along the shaft.
* It cannot have an arrow nock.
* It must not have the word "Javelin" on it if it is a javelin prop.

__Javelins__

* Javelins may be constructed using golf tubes or aqua tubes with foam coverage
  on at least 1/3 the weapon length.
* Javelins cannot be nocked, and must have foam coverage on the non-damaging end
  of the shaft.
* Javelins must be made from new pipe foam and must have squishy-foam heads.
* Any fletching added must be made out of foam, and the javelin may not be
  weighted in any way.
* Javelins must be between 2'6" and 5' in length.
* The word "Javelin" must also be written on the side of the weapon.

## Shield Construction ## {#shield-construction}

* You as an individual must be able to safely wield any shield you use. A
  marshal or EH can pull a shield if they feel it is unsafe.
* Shields can be made of any safe material, such as wood, plastic, or cardboard.
  Metal shields are heavy, but are allowed if otherwise safe.
* All shields must have their edges covered by foam. Any protruding metal screws
  or bolts must also be padded.

## Equipment Inspections ## {#equipment-inspections}

You must inspect any armor, weapon, or shield before using it. If you are unsure
about an item's safety, ask a marshal and they will inspect it for you. Any item
can be inspected at any time during an event at anyone's request. This is meant
for the purpose of ensuring safety and may never be used for strategic or
tactical purposes. The EH or a designated marshal retains final ruling on the
approval for use of any armor, weapon, or shield.

### Armor Inspection ### {#armor-inspection}

Armor can be failed or the point value of armor may be adjusted for many
reasons. Armor can have exposed edges that could cause a safety concern for the
wearer or other combatants. The EH or designated marshal retains final ruling on
the point value and approval for use of armor.

### Shield Inspection ### {#shield-inspection}

Shields can be failed for many reasons. A shield can have exposed edges or
protrusions that could cause a safety concern for the wearer or other
combatants. A shield can be failed if it has seen too much abuse and has not
been repaired recently. The EH or designated marshal retains final ruling on the
approval for use of a shield.

### Weapon Inspection ### {#weapon-inspection-construction}

Weapons can be failed for many reasons. A weapon can have too much "whip" (one
that flexes too much) or not enough "whip" (one that doesn't flex at all). There
is no standard way of measuring flex, you will have to use your common sense. A
weapon can be failed if it has seen too much abuse and has not been repaired
recently. The most common problem weapons have is that their thrusting tips are
breaking down or have been compacted. The EH or marshal retains final ruling on
the approval for use of a weapon.

## Combat Calls ## {#combat-calls}

Combat calls are what you may hear yelled in combat, and you must know how these
calls affect your PC.

### Armor ### {#armor-call}

Negates a hit attack. When "Armor" (or "Armor 1," "Armor 2," or "Armored Cloak")
is called in combat, it means that the person calling armor is protected against
the attack that landed on them, usually by means of armor or a spell effect.

### Boulder ### {#boulder}

A PC can encounter a boulder call in one of two ways: being hit with a physical
boulder prop or by a weapon simulating the effects of a boulder. In either
instance, the same call of "Boulder!" is used.

If you or any of your equipment are hit by the call of "Boulder!", you suffer
the following effects: your character is dead, any armor hit is "broken" where
the boulder touched the locations, magic items hit by the boulder are
disenchanted, and any non-monetary equipment (i.e. weapons, bows, and shields)
that is hit by the boulder is "broken." It is the player's responsibility to see
that items damaged this way are not used until the appropriate repair spells are
cast upon them.

A boulder prop is often represented by throwing large duct-taped chunks of foam
or beanbag chairs with the zippers duct-taped over. When thrown, the boulder is
active until it comes completely to rest. PCs may not throw or pick up boulders.
Four or more PCs, using a total of eight hands on the boulder, may work together
to "push" a boulder along, to free trapped gear and companions, but not to cause
damage.

### Disease ### {#disease}

If a weapon strikes a PC and the wielder calls "Disease," wounds on their body
take twice as many spells to be healed until the disease is cured. This effect
only damages a PC, not their armor. For example, if you are hit by a diseased
blow to the leg, [Heal Limb](#heal-limb) would need to be cast twice on the leg
for it to be healed until the disease is cured. Similarly, if you are killed by
a disease shot, the spell [Raise Dead](#raise-dead) will need to be cast twice
on you until you are cured of the disease.

If a spell cast on you fails because you are diseased, you would respond with
"The job is not yet done" to alert the spellcaster to your condition.

Regeneration effects will still affect a diseased character with a single use,
but the regeneration time is doubled until the disease is cured. Disease can
affect both living and dead characters.

The spells used to heal a wound of a diseased character must be cast in
succession (i.e. without other spells being cast on the character between the
two spells).

### Fireball ### {#fireball}

If you or any of your equipment are hit by the call of fireball, your character
is dead. Armor and weapons hit by the fireball are not broken, but also do not
protect against its damage.

The fireball call may come in the form of a thrown prop (as with a magic
missile) or as a weapon blow. Fireball is magical in nature.

### Flat ### {#flat}

To "knock out" an opponent, the attacking player must call out the word "Flat"
prior to their attack. Should a successful killing blow be struck, they have
instead rendered their foe unconscious for a steady count of 120 seconds. The
unconscious PC may be wakened sooner by having another PC come and wake them up
by touching the unconscious PC and saying "Wake Up." The PC can also wake up
when any damage is inflicted on them. You may call "Flat" at any time, even in
the middle of a fight. This form of attack deals no damage to armor.

### Impale ### {#impale}

Impaling is the act of holding a weapon in an opponent's kill location after
death, and saying the word "Impale." It counts as continuous,
non-body-destroying blows to that location. The act of impaling will bypass any
armor, damaged or not. While impaled, a character cannot be raised or animated
and is unable to regenerate. The results can vary when impaling NPCs.

### Lightning Bolt ### {#lightning-bolt-call}

If you are hit by any part of a white boff arrow or javelin and the thrower
calls "Lightning Bolt," your PC is struck as if by a [magic](#magic-call)
[piercing](#piercing) weapon.

### Piercing ### {#piercing}

Armor cannot always protect a PC from certain attacks. If an opponent attacks a
PC in any way and calls out "Piercing," any of the armor that is struck by the
attack is completely destroyed, and the PC suffers the effect of the blow as if
they were not wearing armor. For example, if a PC is wearing heavy armor on
their right arm, and an opponent hits it while calling "Piercing," the PC's
armor is destroyed on that arm and they lose the limb.

### Poison ### {#poison}

There may be occasions where a PC is struck with a blow and the wielder calls
"Poison." If this strike damages a PC in any location, be it torso, arm, leg,
etc., then the PC is killed. When the blow does no damage to a PC, such as a hit
to an off-target area or a hit to armor that protects the PC, then the poison
has no effect. Armor struck with a poison blow is still used. If a PC is under
the effects of a spell that protects them from poison in some fashion, the PC
still takes the normal damage from the blow but the poison will have no
additional effect. Players must also call "Immunity to Poison," to allow an
opponent to understand that they recognized that the blow was poisoned.

### Weapon Type and Material Calls ### {#weapon-type-and-material-calls}

Occasionally, more powerful monsters are only affected by certain weapon types
or by certain materials. For example, axe-mace trolls are typically only injured
by axes and maces; werewolves are only affected by weapons made of silver. It
might be a good habit when using a melee weapon other than a sword to call the
type of weapon you are using as you swing. If using a mace, say "Mace" with each
swing. When you are wielding a non-normal weapon (e.g. magic or silver), that
weapon will always strike with the same effect therefore you must call that
effect with every swing.

### Magic ### {#magic-call}

If a weapon strikes you and the wielder calls "Magic" (or "Magic Missile"), it
means your PC has been hit with a magical blow. Generally, being hit by magic
doesn't affect your PC any differently than being hit by a normal weapon, but
sometimes PCs are under spell effects where it makes a difference.

### Silver ### {#silver}
If a weapon strikes you and the wielder calls "Silver" it means your PC has been
hit with a silver blow. Being hit by silver doesn't affect your PC any
differently than being hit by a normal weapon.

```{=html}
<a href="./StandingPolicies.html#temporary-trial-of-modified-non-com-rules" class="compatibility" aria-label="This section links to the Standing Policy which replaces it." title="This section links to the Standing Policy which replaces it.">
```

## Non-Combatant Players ## {#non-combatant-players}

At any time during an event, a player may declare themself to be a
non-combatant. They do this by informing an EH or designated marshal and by
putting on a player-provided yellow armband on each arm (or on their head, if
their arms would otherwise be covered). Only players who declare themselves to
be non-combatants may wear a yellow armband. Once a player declares themself to
be a non-combatant, they will stay a non-combatant until the end of an event
(unless given approval by an EH). A player declaring themself to be a
non-combatant takes on a few unique rights and responsibilities:

* First, and most importantly, never hit a non-combatant with a weapon.
  Intentionally doing so is cheating. Instead, to kill a non-combatant, you may
  simply point at them and say "die", provided either you are wielding a melee
  weapon and are within 10 feet of them, or you are wielding a missile weapon.
  This can not be resisted by any means. If a non-combatant is regenerating, you
  can simply point at them and say "impale" and, while within 10 feet of the
  body, they will be considered impaled. They may be rendered soulless as
  normal.
* Second, a non-combatant may never wield a weapon and must take reasonable
  precautions to avoid looking like they are wielding a weapon. They may carry
  weapons and shields for others, but shall do so in such a manner that makes it
  clear that they are not being wielded. In addition, non-combatant spellcasters
  may not use the spells Magic Missile or Lightning
  Bolt.
* Third, a non-combatant shall do their best to stay out of the flow of combat.
  Should a non-combatant die, and combat is near to them, they may walk while
  dead to an OOC-safer place for them.
* Fourth, a non-combatant must make sure their yellow armband is visible at all
  times. If it is not, they must remove themselves from any combat situations
  until they have a visible armband.
* Finally, a non-combatant is encouraged, but not required, to tell the EH of an
  event the reason for them to be a non-combatant, if the non-combatant feels
  that the information should be known by a member of staff.

A yellow armband must meet the following requirements:

* It must be a dayglo yellow or other similar fluorescent yellow color with the
  words "Non-Com" on it.
* It must be at least 2 inches wide or actively glowing. At night it still must
  be visible, including being lit up, if needed to make sure it is seen.
* If a non-combatant's arms are covered, a headband matching these criteria may
  be worn.

```{=html}
</a>
```

# Non-Combat Interactions # {#non-combat-interactions}

## In-Character and Out-of-Character ## {#in-character-and-out-of-character}

### Being In-Character ### {#being-in-character}

Generally, events officially begin after the safety rules, any specific site
rules, and other special event rules have been read aloud to the gathered
players.

Once an event has begun, you are assumed to be in-character (IC) at all times.
This means that you are playing your PC the whole time you are at an event.
Staying IC can add greatly not only to your own event experience, but to those
who are playing the game around you. When a companion of your PC is "killed" it
adds to the tension and drama of the scene if they pretend they are dead, but it
breaks the mood completely if they are laughing and making comments.

Your character may feel differently than you do about something, like slavery,
magic, politics, or religion. You may be a pacifist, while your character is a
bloodthirsty barbarian. If you can remain true to the character, despite your
differences, you can make a memorable story for yourself and those around you.
Sometimes staying IC is challenging, especially when you know something that
your character shouldn't logically know, but it is worth it to try to remain IC
when playing. Likewise, when the game is done, leave your character behind.

### Breaking Character ### {#breaking-character}

Once an event has begun, breaking character is discouraged except when
necessary. If you must do so, it helps if what you say is prefaced with
"Out-of-character," as in "Out-of-character, where is the tenting area?" That
way, the person you are addressing knows that it is a real-world concern, and
should be dealt with differently than a strictly IC concern.

Sometimes when players get really into character, you may begin to wonder
whether animosity or other emotions are completely IC. It is acceptable to break
character to make sure everything is in fact still IC and no one's feelings are
getting hurt OOC.

### Out-of-Character-Only Terms ### {#out-of-character-only-terms}

There are a few terms that should only be used when speaking OOC: "Hold" and
"Medic." [Hold](#the-safety-rules) is only used in emergencies as it stops the
game. Medic is used when someone needs immediate real-world medical attention
for any reason: an allergic reaction to a bee-sting, a twisted ankle, an asthma
attack, etc. Do not call "Medic" for imaginary (IC) injuries. If you need IC
medical attention, call "Healer!"

### Out-of-Play Areas and Time-Out ### {#out-of-play-areas-and-time-out}

The EH has the option of declaring portions of the event site as "out-of-play"
for safety reasons or for NPC use. Never use these out-of-play areas as safe
havens.

If a fight breaks out in an area that is unsafe to fight in or that is
out-of-play, then the fight needs to be moved to a safe in-play area. If you are
in such an area, you may be asked to leave said area for combat. If you refuse
to leave the unsafe area, your PC is considered dead. After the fight, those
involved can move back to where the fight "really" took place, and continue on.

An EH is free to create an in-game safe area if they choose. For example,
powerful enchantments on the tavern may render weapons and hostile magic
inoperable within the tavern walls. In this case, it is fine for players to hide
there, since they will be taking advantage of an in-game effect.

EHs may also declare a time-out during an event for sleep, dinner, etc. While on
an event site, if you are not acting as an NPC at the EH's request, or you are
not in a time-out, a character is liable to be attacked, and their possessions
open to theft.

## Dragging ## {#dragging}

To drag a dead, unconscious, or otherwise incapacitated body in our game, you
must place a hand on the shoulder, back, or arm of the body and say "Drag." The
player being dragged must then get up and walk with you, bringing what they are
carrying with them. Stealable items stay on a dragged corpse unless explicitly
searched off. A dragged body is considered one-handed and may not be used as a
weapon or a shield. At any time you may tell the dragged player "Drop," thereby
letting go of the player and dropping them on the ground. A body being dragged
can never be "thrown" or "tossed." If the person dragging the body lets go, then
the body drops in place.

## Searching and Theft ## {#searching-and-theft}

There are certain items that are referred to as being "Stealable." Some examples
of stealable items are magic items, magic weapons, Realms currency, silver
weapons, and occasionally non-magical items. With the exception of currency, the
word "Stealable" is likely written somewhere on the object.

Players must assume that any props or items used by event staff are not
stealable unless labeled as such or told otherwise by event staff.

### Searching ### {#searching}

Searching is a touchy subject. In the real world, if a bandit has just killed
someone they can just take everything they own. In the Realms, an object has to
be considered stealable to be taken from a person or location without the
owner's permission. The problem is that often these stealable items are not
easily recognizable, especially the smaller items. Also, while "secret pockets"
and such seem like a good idea at first, frisking a dead character could be
considered a form of harassment towards the player and is to be avoided. To
handle this situation, the searching rule exists.

The searching rule is verbose because there have been a lot of problems about
searching in the past. The rule is mostly common sense. Once you think about it,
it will seem quite simple. There are two ways you can search someone, using a
Point Search or Full Search.

### Point Search ### {#point-search}

To simulate ransacking a character's pouches, weapons, and clothing quickly, a
player can "point search." Essentially, the searcher says "Search" and tells the
victim where they are searching (e.g. "I search your pouch.") If there are
stealable items in the area being searched, then all the items in that area are
handed over immediately. The area that a person point-searches cannot be any
larger than one hit location on the body (e.g. you would have to search each
sleeve of a shirt and the front and back of a shirt to search everywhere inside
the shirt). Pockets and pouches have to be pointed out to be searched. You
cannot say "I'm searching all your pockets," you have to search each one
individually; left and right sleeves, boots, gloves, etc., all have to be
searched separately, one at a time. Only one person can point search a victim at
a time. Point searching does not wake up an unconscious character.

### Full Search ### {#full-search}

The other way to search someone is to simulate taking your time to do it
thoroughly. That is, the character simulates stripping the body from head to
toe, ripping everything to shreds, garnering every last item you own, etc. In
order to do this the searcher simply says "Full search." Every stealable item
the victim has must be handed over to the searcher. This search takes 120
seconds to perform. After this, the items are the IC possessions of the
searching character. Full searching will wake up an unconscious character.

If someone full-searches a character and a different character comes by during
that time and point-searches the victim, the full search is stopped and the
point search is resolved. If a character does not specify what kind of search
they are performing, then it is assumed that they are performing a point search.
If told only that they are being searched, the player whose character is being
searched must assume it is a point search and respond, "Where?"

### Realms Thieves ### {#realms-thieves}

The only objects that are always in-play, are fair game for theft, and can be
stolen without consulting the bearer of the object, are those considered
stealable in-game (see [In-Game Items](#in-game-items), below). In order to
steal any other object, you must have the explicit permission of the
owner/bearer before making the theft. This means that to steal another PC's
jewelry (assuming that some of it is considered treasure), you must ask the
person who plays that PC. One way to do this is to kill or flat the character
and tell them that you are searching them. If they have anything that is
in-play, they must show it to you, for you to take or leave as you wish. Never
pick up something off a table or from in front of someone's tent, unless it is 
Realms currency or either labeled Stealable or Event-Stealable.

It should be restated that people's tents, bags, packs, and pouches are
completely off-limits and out-of-play. No matter how many magic or silver
weapons, or how much Realms currency someone might have, you may never, under
any circumstances, enter their tent or go into their bags, packs, or pouches and
take anything out without the owner's explicit permission.

A magic item is the property of the EH that created it. It is the EH's will that
the item be able to be circulated around the Realms by theft, as a gift, as part
of an inheritance, or any other means so long as it occurs at an event.

IC theft, not gift, of stealable items in OOC situations, not at an event, is
not acceptable and will not be upheld.

## In-Game Items ## {#in-game-items}

### Currency ### {#currency}

Various groups and nations issue different currencies for use as treasure and to
pay for goods and services in-game. Stamped wooden nickels are commonly used, as
are plastic and metal coins, and laser-cut acrylic. Denominations vary from
currency to currency, but most people will refer to "one gold" as the standard
unit of currency. Some issues are backed, meaning that if you accumulate enough
of any one currency, you can trade those coins in to the issuer for goods,
weapons, or services. Many older coinages and silver pieces are not backed, and
while they're still in-play, many people either heavily discount them or don't
accept them at all. Issues and worth of coins fluctuate. If you're in doubt as
to what a coin is worth, ask the merchants and the gamblers. Realms currency
cannot be counterfeited; to do so is cheating.

### Silver Weapons ### {#silver-weapons}

Silver weapons are created by players with the spell [Reforge](#reforge) or are
released by EHs.

### Magic Items and Magic Weapons ### {#magic-items-and-magic-weapons}

All magic items and magic weapons are stealable.

It is recommended that magic weapons be made with a blue-colored striking surface, so that they
are distinctive. [No permanent magic item may be issued by anyone except an
EH](#rules-for-event-holders).

Should a magic item or weapon become broken or disenchanted at an event, it
requires a repair through the [Reforge](#reforge) spell to return to a
functioning state. Additionally, magic items which have been revoked by their
EH/creator are no longer considered magic items and also revert back to the EH
who created them. Magic weapons that are currently in existence may be re-bladed
without the use of a [Reforge](#reforge) spell, to repair safety issues, by
first contacting and gaining permission from the issuing or backing EH.

### Event-Stealable ### {#event-stealable}

The props for certain spells and items as designated by the EH are considered
"Event-Stealable," meaning that they are stealable treasure during an event, but
should be returned to their OOC owner at the end of an event. Before you leave
an event site, you must return (to the best of your ability) any items marked as
"Event-Stealable" to the EH or MM. Props for PC spells are returned by the EH or
MM as an OOC courtesy, and will be done so without revealing the identity of the
thief. If you have a spell that has an event-stealable prop as a component, you
may replace it without penalty at the next event if it is not returned to you
for whatever reason. You can never declare permanently stealable items as
event-stealable.

### Torches ### {#torches}

Anyone may carry and use a normal red chemical light stick, or equivalent, as a
one handed item. It is only considered a one handed item when it is being used
as a light source. Electronic light sources must be checked in before use and
may be pulled if they may be too bright or unsafe for the event. These lights
may only be visible while being wielded.

Torches are subject to the [Disrupt Light](#disrupt-light) spell. The torch
wielder may not wield any torch for 5 minutes after hearing the spell. If using
chemical light sticks, the torch wielder must also carry a bag large enough to
hold all of the glow sticks they will use and thick enough to prevent any light
from escaping. It is the torch wielder's responsibility to know what the Disrupt
Light spell is, how to recognize it, and how to respond to it.

# Character Creation # {#character-creation}

## The Social Structure ## {#the-social-structure}

The Realms is not governed by a single kingdom. Each nation has its own
hierarchy and structure. There are no hard rules for governing the social
structure. Claiming land and titles is anyone's prerogative. The social
structure really only has one rule: if you can back up your title or claim then
you deserve to hold it. If you can't, then you should have nothing to complain
about if you get put in your place.

## Creating a Character ## {#creating-a-character}

If you are already familiar with role-playing in general, or with live action
role-playing specifically, you probably already know how to make a character. If
you are new to the concept of role-playing, the following questions might help
you establish the traits and characteristics of your PC. You should try to
answer the questions for yourself, but some suggestions are provided.

_What is your character's species?_ There are as many species available in the
Realms as there are minds to create them. In the Realms, there are no
restrictions on what species you can play. The only rule is that you may gain no
special benefits or specialized abilities for playing a certain species or
person (the only way to gain supernatural powers is through legally released
items, the spell system, or by being appointed a [Knight of the Realms or a
Knight of the Eternal Flame](#knights-and-knightly-powers)). If you create your
own "species," you would do well to consider its mannerisms, average age, codes
of ethic, etc. Of course humans are the easiest to play, since you are probably
human.

_What is your character's age?_ If you are playing a human character, it is
usually best to pick an age near your own. Other species may have different
average ages.

_Why is your character an adventurer?_ Most people of the Realms prefer the
relatively safe life of a farmer or craftsman. Why has your character left home
to join in the rather hazardous occupation of hero? The answer to this might
give you some valuable insight into the persona of your character.

_What is your character's background?_ There is no limit on where your character
came from. As was stated before, your character can come from anywhere, so long
as they gain no IC benefits from it.

_Does your character have a lifetime goal, dream, or driving force?_ Goals
define characters well, and how far they are willing to go to attain that goal
rounds them off.

## Being a Fighter ## {#being-a-fighter}

Fighters are capable of using any weapon style, to the limits of the [Wielding
Weapons](#wielding-weapons) section. They may use up to 2-point armor (heavy
armor) for each hit location. They are not considered enchanted beings unless
stated in the [Enchanted Beings Caveat](#enchanted-beings).

## Being a Spellcaster ## {#being-a-spellcaster}

The magic system is based on a simple path setup. There are many different paths
available to a spellcaster, with each path consisting of a list of spells. A
spellcaster may choose to take as many as three paths of spells as they
progress, with weapon and armor use becoming more restricted as the number of
paths learned increases. More information on becoming a spellcaster can be found
in [Spellcaster Basics](#spellcaster-basics).

## Multiple Characters ## {#multiple-characters}

You may have more than one character in this game. However, you may only play
one PC per event unless you have EH permission to play multiple characters.

## Knights and Knightly Powers ## {#knights-and-knightly-powers}

There are many knighthoods in the Realms. It is a great honour to be appointed
as a knight, and sometimes this honour comes with special powers. For their
service to the game, a Knight of the Eternal Flame may, once per calendar year,
call "Knight" to protect themselves from one attack. A Knight of the Realms is
allowed to do this once per event. Event Holders may choose to grant powers to
members of other knighthoods.

# Magic in the Realms # {#magic-in-the-realms}

## Basic Magic Effects Everyone Should Know ## {#basic-magic-effects-everyone-should-know}

Even if you are not a spellcaster, and have no desire to become one, there is
still some basic information that you are responsible for understanding. In
nearly all cases, a spellcaster casting a spell on your PC should explain,
either through their Verbal Component or as an aside to you, how the spell
affects your PC. Even fighters should read the magic system and spell
descriptions, so they have a basic understanding of magic in the game and how it
might affect them. No spell effect may be ignored unless otherwise specified in
the spell system.

The following are some rules about spells that can have an effect on your
character on a day-to-day basis. They are included here with some basic
information about how they affect you both IC and OOC.

### Healing Spells ### {#healing-spells}

When your character is injured or killed, only magic can fix the damage. To
raise a character means to return a character to life. Raising a character
repairs any injuries to limbs the character may have taken prior to their death
and also nullifies the effects of any poisons in their system at the time of
their death.

* [Combat Raise Dead](#combat-raise-dead): this three-word spell will raise a
  character.
* [Cry of Life](#cry-of-life): the call of "All in the sound of my voice, rise
  and fight" raises all dead who hear it.
* [Group Healing](#group-healing): allows a spellcaster to make a large circle
  through which they cast a healing spell upon every character in it.
* [Heal Limb](#heal-limb): this spell allows one wounded limb to be magically
  healed. The spellcaster will let you know when your character's limb has been
  repaired.
* [Raise Dead](#raise-dead): this spell raises a character. This spell will not
  work if there is a weapon within 10 feet of the spellcaster. Inform a
  spellcaster the spell has failed if you know about a weapon near enough to
  disrupt the spell. A bow is not considered a weapon, but arrows are considered
  weapons.

### Regeneration ### {#regeneration-bmeesk}

Some spells grant the ability to regenerate from death. This takes 120 seconds.
While this happens, your wounds begin to heal. Until 120 seconds have passed,
this grants no benefit. A blow to any kill location on a dead body will cause a
regeneration count to reset no matter where the killing blow was inflicted.
[Impaling](#impale) stops regeneration; the count resets when the weapon is
removed. Regenerating from death heals all healable wounds on the body. If
examined by another person, wounds can be seen to be regenerating. If you are
[diseased](#disease), it takes twice as long to regenerate. The effect ends when
you are raised.

### Combat Spells ### {#combat-spells}

Certain combat calls ([Lightning Bolt](#lightning-bolt-call) and [Magic
Missile](#magic-call)) involve a prop that is thrown at a combatant. After the
prop has come to rest it is only a physical representation of magic and cannot
be moved or touched other than by the person who threw it. It may be seen and
guarded but not purposefully hidden (e.g. putting a bucket over it). The prop is
not considered a weapon and does not cause [Spell Failure](#spell-failure)
except while the spell is active (i.e. from when the prop is thrown until it
comes to rest).

### Undeath ### {#undeath}

Certain spells make your character undead for their duration. You may not refuse
to have your PC animated as an undead, unless under [certain magical
effects](#protect-the-soul). You are now playing an undead version of your PC
and have access to all their spells, abilities, and knowledge. Your character
will remember what happened to them while they were animated as an undead. While
undead, your character cannot cross a [Circle of
Protection](#circle-of-protection), other than one created by the person who
animated them (represented by a circle of white rope on the ground), or advance
within 5 feet of someone casting [Ward: Undead](#ward-undead) or [Ward:
Enchanted Beings](#ward-enchanted-beings). Your character stops being undead,
and the spell ends, when you are slain and then raised, or simply raised. This
family of spells does not give you the ability to ignore weapon restrictions.
Many spells that turn you undead make you a thrall. Thralls act like your
character, but you must follow commands given by the spellcaster, and you must
act in their best interest, especially when interpreting their commands. You
cannot be compelled to forcibly break restriction, do anything unsafe, or
violate your OOC morals, and you can ignore those commands. If you have any
questions about this, ask the spellcaster when they cast the spell on you.

* [Animate Undead General](#animate-undead-general): if this spell is cast upon
  you when your PC is dead, your PC is animated as an undead thrall under the
  spellcaster's control. In addition your PC now has the ability to cast
  [Animate Lesser Undead](#animate-lesser-undead) with unlimited castings. The
  Undead General will die to any damaging magical attacks regardless of
  location. Your PC must follow the orders of the spellcaster.
* [Animate Undead](#animate-undead): if this spell is cast upon you when your PC
  is dead, your PC is animated as an undead thrall under the spellcaster's
  control.
* [Animate Lesser Undead](#animate-lesser-undead): if this spell is cast upon
  you when your character is dead, your character is animated as a lesser undead
  thrall under the spellcaster's control. Lesser undead cannot use any armor or
  spells regardless of what they normally have. [Animate Lesser
  Undead](#animate-lesser-undead) can be used to reanimate an [undead](#undead),
  but it returns the character to un-life, rather than life.
* [Walking Dead](#walking-dead): certain spells will make a PC's dead body walk
  without either returning them to life or fully animating them as undead. When
  a PC is under the effects of these spells, follow these guidelines:
  * Walk at a steady pace, do not run.
  * Keep any items on you that you would retain if picked up and dragged.
  * Move to the destination as directly as possible without taking an OOC unsafe
    path. If the only option is to move into an OOC unsafe situation, then the
    spell ends and the body falls to the ground.
  * If another character interferes with the body, such as by attacking them or
    physically stepping in their way to block them, the effect of the spell
    ends.
  * The PC cannot take any actions other than walking; no attacking, searching,
    picking up items, using magic items, or drinking potions.
  * The PC is considered to be Undead while the effect lasts. See the [Undead
    Caveat](#undead).
  * The main difference between various [Walking Dead](#walking-dead) spells is
    in where the PC must walk to.
    * [Beckon Corpse](#beckon-corpse): the PC will stand and walk to the
      spellcaster as long as they are chanting. If the PC is forced to stop, the
      spellcaster may have the option to regain their attention and resume
      chanting, at which point the PC will get back up and continue walking to
      the spellcaster.
    * [Zombie Walk](#zombie-walk): the PC will follow the spellcaster until the
      spellcaster either ends the spell, the spellcaster attacks someone, or
      they are attacked.

### Potions ### {#potions-bmeesk}

Potions are disposable magic items that anyone can use. Common forms that
potions can take include, but are not limited to something that must be consumed
or a scroll that must be read or ripped. In all cases the potion needs to be
administered by a living or animated character, and after it is used, cannot be
used again.

* [Potion of Acid](#potion-of-acid): if your PC is dead, and someone indicates
  they are using an acid potion on you, your PC takes 200 body destroying blows.
  See [Strange Brew](#strange-brew) for details.
* [Potion of Combat Raise Dead](#potion-of-combat-raise-dead): it raises a dead
  character when used.
* [Potion of Heal Limb](#potion-of-heal-limb): it heals all of a character's
  injured limbs when used.
* [Potion of Repairing](#potion-of-repairing): it repairs up to two points of 
  armor per hit location in 30 seconds when used.

### Blacksmith Spells ### {#blacksmith-spells}

Certain spells repair damaged armor and broken items.

* [Repair Armor](#repair-armor): this will restore one hit location of
  non-magical armor that has been damaged. For example, the armor on one arm,
  the chest of a shirt of chainmail, head armor, etc.
* [Repair Item](#repair-item): this will restore non-magical armor, weapons, and
  other items that have become damaged or destroyed. This spell has a Verbal
  Component, and it fixes the entire item. For example, all hit locations
  of someone's armor, a bow, a boulder-crushed weapon, a shield, etc.

### Other Spells and Things You Should Know ### {#other-spells-and-things-you-should-know}

* [Create Poison](#create-poison): if you ingest food or drink that is poisoned
  through the use of this spell, you will be handed a scroll upon which is
  written the poison's effect. You must follow the scroll's instructions
  completely. It will either cause your PC to die,
  sleep deeply, or tell nothing but the truth. All effects other
  than death are short-lived.
* [Cure Disease](#cure-disease) or [Potion of Cure
  Disease](#potion-of-cure-disease): these spells cure a character of a
  [disease](#disease).
* [Immunity to Poison](#immunity-to-poison): this spell makes your PC immune to
  the very next poison or [poison attack](#poison) that would otherwise affect
  them. It works only once per use of the spell. Call "Immunity to Poison," when
  you use this spell's effect.
* [Light](#light): you may not take the light out of verbal communication range
  of the spellcaster.
* [Pas](#pas): this spell creates a temporary truce. If you accept the offered
  bribe you are magically bound to not attack the spellcaster for 60 seconds
  unless you are attacked.
* [Protect the Soul](#protect-the-soul) or [Gentle Repose](#gentle-repose):
  these spells will protect your PC from possession, [Animate
  Undead](#animate-undead), and the like.
* [Speak With Dead](#speak-with-dead): this spell will allow the spellcaster to
  ask a dead character a single question per casting. Your PC MUST answer
  truthfully or abstain, using the words, "Yes," "No," or "Abstain."

## Spellcaster Basics ## {#spellcaster-basics}

### Spellbooks ### {#spellbooks}

All spellcasters must have a spellbook, which records details of the spells the
spellcaster knows. It is a marshaling tool and cannot be stolen from the PC. A
spellcaster must have their spellbook on their person in order to cast spells.

* The beginning of each spellbook must have the spellcaster's IC and OOC name,
  their current weapon restriction, whether the spellcaster wears armor or not,
  and whether the spellbook is IC or OOC.
* Next, the spellbook must have a listing of each spell the character has
  learned, in order; each spell's circle; and a note if any spell is unlearned.
* The spellbook must have a description of each spell known - including specific
  components learned for each spell - to which the spellcaster may refer as
  needed during play.
* Lastly, the spellbook must list who taught the spell to the spellcaster and
  the date the spell was learned on either the page of the spell's description
  or the spell list.

Spellcasters may have a Spell Mastery section at the end of their spellbook. The
Spell Mastery section is a list of spells which the spellcaster has learned
during their PC's lifetime. An entry in this list includes the spell's name, who
taught the spell to the spellcaster, and the date it was learned. Spells in this
list cannot be cast, unless they are currently in a spellcaster's path.

Spellbooks may be declared IC or OOC, and so noted on the title page ("IC" or
"OOC" in large, clearly written letters). Any change in a spellbook's IC or OOC
status must take place between events, but requires no particular time to make
any such change.

If a spellbook is declared IC, it can be read by other characters, and found and
perused in a search. Spellcasters have no option to refuse to reveal their IC
spellbooks, provided they are legally found in a search of the spellcaster's
person.

If a spellbook is declared OOC, the information summarized above is OOC
information only and exists solely as a marshaling tool. It cannot be read by
other characters or discovered in a search. Other information written in
spellbooks may be read by others, at the owner's discretion, such as rune sets,
history, or lore.

A player may choose to or need to replace their character's spellbook. This
could be due to such situations as loss, damage, illegibility, or a simple
desire to improve it. The player may create a new spellbook with contents as
close as possible to the prior one. In the case of swapping out an old spellbook
which is still available, the required information in it must be copied over to
the new one. In the case of the old spellbook being unavailable or unreadable,
then the player must make their best effort to recreate the information in the
original. At the next event where they play that character, they must inform the
Event Holder or Magic Marshal and have it inspected. At the beginning of the
spellbook, where their names and weapon restriction is written, they must note
that they are replacing their spellbook along with the date and the signature of
the inspecting Event Holder or Magic Marshal. A player may change the path and
circle where spells and alternatives to spells sit in their spellbook as a part
of replacing their spellbook, so long as the spell build remains legal. The
circle must remain the same when moving alchemy or regional magic.

In all cases, spellbooks remain non-stealable items.

### Knowledge of the Magic System ### {#knowledge-of-the-magic-system}

All spellcasters are responsible for knowing how the magic system works,
specifically the spells they can cast. A spellcaster who misuses their spells is
not allowed to claim ignorance as an excuse.

### Spell Resets ### {#spell-resets}

During some Realms events, you may be told a spell reset has occurred. This
means some portion of your magical resources have been restored to your PC. If
the EH does not make any additional clarifications to the spell reset, it means
that the spellcaster may reset every spell they have (see [Resetting a
Spell](#resetting-a-spell).)

### Resetting a Spell ### {#resetting-a-spell}

When you reset a spell, you regain all spent castings of the spell being reset,
but maintain any choices that were made at the start of the event. If you reset
a spell you have learned multiple times, you regain spent castings up to the
number you have learned. If the spell gives you points to spend, such as
[Familiar](#familiar) or [Implement](#implement), you regain the number of
points that the spell reset gave if you spend the points directly, or if you
spent the points on abilities, you can refresh up the the reset points worth of
abilities you purchased from that spell.

Any ongoing effects from previously cast spells are ended. A spell is considered
ongoing if it may still have an effect (e.g. sash spells, [Enchant
Armor](#enchant-armor) or [Immunity to Poison](#immunity-to-poison)). You must
notify any recipients of your spells the effect ended as soon as is reasonably
possible. Alternatively, you may choose to spend a use of the spell to prevent
the ongoing effect from ending (for example, if you can't find the person you
cast Enchant Armor on to tell them the effect ends, or you don't want to your
[Embrace Death](#embrace-death) spell to end, even temporarily).

### Resetting Alchemy and Regionals ### {#resetting-alchemy-and-regionals}

If [Regional Magic](#regional-magic) or [Alchemy](#alchemy) is reset, they are
reset in the same way that spells are. An EH may designate some regionals or
alchemy can not be reset.

### Checking In ### {#checking-in-spellcasters}

As mentioned previously, a spellcaster must [check in their spellbook with the
EH or the appointed MM](#attending-an-event) before using or learning spells.

### Weapon Restrictions and Magic ### {#weapon-restrictions-and-magic}

Weapon restrictions are a matter of game balance. See below:

+----------+----------+--------------------+--------------------+--------------------+
| Level    | Paths    | Single Weapon or   | Florentine, Weapon | Bow or Javelin     |
|          |          | Shield             | & Shield           |                    |
+==========+==========+====================+====================+====================+
| Light    | 1 Path   | 5' maximum         | 5'2" combined      | Allowed            |
|          |          |                    | length             |                    |
+----------+----------+--------------------+--------------------+--------------------+
| Medial   | 2 Paths  | 3' maximum         | 3'6" combined      | Allowed            |
|          |          |                    | length             |                    |
+----------+----------+--------------------+--------------------+--------------------+
| Severe   | 3 Paths  | 18" maximum        | Not allowed        | Not allowed        |
+----------+----------+--------------------+--------------------+--------------------+

Table: Weapon Restriction Levels

Your weapon restriction changes the moment you learn an additional path of
magic. Spellcasters starting out know only one path (as they only know a single
spell), and thus their restriction is Light.

_For example:_ Roderick is currently a one-path with five spells. He decides
that using a bow and having more magic is more important than his
hand-and-a-half, so he decides to keep learning spells. As soon as he learns his
6th spell, his restriction changes over to being Medial.

### Breaking Weapon Restriction ### {#breaking-weapon-restriction}

A spellcaster may carry any weaponry they want, as long as they don't wield
them. In this context, wield means to make use of the weapon in any way,
including hitting someone or blocking a blow, whether intentional or not. To
[wield a weapon](#wielding-weapons) outside of your weapon restriction is called
"breaking weapon restriction".

If a spellcaster purposely breaks their weapon restriction, they suffer the
consequences of their actions. They immediately lose all of their spells and
become a fighter. Any lingering effects, such as [Circle of
Protection](#circle-of-protection) (but not a [Circle of
Healing](#circle-of-healing) or [Mystic Forge](#mystic-forge)), last until
broken or when spells next reset before going away. They are no longer a
spellcaster, and function as a non-spellcaster in all ways for a minimum of one
year. After that year is over, they may then decide to return to being a
spellcaster, but must start over from scratch.

If they break their weapon restriction without realizing it, such as a blow
being blocked by a weapon they are carrying for a friend, they have the option
of either the previous penalty or taking the blow that was blocked. This
decision must be made immediately.

### Armor ### {#armor-restriction}

Armor restrictions are a matter of game balance. Spellcasters may only use light
armor (1-point) but at a cost to wear it. See below:

+--------+-------+------------------------+
| Level  | Paths | Cost                   |
+========+=======+========================+
| Light  | 1     | Free                   |
+--------+-------+------------------------+
| Medial | 2     | Top spell in each path |
+--------+-------+------------------------+
| Severe | 3     | Top spell in each path |
+--------+-------+------------------------+

For example, a spellcaster with two paths would be able to have 1st through 4th
and 1st through 5th, and call armor. This may only be done once; you cannot
sacrifice the two highest spells of each path in order to be able to wear heavy
armor. A spellcaster may choose to start their spellcasting career with this
ability, in which case it must be noted in their spellbook. If not, they must
spend one event without learning a spell for each path they know in order to
gain the ability to wear armor. Similarly, it takes one event without learning a
spell or changing weapon restrictions to give up wearing armor and regain the
ability to learn their sacrificed spells.

If a Light restriction spellcaster learns more spells and becomes a Medial
restriction spellcaster, upon learning their first spell in their second path,
they must choose to either lose the ability to wear armor or their 5th circle
spell in their first path.

A spellcaster who uses armor (calls "Armor" in response to a blow) in violation
of their armor restriction is immediately faced with the same consequences as
one who has [broken their weapon restriction](#breaking-weapon-restriction), and
they can refer to that section for details.

## Learning and Unlearning Spells ## {#learning-and-unlearning-spells}

### Choosing Spells ### {#choosing-spells}

The first step in choosing spells is to decide which 1st circle spell you want
to learn. Each path of magic is a list of spells of increasing circles.

A spellcaster may choose one of four options for each circle of a path:

* Any of the spells listed for that circle
* Any spell of a lower circle
* [Regional Magic](#regional-magic)
* [Alchemy](#alchemy)

When a spellcaster learns their first path of magic, they gain spell slots for
each of 1st through 5th circles, in order. If they learn a second path, they
gain spell slots for each of 1st through 6th circles, in order. If they learn a
third path, they again gain spell slots for each of 1st through 5th circles. The
same spell may be taken multiple times.

### Learning Progression ### {#learning-progression}

At every event a character attends, they have the possibility of learning one or
more spells. If they do not learn a spell at that event, for whatever reason,
the opportunity is wasted and they may try again at the next event.

### Learning a Spell ### {#learning-a-spell}

At events you attend, your PC may choose to learn spells. A spell may be learned
at any time during an event, so long as one has not already unlearned a spell at
that event. To learn a spell, you must find an appropriate number of teachers
eligible to teach you the spell. You may learn as many spells per event as you
are able to prove understanding for, provided you can find enough teachers for
each spell. Your first spell at an event needs one teacher, your second needs
two teachers, your third needs three, and so on. Each teacher can only help to
teach you a single spell per event, however, when relearning spells in a PC's
spell mastery, the PC may give themself one signature for each spell. You do not
need to check in a spellbook if you are learning for the first time, as you were
a fighter when you checked in.

Teachers are responsible for making sure you understand all of the rules for the
spells they teach, and teachers may be held liable for their students if the
teachers did not teach spells properly. When they are satisfied, they will
indicate this by legibly signing their name in your spellbook. A teacher may
refuse to sign if you seem unable or unwilling to understand the rules. Once all
of your teachers for a spell have signed your spellbook, you officially know the
spell and can cast it.

There are four types of people who can teach you a spell:

* A PC who has that spell in their list of learned spells
* A PC who has that spell in their Spell Mastery and is casting Mentor
* The event's Magic Marshal, who will usually require you to complete a quest
  for this
* You can teach yourself spells listed in your Spell Mastery

### Unlearning Spells ### {#unlearning-spells}

Any number of spells may be unlearned at check-in of an event, and only at
check-in. If you have checked in your spellbook with the Magic Marshal and made
the decision not to unlearn spells, you may not unlearn for the rest of the
event. You may not learn
a spell at an event where you are unlearning any spells. If you unlearn all the
spells in a path, then you no longer have that path, and your restriction
immediately changes to match your current number of paths. When you have learned
2 paths, you may not unlearn your first path unless you are also unlearning the
second path completely. Likewise when you have learned 3 paths you may not
unlearn both paths of magic that only include circles 1-5; you may only unlearn
one of those paths unless you are also unlearning all of your path that includes
circles 1-6. Upon unlearning all your spells, you are no longer considered a
spellcaster. Any given spell may change the path it is in at the beginning of an
event by showing your replaced spellbook to the magic marshal with the spell in
the desired path and circle.

For example, Ethan is a 2 path with some alchemy. He
decides that he doesn't like healing very much, and he misses using armor and a
hand-and-a-half weapon. At the next event he attends, he makes a new spellbook
where he has moved all of his alchemy to be in one path (maintaining the correct
circle where they were learned). At the beginning of the event, when he checks
in his magic, he tells the Magic Marshal he wants to unlearn his second path.
Once the Magic Marshal signs the new spellbook with this change, he begins the
event at Light restriction with the ability to use up to one point of armor.

Fiona is a 3 path, but wants to change her 6th circle spell. The next event she
attends, she unlearns her 6th circle spell, remaining under a Severe restriction
but without access to a 6th circle spell for the event. At the next event she
can learn a new 6th circle spell.

Gunthar wants to become a fighter after being an spellcaster for many years, but
he doesn't want to give up the option of learning spells again later that year.
At the beginning of the next event he attends, he unlearns all of his spells and
starts as a fighter.

### Guidelines on Teaching a Spell ### {#guidelines-on-teaching-a-spell}

Teaching a spell is an important aspect of the game, and while you are
encouraged to roleplay and create an in-character experience, there are some
minimum requirements that must be met when teaching a spell.

As a teacher, you must ensure the learning player understands the following:

* The entirety of the spell.
* Specific rules within the spell.
* Caveats related to the spell.
* Uses and components of the spell.
* Any known interactions with other spells or rules that are atypical from
  common use.

Additionally, you must:

* Confirm the spellbook is legal.
* Confirm the spellbook indicates what circle and path the new spell is being
  learned under.
* Confirm the spell entry includes the current date it is being learned and a
  description with all specific components spelled out.

When you teach a spell, you are asserting that the player whose spellbook you
are signing understands how to cast the spell.

## The Basics of a Spell ## {#the-basics-of-a-spell}

### Spell Components ### {#spell-components}

Spells have components that are necessary in order to cast the spells. Some are
specific, and every player must use the same component to make sure that
everyone understands what spell your PC is casting. Some are left open and the
spellcaster can choose any component that fits the description. The spell
descriptions list the minimal spell components required for each spell. The game
does not limit the spellcaster's freedom to define their own magic, so the
required components are as succinct as possible. You may add more requirements
for shtick if you like, but you cannot leave out any of the minimums. A
spellcaster may cast any number of spells at a time, provided at most one has a
verbal component.

Here are the definitions of the different types of components:

* Verbal Component (VC): These are the words you have to say while casting the
  spell. It is important that you enunciate your verbal component and say it
  loud enough so the person or persons affected can understand what you are
  saying. The verbal usually explains what spell you are casting. If the target
  cannot understand you, they are not affected by the spell. Any required verbal
  must be recited without stopping or errors, with the exception of OOC
  explanations (such as combat calls). A verbal must be written within the
  spellbook in order to be used, and it must meet the criteria for the spell.
  Requirements such as "Talk to the EH" or "an explanation" can simply be
  written as such. Multiple verbals may be written for the same spell, and the
  spellcaster may choose which to use at casting time. Verbals may be changed
  between events.
* Material Component (MC): There are three types of material components;
  required, disposable, and foci.
  * Required components are specific to a spell, such as bean-bags, foam, or
    duct tape blocks for the [Magic Missile](#magic-missile) spell. These
    components cannot vary from what is listed.
  * Disposable components are up to the player, but they must be something that
    is consumed or thrown away with every casting of the spell. A disposable
    component is something that the spellcaster could easily hand to the MM for
    inspection.
  * A focus is a component that is not consumed or thrown away. Often, it is
    necessary for the spellcaster to brandish a focus while casting certain
    spells. The spellcaster may have a single focus for all of their focus-based
    spells. A focus is something that another player or NPC can obviously
    identify as the focus when the spellcaster is using it for a spell. A
    spellcaster must also be able to hand this to the MM for inspection. A focus
    may not be a weapon.

  All MCs must be specified and written down in the PC's spellbook for every
  spell that they know and, except for foci, the component must be different for
  each spell. A spellcaster must have at least one uninjured hand to use a MC.
* Active Component (AC): These are actions that the spellcaster must take in
  order to cast the spell and must be performed at the time of casting. If the
  active component(s) of a spell are not performed throughout the entire casting
  process, the spell is not cast. Characters may add anything else for
  role-playing purposes.
* Duration: Unless otherwise noted in the spell description or caveat, all
 spells end when the event ends.

## Alternatives to Spells ## {#alternatives-to-spells}

These are similar to spells, but they don't have any particular circle. You can
learn and unlearn them as if they were spells, and you can put them into your
Spell Mastery as if they were spells.

### Regional Magic ### {#regional-magic}

At any given event, the EH may wish or require that certain magical abilities be
available to the players. One of the ways they can accomplish this is through
Regional Magic. Regional Magic is usually an additional number of spells that
spellcasters can choose from. Spellcasters can only choose from this list if
they had filled at least one spell slot with a Regional Magic spell. Regional
Magic is learned and unlearned just like any other spell, and may be learned
from anyone who knows it at any circle.

At some events, the Regional Magic your PC will receive is based on which circle
spell slot you filled with the Regional Magic spell. At some events, all of the
Regional Magic spells are the same, no matter which circle slot you filled with
Regional Magic. Others are completely random. Some EHs may require you perform
certain actions before gaining the Regional Magic. The details of Regional Magic
are left entirely up to the EH. No Regional Magic spell will have a lingering
effect that lasts longer than the end of the event. One thing to keep in mind is
that while Regional Magic is more versatile, it is also more unreliable. An EH
may choose a different spell from the list, a new spell, or nothing.

### Alchemy ### {#alchemy}

A spellcaster may choose to take Alchemy in a spell slot instead of learning a
spell. This still requires someone to teach and sign off their spellbook. Any
spellcaster that knows Alchemy must include a points total on their spell list
page. Whenever a spellcaster learns Alchemy they receive points equal the chart
listed below. For example, if Samuel learns Alchemy in his third and fifth
circle slot, he will have 12 points with which to make potions. All spellcasters
know three basic potions, listed below, and can learn more with the [Strange
Brew](#strange-brew) spell. All Potions are governed by the Enchanted Items and
Potions Caveats.

```{=html}
<table style="width:67%;">
  <colgroup>
    <col style="width: 31%">
    <col style="width: 6%">
    <col style="width: 6%">
    <col style="width: 6%">
    <col style="width: 6%">
    <col style="width: 6%">
    <col style="width: 6%">
  </colgroup>
  <tbody>
    <tr>
      <th scope="row">
        Spell Circle
      </th>
      <td>1</td><td>2</td><td>3</td><td>4</td><td>5</td><td>6</td>
    </tr>
    <tr>
      <th scope="row">
        Points Gained
      </th>
      <td>1</td><td>2</td><td>4</td><td>6</td><td>8</td><td>12</td>
    </tr>
  </tbody>
</table>
```

Basic Potions

* (1 Point) [Potion of Mending]{#potion-of-mending}: Recipient receives a basic
  regeneration. This potion will have no effect on a living target.
* (1 Point) [Potion of Repairing]{#potion-of-repairing}: Repairs up to two 
  points of armor per hit location on a single character. The potion is poured
  or applied to the damaged armor and held there for a 30-second count. The
  armor does not need to be removed for the potion to be applied to it.
* (Special) [Power Potion]{#power-potion}: Resets a single spell of the
  recipient equal to half of the points spent to create it, rounded down. This
  cannot be used to reset a 6th circle spell. (Example: a spellcaster may spend
  10 points to make a potion to reset a 5th circle spell.) Each level of Power
  Potion requires either its own unique sigil or to be clearly marked with the
  spell circle it was created to reset.

## Caveats ## {#caveats}

Caveats are general rules that apply to all spells or spell effects of a similar
type. Each spell that is affected by a caveat is listed in the appropriate
place.

### OOC Calls ### {#ooc-calls}

These are special OOC calls to inform and describe specific IC game effects,
These calls do not break verbals, chants or other VCs as they are purely game
mechanics and martialing tools. All spells that utilize special combat calls
describe the specific call that must be made with its use.

### Chanting ### {#chanting}

Some spells require that their VC be chanted continuously for the duration of
the spell. To cast a chanting spell, the spellcaster recites the verbal once 
and then says the name of the spell. At this point, the spell takes effect. The
spellcaster can then recite their verbal for as long as they want the spell to
continue. OOC explanations (such as combat calls) do not interrupt these spells. 
For example, if a PC is chanting a [Merge](#merge) spell and is
hit by a weapon, they may call "No effect" without interrupting the spell; if a
PC is chanting a [Ward: Undead](#ward-undead) spell and a goblin hits their leg,
they can call "Leg" (or "Armor," or "Armored Cloak," etc.) without having the
spell end. The VC for these spells must be spoken clearly and loudly enough that
anyone affected by the spell can understand them. Chanting spells can be
disrupted by the 4th circle spell [Disrupt](#disrupt). It is the PC's
responsibility to know what the [Disrupt](#disrupt) spell is, how to recognize
it, and how to respond to it.

### Circles ### {#circles}

There are a number of spells that are considered circle spells. A circle spell
must be clearly defined by a length of rope that has been laid on the ground
with the ends overlapping. The ends cannot be tied together or secured in any
way, and the rope in general cannot be secured or bound in place or the [spell
fails](#spell-failure). Only one spell may be cast with a particular rope at a
given time, although after the spell ends, a different spell may be cast with
that rope. The rope does not need to be laid down in a circular pattern.
Although a given circle spell may have a specific way of being broken, all
circle spells are broken if the rope is jostled enough to move the ends apart
by a character able to cross it. Any circle spell can be suspended by the 4th
circle spell [Disrupt](#disrupt). It is the PC's responsibility to know what the
[Disrupt](#disrupt) spell is, how to recognize it, and how to respond to it.

### Compulsions ### {#compulsions}

Some spells are able to magically compel a character to act in a way the player
would prefer they did not. Compulsions can be ignored if they are humiliating or
exceedingly difficult commands such as "Kiss my feet" or "Move that wall ten
feet to the left." They also cannot violate OOC mundane laws or ethical codes.
They may not force a spellcaster to break their weapon restrictions.
Additionally, if the character has been turned undead, they cannot be compelled
to communicate knowledge gained before they were made undead.

### Enchanted Beings ### {#enchanted-beings}

All spellcasters, undead, and certain creatures are considered to be enchanted
beings. Normal fighters are only enchanted beings if under the effect of certain
spells, as per the [Undead Caveat](#undead). Enchanted beings are affected by a
certain number of spells, while non-enchanted beings are not. These spells
include [Circle of Protection](#circle-of-protection) and [Ward: Enchanted
Beings](#ward-enchanted-beings). By definition, any undead creature
is an enchanted being.

### Enchanted Items ### {#enchanted-items}

Some spells create or enchant items, or are enhancements that affect players.
These spells create magical effects, but are not potent enough for the items
bearer or the target of the enchantment to be considered an Enchanted Being, nor
is their magic potent enough for the item to be affected by the spell [Circle of
Protection](#circle-of-protection) once they are cast. If the MC of these spells
are disenchanted (through the spells [Disenchant](#disenchant) or the [Strange
Brew: Potion of Disenchant](#potion-of-disenchant) option) the spell will end,
rendering the MC inert. Magic items are not covered by the Enchanted Items
Caveat.

### Potions ### {#potions-caveat}

A spellcaster who learns [Alchemy](#alchemy) must have a page in their spellbook
listing the sigils that they will use to label potions. Each type of potion they
can make, must have a unique, distinguishable sigil. When a potion is made, the
spellcaster must put their legible signature, the appropriate sigil, and the
date upon the container. Once created, all potions are considered stealable
items. A potion can be a represented by a liquid, lotion, elixir, magical food,
or anything else, as long as it is safe to be administered in a combat
situation. Potions must be directly applied to the recipient. They may not be
thrown, dropped, or remotely applied in any manner. The spellcaster need not be
present in order to use their potions.

Potions take a 60 second process called Brewing to be cast. At the beginning of
an event, the spellcaster must describe in general terms their process of
brewing. An alchemist may brew as many potions as their spells allow during the
same 60 seconds. If they wish to create more later, they must begin the process
again. If the alchemist is interrupted while brewing, Alchemy points are not
lost, but the potions will not be made, and they must start over again in order
to make any potions. Although not required, use of additional props or
role-playing is encouraged for this process.

No potion created by a player can carry over from one event to another; it
expires at the end of the event at which it is cast. The PC may choose whether
or not to further limit the lifespan of a potion when it is brewed by writing a
distinct expiration time among the required spell information on the container.
Any potions lacking a specified expiration time last until the end of the event.

### Precast ### {#precast}

Spells a spellcaster has learned with this caveat allow the spellcaster to begin
play with that spell active on them or their gear. This represents the character
casting the spell before arriving at the event. This uses up one use of the
spell. Any material component that must be on the caster for the spell to work
must still be present as per normal (such as sashes). A spell "precast" in this
way may only be cast on the caster, and may not be "precast" on anyone else.

### Regeneration ### {#regeneration-caveat}

Some spells grant the ability to regenerate. When this ability is triggered (by
death, being wounded, etc.), the target's wound(s) begin to heal. Until the
specified amount of time has passed, this grants no benefit. A blow to any kill
location on a dead body will cause a regeneration count to reset no matter where
the killing blow was inflicted. Impaling stops regeneration; the count resets
when the weapon is removed. Regenerating from death heals all healable wounds on
the body. If examined by another person, wounds can be seen to be regenerating.
Regeneration that brings a character back from death or soul loss takes 120
seconds. If you are [diseased](#disease), it takes twice as long to regenerate.
You may only be under the effects of one basic regeneration and one advanced
regeneration at a time. If more than one source is causing you to regenerate,
you may choose which of those spells is causing you to regenerate. A
regeneration will only work on someone who is soulless if the spell directly
states that it can.

#### Basic Regeneration #### {#basic-regeneration}

A basic regeneration is a regeneration from death where the regeneration begins
when the target dies (or upon casting the spell if the target is already dead).
The effect is considered used if the target is raised before their regeneration
is complete.

#### Advanced Regeneration #### {#advanced-regeneration}

An advanced regeneration is a regeneration from death (or soul loss, if
specified in the spell description) which begins when the character dies (or is
rendered soulless, if it brings a character back from losing their soul). Each
time they die, the spellcaster may choose to double the length of their
regeneration (to 240 seconds, or 480 if diseased). The regeneration is not
considered used if the target is raised before their regeneration is complete.

### Spell Sash ### {#spell-sash}

Some spells require a Spell Sash be worn to signify that a specific spell is
affecting the wearer. A Spell Sash may be constructed as a sash, a tabard, or a
belt favor. A Spell Sash must have the name of the spell clearly written on it.
Putting the Spell Sash on the target of the spell, or touching it if the target
is already wearing it, is considered to be an Active Component of the spell. A
Spell Sash is not stealable and stays on the target of the spell until the spell
is ended, at which time it is removed and returned to the spellcaster at the
earliest convenient time. Either the spellcaster or the target may choose to end
the spell by removing the Spell Sash, and it is also ended early if the Spell
Sash is [disenchanted](#disenchant). The Spell Sash must be worn in such a way
that another player standing 5 feet in front of them can recognise it, and may
not be covered unless the majority of their body is covered, such as by wearing
a concealing cloak.

### Spell Failure ### {#spell-failure}

Some spells have fail conditions in their description. Any spell that fails due
to its fail condition expends the casting of that spell.

### Suspension ### {#suspension}

While a spell is suspended, the spell has no effect and does not function until
the suspension ends. In addition, a suspended spell can be ended any way it
normally could. It is the spellcaster's responsibility to notify anyone else who
is affected by this (such as any players carrying potions that the spellcaster
created). When the suspension ends, any spell which has not already ended will
resume functioning as per normal.

### Undead ### {#undead}

Some spells will animate a character as undead, allowing them to act as if
alive. A person is no longer animated if they are killed, but they are still
undead. An undead who is successfully raised (whether they are dead or still
animated) is brought back to life and is no longer undead. Animating a character
who is undead but slain back to their previous type of undead is referred to as
"reanimating". All undead are affected by the spells: [Ward: Enchanted
Beings](#ward-enchanted-beings), [Ward: Undead](#ward-undead), and [Circle of
Protection](#circle-of-protection) (unless the person who animated the undead
cast the Circle of Protection). Enchanted beings and items still cannot cross a
Circle of Protection that is not their own, regardless of undead status. A PC
will remember what happened to them while
they were an animated undead. A PC may be animated as undead if they are
[diseased](#disease), but this does not cure them of the disease. Unless
otherwise specified, an undead can be harmed as per normal and gains no special
protections. If the target is still undead at the end of the event, they are
considered dead, and thereforee become soulless and receive a tick. Any spell
that ends or is suspended when raised is also ended or suspended while animated
as undead.

#### Thrall #### {#thrall}

These spells turn a dead (but not soulless) body into an undead creature
(referred to as a thrall) that will follow commands given by the spellcaster.
What kind of creature the thrall is is left up to the spellcaster, but it must
definitely look slightly non-human. The spell must be cast upon a corpse, and
the spellcaster must recite the verbal while helping the player into the MC for
the spell as appropriate (e.g. tabards or sashes worn correctly). If there is
already an appropriate MC on the person, the spellcaster may simply touch the
recipient of the spell. When the spell is cast, the spellcaster must explain to
the target what it means to be undead. They also must explain the target will
obey all direct commands given by the spellcaster that follow the [Compulsions
Caveat](#compulsions), but the target is otherwise in full possession of their
faculties. In addition, the target has the spellcaster's best interests and
intentions in mind. If the spellcaster gives a command that goes against the
Compulsions Caveat, the target can ignore the command. These spells simply
reanimate a person under the effects of [Embrace Death](#embrace-death) and do
not grant any additional powers or control.

### Walking Dead ### {#walking-dead}

The spells [Beckon Corpse](#beckon-corpse) and [Zombie Walk](#zombie-walk)
enchant a corpse to walk without either returning them to life or being fully
animated as undead. A corpse under these effects must move at a walking pace
with their hands above their heads to their destination as dictated by the
spell.

They keep any items on them that they would retain if dragged, whether or not
the items are stealable. If anyone directly interferes with their movement or
attacks them, they fall to the ground and the effect ends. The Walking Dead
cannot take any actions other than walking. They may not attack, search other
bodies, cast spells, pick up objects, use magic items, drink potions, or perform
any other action aside from movement. The effect does not stop regeneration or
other methods of returning to life or becoming fully animated from occurring.
Becoming alive or fully animated as undead ends the Walking Dead effect. Corpses
under these effects are considered to be [undead](#undead).

### Wards ### {#wards}

All Ward spells are also [Chanting spells](#chanting). Ward spells affect a
specific type of creature listed in the name of the Ward. When active, a Ward
spell keeps the spellcaster from being attacked by creatures affected by that
spell. A Ward spell affects creatures in front of the spellcaster, limited by a
180 degree hemisphere extending from shoulder to shoulder and outwards from the
chest. The Ward will not keep the targeted creature(s) from walking around the
spellcaster to attack others nearby. If the Ward affects them, the targeted
creature(s) must stay approximately five feet away from the spellcaster, but
need not retreat if the spellcaster advances upon them. To cast a Ward, the
spellcaster must hold their spell focus out toward the targeted creatures while
repeating the verbal. The verbal needs to make it clear what creatures are
affected by the Ward. For example, "Stay back undead. Stay back undead. Stay
back undead. Shoo," would be an appropriate verbal for the [Ward:
Undead](#ward-undead) spell. This spell will work for as long as the spellcaster
holds out the focus and keeps repeating the verbal. The people playing the
targeted creatures must be able to hear what the spellcaster is saying, so it is
up to the spellcaster to clearly and loudly chant their verbal. While casting
this spell, the spellcaster may not attack the targeted creatures. A being that
looks like it should be a targeted creature may not be (or may be immune to the
Ward). It is still the responsibility of the spellcaster to take any and all
weapon blows that hit them, even if the blows are from a creature they believe
should be affected by the Ward.

### Weapon Calls ### {#weapon-calls}

Each Weapon Call spell has a VC or AC which prepares the weapon with the special
combat call, using a casting of the spell. The casting is spent if the blow
lands an attack to a legal hit location. Otherwise, upon a parry or a miss, only
the preparation is lost and the use remains. If you are unsure whether the blow
landed, you must assume that it did. The spellcaster cannot cast a different
Weapon Call spell on a currently enchanted weapon until the first spell has been
discharged. The spellcaster's weapon may not be used by anyone else and still
retain the enchanted status. If someone other than the spellcaster attacks with
the prepared weapon, the preparation is lost and cannot be used unless it is
reapplied.

The spells that allow special combat calls are [Assassin's
Blade](#assassins-blade), [Armor-Piercing Weapon](#armor-piercing-weapon),
[Disease Weapon](#disease-weapon), [Enchant Weapon](#enchant-weapon), and
[Create Poison](#create-poison). These spells are mutually exclusive and cannot
be cast upon a weapon that already has a separate combat call. Weapon types,
such as axes or maces, are not covered by this caveat. None of these spells can
be cast upon a [Magic Missile](#magic-missile) or [Lightning
Bolt](#lightning-bolt-spell).

## Converting Spellbooks ## {#converting-spellbooks}

When a player has been gone for a while, or when the rules have changed at the
Event Holders' Council, a player might have a spellbook that is no longer legal
by the rules of the Omnibus. When a player arrives at an event with an illegal
spellbook, they must convert their spellbook to match the current rules.
Some years, the Event Holders' Council may decide
to allow more permissive conversions, based on the changes that were made. Any
such decisions are listed in the [Standing
Policies](./StandingPolicies.html){.compatibility}.

If a spell list has spells that no longer exist, or spells at circles that are
no longer legal, remove those spells from the spell list. After this, the
character may add spells from their Spell Mastery to replace removed spells.
The spells added this way are subject to the usual rules about what spells can
be learned at what circles. The number of spells they can add is dictated by how
many they had before converting their spellbook; they may not end with more
spells than they started with. This must result in a legal spellbook.

After making changes to it, a player converting their spellbook must check it
in with the Event Holder or a designated Magic Marshal to get it signed. Though
characters might learn or unlearn spells as part of this conversion, it is a
separate process intended for correcting illegal spellbooks. As such,
converting a spellbook does not prevent a character from otherwise learning or
unlearning spells at an event. If any spells were learned as part of
converting, the signature they receive for converting counts as the
signature(s) needed for those learned spells. Further, any spells learned may be
added to the character's Spell Mastery section, if they have one.

## The Spells ## {#the-spells}

### 1st Circle Spells ### {#first-circle-spells}

::: {.columns-list}
* [Cure Disease](#cure-disease)
* [Detect Magic](#detect-magic)
* [Disrupt Light](#disrupt-light)
* [Fighter's Intuition](#fighters-intuition)
* [Gentle Repose](#gentle-repose)
* [Ghost Blade](#ghost-blade)
* [Heartiness](#heartiness)
* [Identify](#identify)
* [Immunity to Poison](#immunity-to-poison)
* [Implement](#implement)
* [Light](#light)
* [Mentor](#mentor)
* [Pas](#pas)
* [Protect Item](#protect-item)
* [Protection from Boulder](#protection-from-boulder)
* [Repair Armor](#repair-armor)
* [Speak](#speak)
* [Speak with Dead](#speak-with-dead)
* [Strange Brew](#strange-brew)
* [Transmute Weapon](#transmute-weapon)
* [Zombie Walk](#zombie-walk)
:::

### 2nd Circle Spells ### {#second-circle-spells}

::: {.columns-list}
* [Aura of Protection](#aura-of-protection)
* [Death Watch](#death-watch)
* [Deep Pockets](#deep-pockets)
* [Disenchant](#disenchant)
* [Dream](#dream)
* [Enchant Weapon](#enchant-weapon)
* [Group Healing](#group-healing)
* [Guidance](#guidance)
* [Heal Limb](#heal-limb)
* [Protect the Soul](#protect-the-soul)
* [Protection from Missile](#protection-from-missile)
* [Reanimate Limb](#reanimate-limb)
* [Repair Item](#repair-item)
* [Ward: Undead](#ward-undead)
:::

### 3rd Circle Spells ### {#third-circle-spells}

::: {.columns-list}
* [Animate Lesser Undead](#animate-lesser-undead)
* [Beckon Corpse](#beckon-corpse)
* [Cantrip](#cantrip)
* [Commune with Spirit](#commune-with-spirit)
* [Disease Weapon](#disease-weapon)
* [Enfeeble Being](#enfeeble-being)
* [Feign Death](#feign-death)
* [Foretell](#foretell)
* [Fortune Tell](#fortune-tell)
* [Merge](#merge)
* [Precognition](#precognition)
* [Purity](#purity)
* [Raise Dead](#raise-dead)
* [Skew Divination](#skew-divination)
* [Soul Bane](#soul-bane)
:::

### 4th Circle Spells ### {#fourth-circle-spells}

::: {.columns-list}
* [Animal Companion](#animal-companion)
* [Animate Undead](#animate-undead)
* [Armored Cloak](#armored-cloak)
* [Call the Soul](#call-the-soul)
* [Circle of Protection](#circle-of-protection)
* [Combat Raise Dead](#combat-raise-dead)
* [Create Poison](#create-poison)
* [Death Wish](#death-wish)
* [Disrupt](#disrupt)
* [Divine Aid](#divine-aid)
* [Enchant Armor](#enchant-armor)
* [Find the Path](#find-the-path)
* [Magic Missile](#magic-missile)
* [Mystic Forge](#mystic-forge)
* [Séance](#seance)
* [Shapeshifting](#shapeshifting)
:::

### 5th Circle Spells ### {#fifth-circle-spells}

::: {.columns-list}
* [Animate Undead General](#animate-undead-general)
* [Armor-Piercing Weapon](#armor-piercing-weapon)
* [Circle of Healing](#circle-of-healing)
* [Familiar](#familiar)
* [Reforge](#reforge)
* [Regenerate the Soul](#regenerate-the-soul)
* [Regeneration](#regeneration-spell)
* [Resist Magic](#resist-magic)
* [Vision](#vision)
* [Ward: Enchanted Beings](#ward-enchanted-beings)
:::

### 6th Circle Spells ### {#sixth-circle-spells}

::: {.columns-list}
* [Assassin's Blade](#assassins-blade)
* [Cry of Life](#cry-of-life)
* [Embrace Death](#embrace-death)
* [Intervention](#intervention)
* [Lightning Bolt](#lightning-bolt-spell)
* [Masterwork Hammer](#masterwork-hammer)
* [Prophecy](#prophecy)
* [Resist Death](#resist-death)
* [Ritual of Banishment](#ritual-of-banishment)
* [Second Chance](#second-chance)
* [Seed of Life](#seed-of-life)
* [Transformation](#transformation)
:::

## Spell Descriptions ## {#spell-descriptions}

### Animal Companion (4th Circle) ### {#animal-companion}

__Uses:__ 1, and the spellcaster may only have one in-play - __Material:__ A
stuffed or toy animal that must be at least 4" tall, labeled with the
spellcaster's name and the words "Event-Stealable" - __Caveats:__
[Suspension](#suspension)

The spellcaster has an animal companion represented by a specific stuffed or toy
animal. The animal companion cannot be slain or disenchanted, but can be stolen.
The stuffed animal must be labeled with the spellcaster's name and the words
"[Event-Stealable](#event-stealable)." The animal companion grants two separate
abilities. The first ability is that the spellcaster may send their animal
companion to gather information once per learning of the spell. The information
gathered can only be relayed in a short sentence or concept, such as "the way
ahead is blocked," or "there are many foes," etc. This ability is represented by
the spellcaster giving their animal companion to the EH or MM and asking them a
question. If the spellcaster asks for information that their animal companion is
unable to obtain, they receive no answer.

The second ability that the animal companion grants is the ability to cast a
single spell up to third circle, except the spell [Implement](#implement), which
cannot be chosen. This spell will function as if it were learned normally, with
the same requirements, limitations, number of castings, and components. While
the animal companion is out gathering information the spells provided from the
secondary ability are suspended.

Any additional spells provided by the animal companion require the animal
companion to cast and maintain, as if the animal companion were a spell focus.
Each spell must meet the requirement for verbal, material and active components.
Spells with lasting effects (protections, immunities, etc.) can only be cast
upon the spellcaster. Any blow that strikes the animal companion must be taken
as if the animal companion is not there.

When the spellcaster first learns this spell, they choose their animal
companion's abilities. These abilities are not alterable from event to event.
The spellcaster must list in their spellbook every spell their animal companion
grants them as if they have learned the spell.

If they learn the spell additional times, the animal companion gets stronger.
They may alter the abilities of their animal companion upon completion of each
learning by giving it an additional spell and question. If the spellcaster
unlearns a use of the spell, the animal companion becomes weaker and must be
adjusted accordingly.

A spellcaster may use the same prop for both an Animal Companion and a Familiar
but all spells from both sources are considered suspended while the prop is not
on their person.

### Animate Lesser Undead (3rd Circle) ### {#animate-lesser-undead}

__Uses:__ 3 - __Verbal:__ 30 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask - __Caveats:__ [Compulsions](#compulsions), [Thrall](#thrall),
[Undead](#undead)

This spell animates a weak undead thrall that cannot use any armor or spells. It
will work on PCs but won't always work on NPCs; if cast on NPCs, they can
refuse. If an NPC refuses a casting of the spell, the casting is not spent.

### Animate Undead (4th Circle) ### {#animate-undead}

__Uses:__ 2 - __Verbal:__ 30 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask - __Caveats:__ [Compulsions](#compulsions), [Thrall](#thrall),
[Undead](#undead)

This spell animates an undead thrall that can use any spells or weapons the
target normally could use. If [Animate Lesser Undead](#animate-lesser-undead) is
cast upon the target while the target is still undead, it will reanimate the
target as if another Animate Undead spell were cast upon it, but the target's
loyalties transfer to the person who cast [Animate Lesser
Undead](#animate-lesser-undead).

### Animate Undead General (5th Circle) ### {#animate-undead-general}

__Uses:__ 1 - __Verbal:__ 40 words, and an explanation - __Material:__ A tabard,
or sash which clearly states "Undead," "Skeleton," "Ghost" or the like, or an
appropriate mask, a written explanation of what this spell does, and you must
supply them with 3 MCs for the [Animate Lesser Undead](#animate-lesser-undead)
spells they will cast. - __Caveats:__ [Compulsions](#compulsions),
[Thrall](#thrall), [Undead](#undead)

This spell animates a greater thrall that can use any spells or weapons that the
target normally could use.

If [Animate Lesser Undead](#animate-lesser-undead) or [Animate
Undead](#animate-undead) is cast upon the target while the target is still
undead, it will reanimate the target as if another Animate Undead General spell
were cast upon it, but the target's loyalties transfer to the person who cast
the new spell (no additional MC is needed). The Undead General will die to any
magical attacks that damage it, regardless of location. You may have at most one
undead general at a time.

In addition, the Undead General gains the ability to cast the spell [Animate
Lesser Undead](#animate-lesser-undead) with unlimited uses. An Undead General
can only have 3 Lesser Undead at a time and must retrieve their MC before
casting [Animate Lesser Undead](#animate-lesser-undead) again. If the Undead
General casts [Animate Lesser Undead](#animate-lesser-undead) on anyone who is
not under the effects of [Embrace Death](#embrace-death), they must use an MC.

### Armor-Piercing Weapon (5th Circle) ### {#armor-piercing-weapon}

__Uses:__ 4 - __Material:__ A cloth - __Active:__ Wipe the entire length of the
weapon's striking surface 5 times - __Caveats:__  [Enchanted
Items](#enchanted-items), [OOC Calls](#ooc-calls), [Weapon Calls](#weapon-calls)

This spell gives the spellcaster the ability to enhance their weapon to pass
through and destroy armor. After preparing the spell the spellcaster must call
"[Piercing](#piercing)" on the next attack with that weapon.

### Armored Cloak (4th Circle) ### {#armored-cloak}

__Uses:__ Unlimited, one at a time - __Verbal:__ 30 words - __Material:__
Garb with obvious runes or mystic symbols - __Active:__ Kneel with no weapons or
shields in hands while wearing the garb, or lie on back while wearing the garb -
__Caveats:__ [OOC Calls](#ooc-calls), [Precast](#precast)

This spell enchants some of your garb to provide one call of armor against an
attack. It provides one point of armor against the next blow that lands upon one
of the garments or any location at least 75% covered by one of the garments.

This Armored Cloak cannot be worn in combination with any other form of armor,
ever. It can be worn while protected by a [Protection from
Missile](#protection-from-missile) or [Resist Magic](#resist-magic) spell, in
which case the wearer can choose to call either protection, saving the other for
later. It can only be worn by the spellcaster, and the spell cannot be cast on
any of the garments more than once at a time. Specific garb must be chosen for
the MC at the beginning of the event, and it cannot be changed during the course
of the event without the EH's or MM's permission.

A spellcaster can only cast one Armored Cloak at a time.

### Assassin's Blade (6th Circle) ### {#assassins-blade}

__Uses:__ Unlimited, one at a time - __Material:__ A cloth, and either a single
weapon or arrow within the spellcaster's weapon restriction, labeled with
"Assassin's Blade" and the spellcaster's name - __Active:__ Wipe the entire
length of the blade 5 times - __Caveats:__ [Enchanted Items](#enchanted-items),
[OOC Calls](#ooc-calls), [Precast](#precast), [Weapon Calls](#weapon-calls)

This spell allows the spellcaster to prepare their MC with the spells
[Armor-Piercing Weapon](#armor-piercing-weapon), [Enchant
Weapon](#enchant-weapon), [Transmute Weapon](#transmute-weapon), or [Create
Poison](#create-poison), without expending a casting, although a use of the
spell must still remain in order to prepare it. To prepare the weapon with one
of the listed spells, they must wipe the blade of the weapon with the cloth 5
times. Only one spell may be prepared at a time onto the Assassin's Blade.
Anyone may use the weapon, but only the spellcaster may utilize the special call
with it. The weapon is one-handed, but may not be used in conjunction with
another weapon or shield by the spellcaster. If the MC is IC broken, it will
retain its magical properties if repaired.

When the spellcaster first learns this spell, they choose one of the spells
[Enchant Weapon](#enchant-weapon), [Transmute Weapon](#transmute-weapon), or
[Create Poison](#create-poison), to be an ability inherent to their Assassin's
Blade, and they may prepare its effect on the weapon as if they know the spell
and have uses remaining. However, this does not grant them the spell to use on
other weapons. This ability is not alterable from event to event. The
spellcaster must list which spell is inherent to the blade in their spellbook as
if they have learned the spell.

In addition, any body destroying blow dealt by an Assassin's Blade counts as two
blows dealt.

### Aura of Protection (2nd Circle) ### {#aura-of-protection}

__Uses:__ Unlimited, one at a time per learning - __Verbal:__ 20 words -
__Material:__ Spell Sash - __Active:__ Kneel with no weapons or shields in
hands, or lie on back - __Caveats:__ [Enchanted Items](#enchanted-items), [OOC
Calls](#ooc-calls), [Spell Sash](#spell-sash), [Precast](#precast)

The recipient of this spell lessens the effect of the next hit they take from a
call of "Magic," "Silver," "Disease," "Piercing," "Lightning Bolt," or
"Poison." Rather than taking the effect they were hit with, the recipient of the
spell takes the blow as a normal sword blow instead. It is necessary to call
"Protection" when the spell activates.

When the spell is cast, the recipient of the spell must be kneeling with no
weapons in hand or be lying on their back. This spell may be cast on a recipient
other than the spellcaster; to do so the spellcaster must have no weapons in
hand and touch the recipient while the spell is being cast. More than one
casting of this spell may be in effect on a single PC. If the spellcaster casts
this spell on another PC, they may not re-cast the spell until the sash is
returned to them.

### Beckon Corpse (3rd Circle) ### {#beckon-corpse}

__Uses:__ 5 - __Verbal:__ 20 words, repeated continuously, stating purpose of
spell - __Active:__ Spellcaster must be stationary until finished - __Caveats:__
[Chanting](#chanting), [Suspension](#suspension), [Undead](#undead), [Walking
Dead](#walking-dead)

This spell allows the spellcaster to summon a corpse to get up and move to them
as the [Walking Dead](#walking-dead). The spellcaster must say their complete
verbal once before attempting to get the attention of the player of the corpse
they wish to beckon. If the caster fails to get the attention of the player of
the corpse they wish to beckon, the casting is not spent. As long as the 
chanting continues, the corpse will get up and walk in the most direct (but
OOC safe) path to the spellcaster as if under the effects of the spell [Zombie
Walk](#zombie-walk). If the corpse is interrupted, it will fall to the ground,
but the spellcaster may finish the current round of the verbal, regain the
corpse's attention, and resume chanting the verbal to renew the effect on the
corpse. The spell will end if the corpse reaches the spellcaster, the
spellcaster stops chanting to do something else, or they move from where they
are standing (although they may be moving their arms).

### Call the Soul (4th Circle) ### {#call-the-soul}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Five objects of equal size
and shape, four of one color or design and one of a second color or design, and
an opaque pouch or other way to keep them hidden - __Active:__ A quest may be
required

Allows the spellcaster to possibly find and reattach the soul of a soulless
character. When cast, the spellcaster presents the pouch of objects to the
soulless character, who must then reach in and take one without looking. If the
object is one of the four, the soul is reattached. If the object is the second
color or design, nothing happens. This spell must be cast in the presence of the
EH or MM, who may require an additional quest be completed.

### Cantrip (3rd Circle) ### {#cantrip}

__Uses:__ 3

Allows the spellcaster to gain one casting of any First Circle spell, chosen at
the time of casting. The spell gained must be cast following the rules for that
spell, including VC, MC, and ACs. This spell cannot be used to cast
[Implement](#implement) or [Strange Brew](#strange-brew).

### Circle of Healing (5th Circle) ### {#circle-of-healing}

__Uses:__ 2 - __Verbal:__ 25 words - __Material:__ 10-foot rope - __Active:__
Touch the rope - __Caveats:__ [Circles](#circles), [Suspension](#suspension)

This spell allows the spellcaster to create a Circle of Healing. The spellcaster
may name the circle with [Cure Disease](#cure-disease), [Heal Limb](#heal-limb),
or [Raise Dead](#raise-dead), chosen at the time of casting. The spellcaster
does not need to have the named spell in their spellbook. While the spellcaster
is standing in the Circle of Healing, they may cast the named spell as many
times as desired without consuming a use of that spell. All of the requirements
of the named spell must be met for each casting, including the AC and VC. No one
but the spellcaster may use the Circle of Healing in this manner. This spell is
broken if a weapon crosses the plane of the circle. For this purpose, a weapon
is considered to be anything with a legal striking surface - therefore, swords
and arrows are weapons, although bows and shields are not. You may only have one
casting active from a single learning of this spell at a time.

### Circle of Protection (4th Circle) ### {#circle-of-protection}

__Uses:__ Unlimited, one at a time - __Verbal:__ 10 words - __Material:__
15-foot white rope, or less - __Caveats:__ [Circles](#circles),
[Suspension](#suspension), [Enchanted Beings](#enchanted-beings)

This spell creates a barrier that no enchanted being can physically pass,
affect, or attack, with the exception of the spellcaster that cast it. In
addition, no magic of any kind can pass through the barrier in either direction,
again with the exception of the spellcaster that cast the spell. The spellcaster
that cast the circle may pass over the barrier freely and may cast spells
through the barrier at will. Magic items carried by the spellcaster cannot pass
through the Circle of Protection.

A single spellcaster with multiple castings, or several different spellcasters,
can combine Circle of Protection spells to make a larger one. If multiple
spellcasters join their circles together, the result is a larger circle, but no
enchanted being (even the spellcasters that created it) may pass in or out of
the circle, and no magic of any kind (even from the spellcasters that created
it) can pass through the barrier in either direction. Magic can be cast inside
the circle as normal, but all the effects remain within the circle.

The spellcaster may decide to break their own circle whenever they choose by
uncrossing the rope. The spellcaster can do this even if the circle was cast
with other spellcasters.

### Combat Raise Dead (4th Circle) ### {#combat-raise-dead}

__Uses:__ 3 - __Verbal:__ 3 words - __Active:__ Must touch recipient of spell

This spell will raise a dead character, healing all of their injured limbs. The
VC must clearly state the effects of the spell. For example, "Rise and fight" is
a VC that would make it clear that the individual is being raised.

### Commune with Spirit (3rd Circle) ### {#commune-with-spirit}

__Uses:__ 1 - __Active:__ Ritual - it is required that the spellcaster not
actively seek out the EH or MM. The ritual must, in effect, be spectacular
enough that the EH or MM comes of their own volition

Allows the spellcaster to gain a boon of insight/wisdom from the EH or MM. This
spell allows the spellcaster to ask a spirit something relating to the plot of
the event. How detailed the response or how lengthy the conversation is with the
spirit, is determined completely by the spirit. It should be noted that the
spirit does not have to answer the call of the spellcaster, and that sometimes
instead of helping solve a problem, may give the spellcaster an awareness of
more problems that need solving.

### Create Poison (4th Circle) ### {#create-poison}

__Uses:__ 6 - __Material:__ Disposable edible or drinkable component -
__Caveats:__ [Enchanted Items](#enchanted-items), [Compulsions](#compulsions),
[OOC Calls](#ooc-calls), [Weapon Calls](#weapon-calls)

This spell creates one dose of a specific kind of poison per use. There are only
three types of poison created by this spell: Death, Sleep, and Truth.

* Death kills instantly.
* Sleep causes the victim to fall into a deep sleep for 10 minutes. They cannot
  be woken before this time passes.
* Truth causes the victim to be unable to speak anything except the truth for 10
  minutes, but does not force them to talk.

The type of poison must be chosen when it is cast, and each individual effect
listed in the spellcaster's spellbook. The poison must be ingested and cannot
affect those who don't eat or drink it. This effect must be written legibly on a
scroll. This scroll is to be given to the victim by the spellcaster, EH, or MM
immediately after the MC has been consumed. Only one dose may be made per use,
and only the first person to ingest part of the MC is affected by a spell. The
spell is always rendered inert by [Immunity to Poison](#immunity-to-poison).

Alternatively the spellcaster may expend a use of the spell to coat a weapon
with deadly poison. The next time the spellcaster attacks with that weapon they
must call "[Poison](#poison)." To use this spell this way, the MC becomes a
cloth, and the AC becomes wipe the length of the weapon's striking surface 5
times with the MC.

Only sleep and truth can be resisted by [Protect the Soul](#protect-the-soul) as
per the compulsions caveat.

### Cry of Life (6th Circle) ### {#cry-of-life}

__Uses:__ 1 - __Verbal:__ "All in the sound of my voice, rise and fight."

This spell instantly raises all dead characters whose players hear the verbal.
The spell affects all who hear it, including NPCs and characters fighting
against the spellcaster.

### Cure Disease (1st Circle) ### {#cure-disease}

__Uses:__ 5 - __Verbal:__ 20 words - __Active:__ Touch the recipient

This spell will cure the recipient of all diseases that are currently affecting
them. It will not provide protection from catching a disease after the spell is
cast.

### Death Watch (2nd Circle) ### {#death-watch}

__Uses:__ Unlimited, one at a time - __Active:__ Spellcaster must sit without
weapons in-hand for 60 seconds before they are killed - __Caveats:__
[Precast](#precast)

Enables the spellcaster to see and hear while they are dead. You may not speak
or move while dead, except for addressing marshaling calls or OOC
unsafe/uncomfortable situations. If the spellcaster is rendered soulless, all
memories acquired through the current casting of Death Watch are erased (i.e.
all memories acquired from the time of your PC's last death). The spell ends
when the spellcaster is raised or animated as undead.

### Death Wish (4th Circle) ### {#death-wish}

__Uses:__ 2 - __Verbal:__ 30 words, and an explanation - __Material:__ A scroll
containing the trigger phrase and command - __Caveats:__
[Compulsions](#compulsions)

This spell, when cast upon a dead body, implants a simple command into their
mind. The spellcaster must give the target player the scroll after completing
the verbal. The target must read the scroll at this time and may refer to it at
any later time as needed. However, the scroll and the information it contains
are OOC, and are not known to the PC in any way. When the target character is
alive, and hears the trigger phrase, they must perform the command to the best
of their ability. The spell ends when the target successfully completes the
command, or is slain trying. The target may ignore any commands that violate the
compulsions caveat.

This spell will not work on a body without its soul. The spell [Protect the
Soul](#protect-the-soul) negates all effects of this spell.

### Deep Pockets (2nd Circle) ### {#deep-pockets}

__Uses:__ 3 - __Material:__ A bag no larger than 6" by 12" by 3" - __Caveats:__
[Precast](#precast)

Each casting allows spellcaster to deny any stealable items that are completely
within the MC to the next three characters that search the spellcaster, while
that bag is on the spellcaster's body. If the spellcaster is not carrying any
stealable items outside of the bag, they may answer, "Nothing." All other
stealable items must be yielded to a search. Each additional learning of this
spell allows the spellcaster an additional 6" by 12" by 3" volume of bag, either
as a separate bag, or a larger bag. However, the spellcaster can only have one
usage cast at a time. Each search denial is used up on all of the volumes
simultaneously. One Deep Pockets bag may never contain another. No matter how
many Deep Pockets castings are combined it does not combine the amount of people
that need to search it; it only increases the size of the bag.

### Detect Magic (1st Circle) ### {#detect-magic}

__Uses:__ 5 - __Verbal:__ 20 words

This spell allows the spellcaster to take any one item (not a living or dead
creature) to the EH or MM to ask whether casting [Identify](#identify) upon the
object will yield any information the spellcaster cannot determine by looking at
it, such as "It's a stick," or "It's a sword." It may be cast on a living or
dead being to detect a magical item it carries, such as a spell focus or magic
weapon. In this case, it will not tell what the item does, only that it is there
and which item it is. If cast in this manner multiple times and there are
multiple magical items, it will not repeat magic items until there are no new
items to reveal.

### Disease Weapon (3rd Circle) ### {#disease-weapon}

__Uses:__ 3 - __Verbal:__ 10 words - __Material:__ Spellcaster's weapon -
__Caveats:__ [Enchanted Items](#enchanted-items), [OOC Calls](#ooc-calls),
[Weapon Calls](#weapon-calls)

This spell allows the spellcaster to temporarily enchant their weapon. After
preparing it with the spell, it is considered a diseased weapon and the
spellcaster must call "[Disease](#disease)" on the next attack with that weapon.

### Disenchant (2nd Circle) ### {#disenchant}

__Uses:__ 2 - __Verbal:__ 30 words - __Active:__ Touch the target item

This spell will remove enchantments from the target item. If the target item is
a potion, panacea, or scroll, it will be rendered inert. If the target is a
magic weapon it will no longer function as such until repaired by a
[Reforge](#reforge) spell. Only magic items specified by the EH or MM are immune
to this spell. If the target is a [Spell Sash](#spell-sash), then the spell
represented by the sash is ended. Other spells are not affected unless specified
in their description.

### Disrupt (4th Circle) ### {#disrupt}

__Uses:__ 5 - __Verbal:__ 30 words, starting with "I disrupt this (spell name)
..." - __Active:__ Clearly point at the target - __Caveats:__
[Suspension](#suspension)

This spell will suspend any [circle](#circles) or [chanting](#chanting) spell.
It may only be cast upon a spell that is currently in use. Once the spellcaster
completes the disruption, the target spell is suspended for five minutes and the
spellcaster of the target spell loses the ability to cast the target spell for
five minutes. If the target spell ends before the disruption is completed (the
spellcaster stops chanting, the circle is broken, etc.), the spellcaster of that
spell still loses the ability to cast that spell for five minutes. This spell
only stops the current learning of the target spell. Therefore, if the
spellcaster has taken [Ward: Enchanted Beings](#ward-enchanted-beings) twice,
they temporarily lose the ability to cast one, but retain the ability to cast
the other.

### Disrupt Light (1st Circle) ### {#disrupt-light}

__Uses:__ 5 - __Verbal:__ 20 words, which must clearly state the effect of the
spell

This spell cancels [Light](#light) spells cast by other spellcasters. Once the
Disrupt Light spellcaster is within sight and hearing of a [Light](#light)
spellcaster, they may loudly call out their verbal. Upon completion of the
verbal, all other spellcasters within hearing range must put away their active
[Light](#light) spells. This action is OOC, and those affected must do so even
if they hear the spell while dead. Spellcasters so affected cannot recast the
[Light](#light) spell for five minutes, after which time they may reuse the same
chemical light sticks.

### Divine Aid (4th Circle) ### {#divine-aid}

__Uses:__ 1 - __Verbal:__ Speak to EH or MM - __Material:__ A sacrifice may be
required - __Active:__ A quest may be required

This spell allows the spellcaster to send a request for aid to a higher power.
The request cannot be specific and the higher power may send whatever aid they
see fit. This spell comes with no guarantee that the EH or MM won't simply
listen to the request and say "No." This spell cannot create an effect that will
last beyond the end of the event, other than for healing purposes. A spellcaster
who uses drama and theatrics has a better chance of success.

### Dream (2nd Circle) ### {#dream}

__Uses:__ 1 - __Verbal:__ 20 words - __Material:__ Incense or candle

The spellcaster, after meditation with the MC, goes to sleep. In the morning,
when the character wakes up, they may obtain a dream from the EH or MM. This
dream will often (but not always) be based around the event plot, and it may be
as detailed as the marshal wishes it to be. The spellcaster dreams very lucidly
and may write down a permanent record of what they learned through the dream.
This spell may be pre-registered, and the results of the Dream will be presented
to the spellcaster at check-in.

### Embrace Death (6th Circle) ### {#embrace-death}

__Uses:__ 1 - __Verbal:__ 40 words - __Material:__ A container at least 4 inches
in diameter, labeled with
"Event-Stealable". The container cannot be placed into [Deep
Pockets](#deep-pockets). - __Caveats:__ [Enchanted Items](#enchanted-items),
[Precast](#precast), [Regeneration](#regeneration-caveat), [Basic
Regeneration](#basic-regeneration), [Undead](#undead)

This spell allows the spellcaster to remove their soul from their body, giving
them the ability to defy death.
Nothing physical may be stored in the MC. When the spell is learned, the
spellcaster must scribe in their spellbook a description of exactly what the MC
looks like. You may change this description between events.

Upon casting this spell, the spellcaster's soul enters the MC,
which becomes an [event-stealable](#event-stealable) item for the duration of
the spell. The spellcaster may do whatever they want with the MC: place it in a
mundane pocket, hide it, give it to someone, etc. However, the MC may not be put
into [Deep Pockets](#deep-pockets). The spellcaster may end this spell at any
time by opening the MC, returning the soul to their body.

The following effects are on the caster while the spell is active:

* The spellcaster is [undead](#undead) and is not affected by:
  * [Poisons](#poison)
  * [Diseases](#disease)
  * Any effect that returns the spellcaster to life, unless otherwise specified
  * The spells:
    * [Call the Soul](#call-the-soul)
    * [Heal Limb](#heal-limb)
    * [Potion of Heal Limb](#potion-of-heal-limb)
* The following spells and potions will reanimate the spellcaster (repairing any
  damaged limbs), without placing the spellcaster into someone else's control,
  and they will not gain any other power from these spells, even if they were
  active before Embrace Death:
    * [Animate Lesser Undead](#animate-lesser-undead)
    * [Animate Undead](#animate-undead)
    * [Animate Undead General](#animate-undead-general)
    * [Raise Dead](#raise-dead), if the character casting it is touching the MC.
    * [Combat Raise Dead](#combat-raise-dead), if the character casting it is
      touching the MC.
    * [Potion of Animate Lesser Undead](#potion-of-animate-lesser-undead)
    * [Potion of Animate Undead](#potion-of-animate-undead)
* The spellcaster's limbs may be regenerated by remaining stationary for 30
  seconds.
* By sitting without weapons in hand, in possession of the MC and incanting the
  VC, the spellcaster gains a basic regeneration that will reanimate the
  spellcaster, which triggers upon death. Once reanimated, the spellcaster must
  re-enchant themselves as described above to get another regeneration.
* If the spellcaster is killed with magic, the regeneration time will double to
  240 seconds.
* Destruction of their body will prevent the spellcaster from regenerating, but
  they may still be returned to undeath by any other means described previously.
* If the MC is disenchanted (e.g. by use of the [Disenchant](#disenchant)
  spell), the spell is forcibly ended, and the spellcaster is slain and rendered
  soulless.

If the spell is not ended before the end of the event, and if the spellcaster
has the MC on them, then the spellcaster is returned to life, so they do not get
a tick for ending the event soulless.

### Enchant Armor (4th Circle) ### {#enchant-armor}

__Uses:__ 1 - __Verbal:__ 30 words and an explanation - __Material:__ A
non-stealable token with the spellcaster's name and the words "Enchant Armor" on
it - __Caveats:__ [Enchanted Items](#enchanted-items), [Precast](#precast)

This spell creates a magical token that allows the spellcaster to continually
repair armor worn by a specified character without expending other spells. When
the spellcaster casts this spell, they give the token to the specified
character, who must keep the token with them for the spell to remain in effect.
Once this spell is cast, it may be forcefully ended by disenchanting the token.
As long as the specified character has the token, and the spell has not ended,
the spellcaster may cast [Repair Armor](#repair-armor) as an unlimited effect on
any armor worn by the specified character by performing that spell's AC on the
targeted hit location. This spell does not require the spellcaster to currently
have the spell [Repair Armor](#repair-armor) or have any remaining castings of
[Repair Armor](#repair-armor) in order to use this effect.

### Enchant Weapon (2nd Circle) ### {#enchant-weapon}

__Uses:__ 5 - __Verbal:__ 10 words - __Active:__ Hold the target weapon with
both hands - __Caveats:__ [Enchanted Items](#enchanted-items), [OOC
Calls](#ooc-calls), [Weapon Calls](#weapon-calls)

This spell gives the spellcaster the ability to temporarily enchant a melee
weapon or bow. After preparing it with the spell, the weapon or bow user must
call "[Magic](#magic-call)" or "[Silver](#silver)" the next 3 times they swing
that weapon or the next 3 times they fire that bow. The spellcaster chooses
which option, Magic or Silver, upon casting the spell, and must inform the
recipient which option they are imbuing it with. These calls are expended and
the casting is spent whether the user scores a successful hit or not, unlike
other spells with the Weapon Calls caveat. Also unlike those other spells,
anyone may wield the enchanted weapon to make the calls.

### Enfeeble Being (3rd Circle) ### {#enfeeble-being}

__Uses:__ 2 - __Verbal:__ 30 words, starting with "I declare you mundane ..."

This spell allows the spellcaster to remove the special powers and abilities
from a single NPC creature. To cast the spell, the spellcaster must get the
creature's attention and begin the verbal. Once the spell is completed, the
target may lose access to some or all of its abilities. This includes natural
armor, spells, regeneration, etc. Because this is a relatively low-circle spell,
it will probably have little or no effect on more powerful creatures, such as
unique enemies or the proverbial "Big Bad Guy," but it might work on things like
a troll, a lesser demon, or a goblin shaman. This spell will never work on PCs.
A spellcaster should choose their targets wisely.

### Familiar (5th Circle) ### {#familiar}

__Uses:__ 1. The spellcaster may only have one in-play - __Material:__ A stuffed
or toy animal that must be at least 4" tall, labeled with the spellcaster's name
and the words "Event-Stealable" - __Caveats:__ [Suspension](#suspension)

The spellcaster has a familiar that grants them more spell potential,
represented by a specific stuffed or toy animal. The familiar cannot be slain or
disenchanted, but can be stolen.

Any additional spells provided by the familiar require the presence of the
familiar to cast it and maintain it, as if the familiar were a spell focus or
additional MC. If the familiar is stolen or dropped, the spellcaster may not
cast or maintain any of the extra spells. Each spell must meet the requirement
for VC, MC, and ACs. Spells with lasting effects (protections, immunities, etc.)
can only be cast upon the spellcaster. Spells with lasting effects are suspended
while the familiar is not in the spellcaster's possession. Any blow that strikes
the familiar must be taken as if the familiar is not there.

The spellcaster has 5 points to spend on extra spells for every casting of this
spell, up to 20 points total.

+---------------------------------+-------------------+
| Benefit                         | Cost              |
+=================================+===================+
| [Raise Dead](#raise-dead)       | (1 pt.) - 1 use   |
+---------------------------------+-------------------+
| [Disrupt](#disrupt)             | (1 pt.) - 1 use   |
+---------------------------------+-------------------+
| [Call The Soul](#call-the-soul) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Enfeeble                       | (2 pts.) - 1 use  |
| Being](#enfeeble-being)         |                   |
+---------------------------------+-------------------+
| [Find The Path](#find-the-path) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Shapeshifting](#shapeshifting) | (2 pts.) - 1 use  |
+---------------------------------+-------------------+
| [Combat Raise                   | (3 pts.) - 2 uses |
| Dead](#combat-raise-dead)       |                   |
+---------------------------------+-------------------+
| [Reforge: Restore               | (4 pts.) - 1 use  |
| Enchantment](#reforge)          |                   |
+---------------------------------+-------------------+

When the spellcaster first learns this spell, they choose their familiar's
abilities. These abilities are not alterable from event to event. The
spellcaster must list every spell their familiar grants them in their spellbook
as if they have learned the spell.

If they learn the spell Familiar additional times, they may alter the abilities
of their familiar upon completion of each learning. If the spellcaster unlearns
a use of the spell Familiar, the familiar they have then becomes weaker and must
be adjusted accordingly.

A spellcaster may use the same prop for both an Animal Companion and a Familiar
but all spells from both sources are considered suspended while the prop is not
on their person.

### Feign Death (3rd Circle) ### {#feign-death}

__Uses:__ Unlimited - __Material:__ A cloth - __Active:__ Wipe cloth over face 5
times

This spell allows the spellcaster to disguise themselves so as to appear dead.
If someone asks them if they are dead they can legally answer "Yes," and may lie
down or sit with their sword or arm above their head as to appear dead (see
rules on [character death](#character-death) and [soul loss](#soul-loss)). Feign
Death ends once the spellcaster moves or speaks (except for addressing
marshaling calls or OOC uncomfortable/unsafe situations). If a person moves
them, thinking they are dead, the Feign Death does not end; only when they move
themselves. If struck while using Feign Death, the spellcaster is still affected
by the blow as normal.

### Fighter's Intuition (1st Circle) ### {#fighters-intuition}

__Uses:__ 3 - __Material:__ Spell Sash - __Active:__ place sash on fighter -
__Caveats:__ [Enchanted Items](#enchanted-items), [Spell Sash](#spell-sash)

This spell must be cast on a non-spellcaster by placing the sash on the fighter
and giving an explanation of how it works. This does not make the fighter an
enchanted being.

This fighter may now call out "Fighter's Intuition" once. When the fighter does
this, they may or may not learn information about a monster they can see. It is
up to the event staff to decide to provide this information or not. The
information can be anything: weakness, methods of defeating, or even what the
NPC likes to eat. If the event staff does not provide any information about the
monster, the use of the spell is not expended and the fighter may attempt to use
it again.

### Find the Path (4th Circle) ### {#find-the-path}

__Uses:__ 2 - __Verbal:__ 30 words - __Caveats:__ [Spell
Failure](#spell-failure)

This spell provides the spellcaster a route to find, locate, or travel to a
person, place or thing that they know by name. For instance, you can get a
response from "Where is the body of King Joe?" but not from "Take me to the
person who stole my sword." The results of this spell can come as a guide, a
map, a set of directions, a divining rod, or any other mechanic that the EH or
MM deems appropriate. Be aware that the answer may not always be the safest or
shortest path. This spell will fail if an answer cannot be determined because of
PC action.

### Foretell (3rd Circle) ### {#foretell}

__Uses:__ 2 - __Verbal:__ A prediction that the EH or MM must be present for

Allows the spellcaster to make a prediction of an event to come, e.g. "Sir
Thomas will slay a dragon with a silver sword." If the event foretold comes to
pass the EH or MM may grant a boon to the spellcaster or anyone involved in the
prediction. The nature and power of this boon is up to the EH or MM. The greater
the foretold event or more specific the prediction, the more powerful the
resulting boon should be. If the caster believes that their prediction has come 
true, they may check with the Magic Marshal.

### Fortune Tell (3rd Circle) ### {#fortune-tell}

__Uses:__ 2 - __Material:__ Fortune-telling paraphernalia, such as runes or a
tarot deck - __Caveats:__ [Spell Failure](#spell-failure)

This spell allows the spellcaster to ask a question of the EH or MM, which will
be answered in a symbolic manner. How much information (if any) and the form in
which it is given is at the discretion of the EH or MM. No proper names may be
used in either the question or the answer for this spell. For example, while a
spellcaster cannot ask "Who killed Sir Schlep?" they can ask, "Who killed this
knight?" and the answer can be "Tarot Card: Jack of Wands" but not "Bad Bart."
This spell can only be used to determine information that is plot-related. If
the EH or MM does not know the answer because the question asked relates to PC
actions, an answer will not be given, but the spell is still used. If the spell
is cast and an answer cannot be given because of any of the above limitations,
the casting is still used up.

### Gentle Repose (1st Circle) ### {#gentle-repose}

__Uses:__ 5 - __Verbal:__ 10 words, and an explanation - __Active:__ Touch the
target dead body

This spell once cast will protect the target dead body temporarily as per the
spell [Protect the Soul](#protect-the-soul). Once the target is returned to life
by any means this spell effect ends. This spell may only be removed from the
dead body via the [Disenchant](#disenchant) spell otherwise.

### Ghost Blade (1st Circle) ### {#ghost-blade}

__Uses:__ 2 - __Verbal:__ 20 words - __Material:__ A white ribbon with the words
"Ghost Blade" on it - __Caveats:__ [Enchanted Items](#enchanted-items)

This spell enchants a single weapon to no longer affect the casting of the
spells Raise Dead or Regeneration or the breaking of Circle of Healing. Upon
casting this spell, the spellcaster must tie the MC onto the enchanted weapon.

### Group Healing (2nd Circle) ### {#group-healing}

__Uses:__ 2 - __Verbal:__ 10 words - __Material:__ 30' rope - __Active:__ Touch
the rope - __Caveats:__ [Circles](#circles), [Suspension](#suspension)

This spells allows the spellcaster to cast an enchanted circle. This circle
allows certain spells cast into it to affect all the people within the circle.
The Group Healing circle may be used to enhance the power of the following
spells: [Combat Raise Dead](#combat-raise-dead), [Cure Disease](#cure-disease),
[Immunity to Poison](#immunity-to-poison), [Heal Limb](#heal-limb), and [Raise
Dead](#raise-dead). Multiple castings of Group Healing from the same or
different spellcasters may be used at the same time, creating a bigger circle.

To enchant the Group Healing circle, lay the rope(s) in a circle on the ground
with the ends touching. Then all the characters to be cast upon need to be
gathered into the circle. The spellcaster(s) must then recite the VC, which
empowers the circle.

The next spell from the accepted list cast into this circle by any spellcaster
affects all within as if it had been cast on each individually. If the spell has
any MC, only one is used.

### Guidance (2nd Circle) ### {#guidance}

__Uses:__ 2 - __Material:__ Divining paraphernalia to indicate a yes/no answer -
__Caveats:__ [Spell Failure](#spell-failure)

This spell allows the spellcaster to ask the EH or MM a yes/no question. If the
EH or MM does not know the answer because the question asked relates to PC
actions, an answer may not be given. An answer will be given in the form of
"Yes" or "No" by the EH or MM. If the spell is cast and an answer cannot be
given because of any of the above limitations, the casting is still used up.

### Heal Limb (2nd Circle) ### {#heal-limb}

__Uses:__ Unlimited - __Verbal:__ 20 words - __Active:__ Spellcaster must be
stationary, must touch the target limb

This spell allows the spellcaster to heal one damaged limb at a time. The
spellcaster must recite the VC while touching the recipient's injured limb. The
spellcaster cannot move their feet while casting this spell, although they may
be moving their arms (e.g. parrying, so long as they don't step backwards).

### Heartiness (1st Circle) ### {#heartiness}

__Uses:__ Unlimited, one at a time - __Caveats:__ [Precast](#precast)

Having this spell makes it harder to destroy the spellcaster's body. The next
time the spellcaster's body is destroyed it will take 200 extra blows to
successfully destroy their body. If struck for only 200 blows, instead of the
full 400 blows, the spellcaster must inform the individual(s) destroying their
body that "The job is not yet done." A spellcaster can only be under the effect
of one Heartiness spell at a time. A use is considered to be over whenever the
spellcaster receives at least 200 body destroying blows, but is in effect until
either their body is destroyed or they are raised.

### Identify (1st Circle) ### {#identify}

__Uses:__ 3 - __Verbal:__ 30 words

This spell allows the spellcaster to take any one item (not a living/dead
creature) to the EH or MM to ask what it is and expect an answer. This spell can
also determine what species an unknown creature is. If the spellcaster can
successfully reach visual inspection range, recite the verbal, and the creature
is not hostile, it must state what species it is. The response is not IC speech
by the creature, and it can answer while dead.

### Immunity to Poison (1st Circle) ### {#immunity-to-poison}

__Uses:__ 3 - __Verbal:__ 10 words - __Active:__ Touch the recipient -
__Caveats:__ [OOC Calls](#ooc-calls)

This spell makes the recipient immune to the next dose of poison that would have
otherwise affected their PC during the event. When damaged by the next poison
attack, whether ingested or delivered by a poisoned weapon, call "Immunity to
Poison!" Only one Immunity to Poison is used at a time. The recipient must take
any mundane damage from a poisoned weapon regardless of whether they are
protected from the actual poison. More than one Immunity to Poison can be cast
upon a recipient; the effect is stackable. This spell can also be cast as an
antidote for any one poison that the recipient has been subjected to, but in
this case it will not provide any further protection.

### Implement (1st Circle) ### {#implement}

__Uses:__ Special - __Material:__ Safe, non-weapon Staff (between 4' and 6'
long, inclusive), Wand (between 12" and 18" long, inclusive), Orb (at least 4"
in diameter), or Book (bound, minimum 1/2" x 4" x 7", cannot be the
spellcaster's spell-book)

The spellcaster is able to create a staff, wand, orb, or book (hereafter called
"implement") that enhances their own spells. Each time the spellcaster learns
this spell, they gain 1 point into a pool from which they may purchase special
abilities from the choices below. A spellcaster may only have 5 points worth of
implements per event. At the magic check-in of an event, the spellcaster may
choose how the points in their pool are spent.

Unless otherwise stated, abilities gained that augment or alter spells require
that the spellcaster already knows that spell, otherwise there is no effect. In
order to use the gained ability, the spellcaster must be holding the implement
in one hand.

The implement is a magical manifestation and cannot be broken. Any blow that
strikes the implement must be taken as if the implement is not there. You can
not actively parry with an implement.

An implement may be disenchanted causing any effects or castings to be lost
until the implement is restored. If disenchanted it takes 120 seconds of holding
the implement with both hands and nothing else to restore.

Gain one additional casting of one of the following spells for 1 point each:
[Find the Path](#find-the-path), [Fortune Tell](#fortune-tell),
[Guidance](#guidance), [Precognition](#precognition), [Skew
Divination](#skew-divination), [Raise Dead](#raise-dead), [Deep
Pockets](#deep-pockets), [Enfeeble Being](#enfeeble-being), [Beckon
Corpse](#beckon-corpse), [Disenchant](#disenchant), or [Disrupt](#disrupt).

Gain the following abilities for 1 point each:

* When using a Circle spell you may double the length of the rope. This may only
  be done once.
* The AC for [Death Watch](#death-watch) is changed to "Spellcaster must kneel
  on one knee holding their Implement with both hands for 60 seconds before
  being killed." Additionally, the spell no longer ends when the spellcaster is
  raised.
* The spell [Death Watch](#death-watch) allows the spellcaster to move their
  head while dead. They still may not speak, try to communicate, or move in any
  other way while dead.

Gain one additional casting of one of the following spells for 2 points each:
[Call the Soul](#call-the-soul), [Group Healing](#group-healing), [Resist
Magic](#resist-magic), [Animate Undead](#animate-undead), or [Soul
Bane](#soul-bane).

Gain the following abilities for 2 points each:

* The uses for [Speak](#speak) become unlimited.
* Gain 1 point to spend on your [familiar](#familiar). This ability may not be
  taken more than once. When this augmentation is first used through Implement,
  you shall list an alternate build for the [familiar](#familiar). This
  alternate build can only be changed by learning or unlearning a use of
  [Familiar](#familiar). From now on, when you use your [Familiar](#familiar)
  and it is augmented by Implement, it uses that new build. If you are using the
  augmented build, and you do not have your implement on you, you may not use
  your [Familiar](#familiar) abilities.

Gain one additional casting of one of the following spells for 3 points each:
[Vision](#vision) or [Séance](#seance).

Gain the following abilities for 3 points each:

* Gain one use of [Regeneration](#regeneration-spell). You are not required to
  know the spell to use this ability.
* Gain one use of [Regeneration](#regeneration-spell). Upon completion, the
  spellcaster will be animated as a free-willed undead. You are not required to
  know the spell to use this ability.
* Gain one additional [Magic Missile](#magic-missile) prop.

### Intervention (6th Circle) ### {#intervention}

__Uses:__ 1 - __Verbal:__ Speak to EH or MM - __Material:__ A sacrifice may be
required - __Active:__ A quest may be required - __Caveats:__ [Spell
Failure](#spell-failure)

This spell allows the spellcaster to go to the EH or MM and ask a boon from
whatever powers their magic. It only works if it is cast in the presence of the
EH or MM. It is to be used to request favors such as: "Oh, please, great
majestic god/Fire Spirit/Navel Lint, grant me a quest to search for the lost
soul of my overlord, Sir Biff of Bonehead Ridge." This spell comes with no
guarantee that the EH or MM won't simply listen to the request and say "No."
This spell cannot create an effect that will last beyond the end of the event,
other than for healing purposes. A spellcaster who uses drama and theatrics has
a better chance of success, and simple, small requests are also more likely to
be granted. Any requests that will unbalance the game will likely be either
denied straight out, or assigned an unsolvable quest.

### Light (1st Circle) ### {#light}

__Uses:__ Unlimited - __Verbal:__ 3 syllables - __Material:__ Chemical light
stick and dark bag or EH- or MM-approved electronic light with an on/off
mechanism in any color except red - __Active:__ Snap and shake the chemical
light stick or turn on the electronic light

This spell creates light. The spellcaster may use as many Light props as
desired. Electronic light sources must be checked in before use and may be
pulled if they may be too bright or unsafe for the event. If using chemical
light sticks, the spellcaster must also carry a bag large enough to hold all of
the glow sticks they will use and thick enough to prevent any light from
escaping. The bag is to be used if they are affected by a [Disrupt
Light](#disrupt-light) spell. The spellcaster may not give a Light prop to
anyone who is going to travel beyond easy speaking distance. It is possible for
this spell to be disrupted. It is the spellcaster's responsibility to know what
the Disrupt Light spell is, how to recognize it, and how to respond to it.

### Lightning Bolt (6th Circle) ### {#lightning-bolt-spell}

__Uses:__ 1 prop, unlimited use - __Verbal:__ "Lightning Bolt" - __Material:__ 1
white boff arrow or javelin prop between 2'6" and 3'6" long - __Caveats:__ [OOC
Calls](#ooc-calls)

This spell allows the spellcaster to throw a stronger bolt of magic than [Magic
Missile](#magic-missile). The MC for the spell must be made following the
[Weapon Construction rules for Lightning Bolts](#lightning-bolt-construction).

The prop is a physical representation of the magic. After it comes to rest, it
cannot be affected or moved by anyone other than the spellcaster, but it may
still be seen or guarded by anybody. The prop counts as a hand-and-a-half weapon
and must be thrown, not shot from a bow. The prop, including its shaft, strikes
as a [piercing](#piercing) [magic](#magic-call) blow to anything it makes
contact with, until it comes to rest. Once cast, it cannot be cast again until
the spellcaster recovers the prop. The prop is not considered a weapon and does
not cause [Spell Failure](#spell-failure), except while the spell is active
(i.e. from when the prop is thrown until it comes to rest).

### Magic Missile (4th Circle) ### {#magic-missile}

__Uses:__ Unlimited, while spellcaster has MC handy - __Verbal:__ "Magic
Missile" - __Material:__ 3 beanbags or foam & duct tape blocks, about 3"
diameter - __Caveats:__ [OOC Calls](#ooc-calls)

When thrown, this spell strikes whatever it hits as if it were a magic sword. It
will damage every location it hits, until it comes to rest. The prop is a
physical representation of the magic. After it comes to rest, it cannot be
affected or moved by anyone other than the spellcaster, but it may still be seen
or guarded by anybody. A magic missile MC can be thrown with one hand. When a MC
is being thrown, the other hand may contain only a single magic missile MC or a
single-handed weapon or shield, and does not count toward dual-wielding for the
purposes of weapon restrictions. The prop is not considered a weapon and does
not cause [Spell Failure](#spell-failure) except while the spell is active (i.e.
from when the prop is thrown until it comes to rest). The spellcaster may only
throw their spell props, and may not pick up those thrown by another
spellcaster.

### Masterwork Hammer (6th Circle) ### {#masterwork-hammer}

__Uses:__ 1/special - __Verbal:__ 50 words - __Material:__ A boff hammer within
the spellcaster's weapon restriction with "Masterwork Hammer" and the
spellcaster's name written on it - __Active:__ Special - __Caveats:__ [Enchanted
Items](#enchanted-items), [Precast](#precast)

This spell creates a Masterwork Hammer which the spellcaster may use to repair
non-armor, non-magic items (bows, weapons, shields) in 30 seconds. The
spellcaster may also use the hammer to repair all armor on a target player by
using the hammer as the focus of the spell for 60 seconds. While using the
hammer to make any type of repair, the spellcaster cannot move their feet and is
encouraged to actively use the hammer to simulate repairing the target. If the
hammer is broken or disenchanted, the spellcaster may repair it by holding the
item in both hands for 120 seconds.

### Mentor (1st Circle) ### {#mentor}

__Uses:__ Unlimited

Allows the spellcaster to teach a legal spell or alternative from their spell
mastery list,
otherwise following the normal [rules for learning
spells](#learning-and-unlearning-spells).

### Merge (3rd Circle) ### {#merge}

__Uses:__ 3 - __Verbal:__ 10 word chant, repeated. The verbal must be chanted
loudly and clearly - __Caveats:__ [Chanting](#chanting), [OOC
Calls](#ooc-calls), [Suspension](#suspension)

This spell provides an immense amount of protection to the spellcaster, but also
requires an immense amount of concentration. This spell only takes effect once
the spellcaster has completed the VC once. While merged, the spellcaster is
completely immune to all forms of damage, magical or otherwise, regardless of
whether the material into which the spellcaster merges is vulnerable to any
form of damage. It does not make the spellcaster invisible or undetectable. The
spellcaster is capable of merging with trees, stone, or earth. To merge, the
spellcaster must embrace or lie down on the object they are merging with. While
merged, the spellcaster is "stuck" and cannot be dragged. The object the
spellcaster merges with MUST be at least as massive as the spellcaster. The
spellcaster must remain still, and they must be constantly chanting their verbal
while merged. The spellcaster must chant loudly and clearly. If anything
interrupts the spellcaster's concentration, the spell is broken. OOC
explanations (such as combat calls) do not interrupt this spell. For example, if
a PC chanting a Merge spell is hit by a weapon, they may call "No effect"
without interrupting the spell. As soon as the spellcaster moves or stops
chanting, the spell ends. The spellcaster may not merge for at least one slow
200 second count after regaining their proper form. 

### Mystic Forge (4th Circle) ### {#mystic-forge}

__Uses:__ 2 - __Verbal:__ 25 words - __Material:__ 10-foot rope - __Active:__
Touch the rope - __Caveats:__ [Circles](#circles), [Suspension](#suspension)

This spell allows the spellcaster to create a Mystic Forge. The spellcaster may
name the circle with [Enchant Weapon](#enchant-weapon) or [Repair
Item](#repair-item), chosen at the time of casting. The spellcaster does not
need to have the named spell in their spellbook. Until the Mystic Forge is
broken, the spellcaster need only stand in the circle, touch the target item,
and recite the named spell's VC to cast the spell. This does not use up any
castings of the named spell, and this can be done as many times as desired. No
one but the spellcaster may use the Mystic Forge in this manner.

### Pas (1st Circle) ### {#pas}

__Uses:__ 3 - __Verbal:__ "Pas, friend" - __Material:__ Food, coin, or some
offering - __Active:__ Offer the MC to the target - __Caveats:__
[Compulsions](#compulsions)

This spell creates an uneasy, temporary truce between the target and the PC.
Prior to casting this spell, the spellcaster must offer something of value to
the target. If the target accepts the offering, the spellcaster may immediately
cast this spell by reciting the verbal component. When the spell is cast, the 
target is magically bound to not attack the spellcaster for 60 seconds, unless
the target is attacked. If the target is attacked or the spellcaster is slain, 
this spell ends immediately. [Protect the Soul](#protect-the-soul) will block 
the effects of this spell, as will [Resist Magic](#resist-magic). Some powerful
creatures may not be affected by this spell.

### Precognition (3rd Circle) ### {#precognition}

__Uses:__ 3 - __Material:__ Divining paraphernalia (such as a crystal ball or
mirror)

This spell allows the spellcaster to gain non-specified information about the
plot from the EH or MM. How much information (if any) is at the discretion of
the EH or MM.

### Prophecy (6th Circle) ### {#prophecy}

__Uses:__ 1 - __Active:__ Ritual (Optional)

This spell allows the spellcaster to ask the EH or MM a question pertaining to
the plot of the event. The EH or MM will give the spellcaster as complete an
answer as they are willing. The method of delivering this knowledge is at the
EH's or MM's discretion. A spellcaster may use drama, theatrics, or sacrifice
during the ritual to have a better chance of gaining information. After casting
this spell, once every two hours the spellcaster may ask a follow up question to
the EH or MM related to the initial question. The EH or MM may also choose to
release additional information to the spellcaster at any time during the
remainder of the event or until a spell reset.

### Protect Item (1st Circle) ### {#protect-item}

__Uses:__ 3 - __Verbal:__ 20 words - __Material:__ Ribbon tied onto the item
protected. Remove the ribbon soon after the spell is expended/used to protect
the item - __Caveats:__ [Enchanted Items](#enchanted-items), [OOC
Calls](#ooc-calls)

This spell allows a single non-armor item to be protected from the next attack
that would normally damage it. For example, a protected sword struck by a
boulder would not be destroyed, but the wielder would still suffer normal damage
(e.g. death usually). The call for this spell is "Protect Item." A particular
item may only have one casting of Protect Item on it at a time. This spell does
not protect against [Disenchant](#disenchant).

### Protect the Soul (2nd Circle) ### {#protect-the-soul}

__Uses:__ 1 - __Verbal:__ 30 words and an explanation - __Material:__ Spell Sash
- __Caveats:__ [Enchanted Items](#enchanted-items), [OOC Calls](#ooc-calls),
[Precast](#precast), [Spell Sash](#spell-sash)

This spell will protect the recipient from possession, spells under the
[compulsions](#compulsions) caveat, and similar effects as determined by the
EH or MM. When targeted by any spell or effect against which Protect the Soul
immunizes your PC, you must call "Protect the Soul!" The spell will last until
the sash is disenchanted or removed by the spellcaster. The spell will not
function if the recipient is soulless or their soul is not within their body for
any other reason.

### Protection from Boulder (1st Circle) ### {#protection-from-boulder}

__Uses:__ 2 - __Verbal:__ 20 words - __Material:__ Spell Sash - __Caveats:__
[Enchanted Items](#enchanted-items), [OOC Calls](#ooc-calls),
[Precast](#precast), [Spell Sash](#spell-sash)

The spellcaster is protected from the next "boulder" call that strikes them.
This protection extends to all equipment they are carrying.

### Protection from Missile (2nd Circle) ### {#protection-from-missile}

__Uses:__ Unlimited, one at a time - __Verbal:__ 10 words - __Material:__ Spell
Sash - __Active:__ Kneel with no weapons or shields in hands, or lie on back -
__Caveats:__ [Enchanted Items](#enchanted-items), [OOC Calls](#ooc-calls),
[Precast](#precast), [Spell Sash](#spell-sash)

The recipient of this spell is protected from the next hit they take from an
arrow, javelin, or [Magic Missile](#magic-missile) spell. It is necessary to
call "Protection" when the spell activates. This spell will also protect
equipment (such as armor) that would otherwise be affected by the missile.

When the spell is cast, the recipient of the spell must be kneeling with no
weapons in hand or be lying on their back. This spell may be cast on a recipient
other than the spellcaster; to do so the spellcaster must have no weapons in
hand and touch the recipient while the spell is being cast. More than one
casting of this spell may be in effect on a single PC. If the spellcaster casts
this spell on another PC, they may not re-cast the spell until the sash is
returned to them.

### Purity (3rd Circle) ### {#purity}

__Uses:__ 1, Self-only - __Verbal:__ 10 words - __Material:__ Spell Sash which
states the type of immunity granted - __Active:__ Kneel with no weapons or
shields in hands, or lie on back - __Caveats:__ [Enchanted
Items](#enchanted-items), [OOC Calls](#ooc-calls), [Precast](#precast), [Spell
Sash](#spell-sash)

Upon casting this spell, the spellcaster becomes completely immune to the
effects of either poisons or diseases, chosen at the time of casting. The spell
sash worn for any particular casting of this spell must state which immunity is
granted by that casting. This means that a caster should have 2 separate sashes,
or a sash that can be modified to show either effect.

### Raise Dead (3rd Circle) ### {#raise-dead}

__Uses:__ 5 - __Verbal:__ 30 words - __Active:__ Spellcaster must be within 2
feet of corpse and there can be no weapons within 10 feet of the spellcaster -
__Caveats:__ [Spell Failure](#spell-failure)

This spell will raise a dead character, healing all of their injured limbs.
There can be no weapons within 10 feet of the spellcaster at any point while
casting this spell, or the spell will fail to work. For this purpose, a weapon
is considered to be anything with a legal striking surface - swords and arrows
are weapons, although bows are not. The player of the character being raised
must be present to represent the corpse. No proxy can be used for the corpse.

### Reanimate Limb (2nd Circle) ### {#reanimate-limb}

__Uses:__ 10 - __Verbal:__ 20 words - __Active:__ Spellcaster must be
stationary, must touch the target limb

This spell allows the spellcaster to animate damaged limbs on an undead target.
When cast on an undead the spellcaster controls (this excludes themselves), this
spell animates all damaged limbs, regardless of disease status. Otherwise, it
only animates a single limb. The spellcaster must recite the VC while touching
the recipient's damaged limb. The spellcaster cannot be moving around (e.g.,
running for their life) while casting this spell, although they may be moving
their arms (e.g., parrying, so long as they do not step backwards).

### Reforge (5th Circle) ### {#reforge}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Special - __Active:__ Special

This spell allows the spellcaster to reforge an existing item, either improving
it or repairing a magical item. The spellcaster may choose one of the following
options when casting the spell. Each option has its own effects and requirements
for casting the spell that must be met for the spell to be successfully cast.

Silver Weapon
: This option will allow the spellcaster to create a permanent silver weapon.
  This option has an additional MC of the weapon's weight in silver coins (plain
  aluminum roofing tins, aluminum "Coin of the Realm," etc.). The spellcaster
  must write the words "Silver" and "Stealable" on the blade of the weapon. The
  spellcaster must also write "Silvered by" and the spellcaster's name on the
  blade. All of the silver that is collected for the casting of this spell must
  be handed over to the EH or MM. All silver weapons are stealable, and the
  spellcaster must explain to anyone having a weapon made silver that it will be
  stealable, and will be considered property of the Realms, to be passed back
  and forth within the game as a searchable item, for as long as it is silver. A
  silver weapon will lose the quality of being silver if the writing on the
  blade fades to the point of no longer being readable or if the weapon is OOC
  or IC broken (such as by a boulder). Players may not protect the writing in
  any way and may not rewrite it. A silvered weapon broken IC (i.e. by any
  means other than the fading of the writing or the physical destruction of the
  prop) can be repaired by an expenditure of this spell, without having to
  provide the necessary silver. Repairing a weapon in this way does not allow
  you to remake the prop or rewrite the word "Silver" on the weapon. Silver
  Weapon will not allow you to silver an existing magic item.

Reinforce Item
: This option will allow the spellcaster to make a normal weapon or shield
  unbreakable for the duration of the event. This option has an additional MC
  of a ribbon with the words "Reinforce Item" attached to the target item. The
  reinforced item will not be broken by attacks that would normally damage them
  (e.g. a boulder hitting it) as long as the spell remains active. If
  disenchanted through the spell [Disenchant](#disenchant), the ribbon must be
  removed and the effect will end, but the weapon will otherwise remain intact.
  This option may not be used on a magic item, silver weapon, or bows.

Restore Enchantment
: This spell allows the spellcaster to restore a currently-backed [Realms Magic
  Item](#magic-item-rules) that has been broken or disenchanted to working
  order. This option may have an additional AC of a required quest. Potions and
  other spell components are not subject to repair. If an item has been
  physically broken or made unsafe, then the item must be physically repaired or
  replaced with a near duplicate of the item. Note that patching or minor
  repairs may be acceptable instead of a full prop replacement depending on the
  situation. Upon informing the EH or MM of their intention to cast this spell,
  the EH or MM may either present to the spellcaster a quest for the completion
  of this spell or simply declare the item "repaired" at their discretion. The
  spell is wasted if the quest fails or if permission for a repair is outright
  denied.

### Regenerate the Soul (5th Circle) ### {#regenerate-the-soul}

__Uses:__ 1 - __Verbal:__ 30 words -
__Caveats:__ [Precast](#precast),
[Regeneration](#regeneration-caveat), [Advanced
Regeneration](#advanced-regeneration)

This spell grants an advanced regeneration which returns a character to life
from soullessness only.


### Regeneration (5th Circle) ### {#regeneration-spell}

__Uses:__ 2 - __Verbal:__ 30 words - __Active:__ Spellcaster must sit on the
ground with weapons a minimum of 10 feet away while casting this spell -
__Caveats:__ [Precast](#precast), [Regeneration](#regeneration-caveat),
[Advanced Regeneration](#advanced-regeneration), [Spell Failure](#spell-failure)

This spell grants the spellcaster an [advanced
regeneration](#advanced-regeneration). If you have learned this spell twice, you
may cast a [Raise Dead](#raise-dead) to "recharge" one usage. These recharges
cannot come from [Circle of Healing](#circle-of-healing), or any other raising
effects other than the [Raise Dead](#raise-dead) spell.

### Repair Armor (1st Circle) ### {#repair-armor}

__Uses:__ 5 - __Material:__ Focus - __Active:__ Hold armor and MC
for 15 second count

This spell will repair one hit location of armor. You are encouraged to simulate
physically repairing the armor, such as tapping it with a focus, like a
boff-hammer.

### Repair Item (2nd Circle) ### {#repair-item}

__Uses:__ 5 - __Verbal:__ 20 words - __Active:__ Touch the target item with both
hands

This spell repairs any one normal object: a weapon, shield, bow or armor. It
cannot repair an item with a special property, such as a [magic
item](#magic-item-rules). If used to repair armor that is being worn, a single
casting will repair all pieces of armor that can be legally called by the
wearer. The spellcaster may have nothing else in their hands while casting this
spell.

### Resist Death (6th Circle) ### {#resist-death}

__Uses:__ Unlimited, one at a time - __Verbal:__ 30 words - __Material:__ Spell
Sash - __Active:__ Kneel with no weapons or shields in hands, or lie on back -
__Caveats:__ [Enchanted Items](#enchanted-items), [OOC Calls](#ooc-calls),
[Precast](#precast), [Spell Sash](#spell-sash)

The spellcaster is protected from any damaging attack for 1 hit. The call for
this is "Resist Death." The spellcaster can choose when to utilize this effect.

### Resist Magic (5th Circle) ### {#resist-magic}

__Uses:__ 3 - __Verbal:__ 20 words - __Caveats:__ [OOC Calls](#ooc-calls),
[Precast](#precast)

This spell prepares a burst of null-magic within the spellcaster. If the
spellcaster so desires, they may ignore a single magical effect. This ability
can be used at any time, whether the spellcaster is dead or not. A spellcaster
may not be under the effect of more than one Resist Magic spell at the same
time. When targeted by a spell or effect against which Resist Magic protects
them and the spellcaster wishes to ignore the effect, they call "Resist Magic."
For example: this spell will allow the spellcaster to treat a blow from a magic
weapon as if it were a normal weapon blow, ignore the effect of any spell when
it is first cast, ignore any potion when it's first applied, or cross the
boundary of a [Circle of Protection](#circle-of-protection). The spell ends if
the spell [Disenchant](#disenchant) is cast upon the spellcaster (although the
spellcaster can use the Resist Magic to prevent the [Disenchant](#disenchant)
from removing any other spells upon them). This spell cannot be cast on anyone
other than the spellcaster and will only protect the spellcaster, not anything
they have or possess.

### Ritual of Banishment (6th Circle) ### {#ritual-of-banishment}

__Uses:__ Special, see below - __Verbal:__ 40 words in "burst" form, otherwise
special, see below

This spell allows the spellcaster to use their knowledge of magic and the planes
to shift a creature back to its home dimension or to scatter the magic of a
being. The spellcaster has two ways of performing this ritual. If not given time
to prepare a stronger ritual, the spellcaster may simply use this spell in a
"burst" fashion. To do so, the spellcaster must get the attention of the
creature and say the 40 words, which must begin with "I banish you to the place
from which you came..." During the casting of the spell, the spellcaster is
still open to any retaliation from the creature being banished.

If given more time, the spellcaster can craft a more potent spell to send a
creature to its home plane or strip the being of magics, potentially weakening
it in the process. The player may do so by performing a ritual in front of the
EH or MM. By reading the available magic and essences of the creature, this
ritual may grant the spellcaster knowledge and further steps that can be taken
to banish or weaken the creature, possibly leading to the final incantation.
After the initial casting of the ritual, this spell is unleashed when the
spellcaster uses the researched magic upon the creature. The prepared spell may
only be good for one attempt, whether successful or not. Greater success in
crafting this magic may be achieved through knowledge of the True Name of the
creature, a physical or magical part of it or through a particularly potent
ritual.

Players who use drama and theatrics in their ritual are more likely to achieve
better results. This spell will only function upon NPCs. Once this spell is
attempted upon a creature, either in short or long formats, the spellcaster may
not cast this spell again for an hour. You are encouraged to use a small
timepiece to keep track of this time. Creatures that are shown to be too
powerful from the preparation ritual do not weaken the spellcaster. You may not
begin a preparation ritual on the same day as an uncompleted previous spell.
Upon completion of a more in-depth ritual, the spellcaster must ask the EH or
MM how long this spell is exhausted for, which may be the rest of the day. If
you know this spell, you must inform the EH or MM at check-in.

### Séance (4th Circle) ### {#seance}

__Uses:__ 1 - __Verbal:__ 20 words to start - __Material:__ 3-minute
hourglass/timer

This spell allows the spellcaster to have an extended discussion with a spirit,
either one of another world or of a soulless character. Upon informing the EH or
MM of their intent to cast this spell, the spellcaster must start the ritual by
flipping the hourglass. If the spirit does not arrive within the first three
minutes, then the casting is not used. If the spirit arrives, let the glass run
out and flip it again. The spellcaster and spirit may then speak freely until
all the sands have fallen. If the spirit stays longer than three minutes, the
spellcaster may continue to converse with it. Please note that this spell does
not change any behavior on the part of the spirit, and it may choose not to
talk.

Whether a soulless character can answer is entirely up to the discretion of the
EH or MM, who must be present for the ritual. This spell in no way grants the
knowledge of the circumstances of a soulless character's death. If they are
allowed to be contacted, the soulless PC can still refuse to answer, is not
compelled to speak, can lie or tell the truth freely, and can end the séance at
any time. A PC contacted with a séance must leave after three minutes.

### Second Chance (6th Circle) ### {#second-chance}

__Uses:__ 1 - __Active:__ Speak to EH or MM

This spell allows the spellcaster to recover once, even from the most grievous
of wounds and situations.

After the spell is cast, the spellcaster may activate the spell, which removes
them from play. They must then go find the EH or MM. All stealable items in
possession of the spellcaster must be left behind. This spell may be activated
even if the spellcaster is dead or soulless.

The EH or MM will then place them somewhere on site (location determined by the
EH or MM). Upon being placed, the spellcaster is alive and unwounded.

### Seed of Life (6th Circle) ### {#seed-of-life}

__Uses:__ Unlimited, while spellcaster has MC handy - __Verbal:__ 30 words and
an explanation - __Material:__ 2 tokens with the spellcaster's name and the
words "Seed of Life" on it - __Caveats:__ [Regeneration](#regeneration-caveat),
[Basic Regeneration](#basic-regeneration)

When cast on a dead body, this spell grants the recipient a basic regeneration.
The spellcaster must hand the MC to the recipient when the spell is cast. If the
recipient is soulless, they must alert the spellcaster after completion of the
verbal that the spell has failed and hand the MC back to the spellcaster.

Once the spell ends, the recipient needs to return the MC to the spellcaster as
soon as reasonably possible. Other than this, the MC is neither stealable nor
transferable in any way. If the recipient is rendered soulless, the spell ends.
If the recipient is diseased, this spell will also cure them of their disease
upon completion of the spell (although the regeneration time will still be
doubled from the effects of the disease). This spell has no effect on animated
undead.

If the spell is cast with both tokens on the same recipient, then the recipient
will regenerate in three-fourths the normal time (usually 90 seconds).

### Shapeshifting (4th Circle) ### {#shapeshifting}

__Uses:__ 2 - __Material:__ Makeup and/or mask and any disguise garb -
__Active:__ Change into disguise - __Caveats:__ [Enchanted
Items](#enchanted-items)

This allows the spellcaster to shapeshift into a humanoid monster of about their
height and size. This transformation takes as long to complete as it takes the
player to change into the appropriate disguise outfit. The type and features of
the monster are up to the player. Once the shapeshifting is complete, the player
will respond to the spell [Identify](#identify) as the new type of monster. This
spell will mimic a general monster type, and cannot accurately impersonate a
named or unique monster, or appear to be another PC. You are free however, to
attempt to convince your victims that you are more important than you actually
are.

The shapeshifted form confers no combat benefit or other NPC power, though they
can appear to wear armor or carry larger weapons to complete the disguise. You
may in no way signal to NPCs that you are NPCing. The shapeshifted form ends if
you are killed or if any part of your disguise is [disenchanted](#disenchant).

In addition, at the door of the event, the player is allowed to ask the EH or MM
to borrow an appropriate mask for the event in order to complete the illusion.
There is no guarantee that they will be able to provide the materials, so you
may wish to bring your own.

### Skew Divination (3rd Circle) ### {#skew-divination}

__Uses:__ 2 - __Verbal:__ 30 words - __Material:__ Scroll with a name of an
item, person, group, place, or situation - __Active:__ Give scroll to EH or MM

This spell will alter the next [Guidance](#guidance), [Fortune
Tell](#fortune-tell), [Precognition](#precognition), [Find the
Path](#find-the-path), [Foretell](#foretell), [Séance](#seance),
[Vision](#vision), or [Prophecy](#prophecy) spell cast about the target at that
event, giving them misinformation. How much the spell is altered is up to the
EH or MM. To cast this spell, the spellcaster must write the name of the target
(item, person, group, place, or situation) on a scroll, sign the scroll, and
give the scroll to the EH or MM.

### Soul Bane (3rd Circle) ### {#soul-bane}

__Uses:__ 1 - __Active:__ Destroy a dead body. Following the final body
destroying blow the spellcaster says "Soul Bane." - __Caveats:__ [OOC
Calls](#ooc-calls)

This spell alters the next [Call the Soul](#call-the-soul) cast on the target by
reversing which object is successful and unsuccessful. The spellcaster must
inform the EH or MM whose body they destroyed and cast Soul Bane on as soon
possible. The effect triggers the next time the target's soul is called. The
spell ends after the first [Call the Soul](#call-the-soul), whether it was
successful or not. Only one Soul Bane can be active on a person at a time.

### Speak (1st Circle) ### {#speak}

__Uses:__ 2 - __Verbal:__ "Speak, friend... " - __Material:__ An offering for
the creature to be spoken with - __Active:__ The spellcaster approaches the
creature with no weapons and with an offering in plain sight, and hands it to
the creature

This spell allows the spellcaster to approach a creature and present an offering
to them. If the offering is taken, the creature now has the ability to speak and
understand the language of the spellcaster. This ability lasts until the
creature is no longer in possession of the offering. No creature approached has
to take the offering, nor is there any guarantee that the creature will speak to
you.

### Speak with Dead (1st Circle) ### {#speak-with-dead}

__Uses:__ 10 - __Verbal:__ An explanation, followed by a question - __Caveats:__
[Compulsions](#compulsions)

This spell allows the spellcaster to ask a corpse one "yes or no" question. The
corpse may only answer "Yes," "No," or "Abstain," and it may not lie. An
abstention means that the spirit cannot or does not want to answer the question.
Before asking the questions, the spellcaster must explain to the corpse's player
what the acceptable responses are and that the character may not lie.

### Strange Brew (1st Circle) ### {#strange-brew}

__Uses:__ Special, see [Alchemy](#alchemy) - __Caveats:__ [OOC
Calls](#ooc-calls), [Potions](#potions-caveat)

You may create 2 additional types of potions listed below using
[Alchemy](#alchemy). Upon learning Strange Brew, you must record the potions you
are choosing to learn, and their specific rules, in your spellbook:

* (1 Point) [Panacea]{#panacea}: Cures the target of all disease and poisons. If
  the target is not diseased or poisoned, this grants them immunity against the
  next time they would become diseased or poisoned.
* (1 Point) [Potion of Heal Limb]{#potion-of-heal-limb}: Heals all damaged limbs
  of a living recipient
* (2 Points) [Potion of Animate Lesser Undead]{#potion-of-animate-lesser-undead}:
  Animates a corpse as per spell the   [Animate Lesser
  Undead](#animate-lesser-undead). The spellcaster must also provide the
  necessary material requirements of that spell for when the potion is
  administered. Treat the person who applies this potion as the controller of
  the undead.
* (2 Points) [Poison Potion]{#poison-potion}: Create a death, sleep, or truth
  poison as per [Create Poison](#create-poison).
* (3 Points) [Potion of Acid]{#potion-of-acid}: Deals 200 blows to a body, only
  usable on non-living objects.
* (3 Points) [Potion of Combat Raise Dead]{#potion-of-combat-raise-dead}: Raises
  a dead character, healing all of their injured limbs.
* (3 Points) [Potion of Disenchant]{#potion-of-disenchant}: Disenchants an
  enchanted or magic item, as per the spell, [Disenchant](#disenchant).
* (4 Points) [Weapon Oil]{#weapon-oil}: Apply to a weapon for 60 seconds, and
  affix a ribbon to it with the alchemist's name, the potion sigil, and either
  Silver or Magic. The weapon swings Silver or Magic depending on which you
  chose until the end of the event. The effect of the weapon oil ends early if
  the weapon is broken or disenchanted. This has no effect on a weapon that is
  already Silvered or Magic.
* (5 Points) [Potion of Animate Undead]{#potion-of-animate-undead}: Animates a
  corpse as per the spell [Animate Undead](#animate-undead). The spellcaster
  must also provide the necessary material requirements of that spell for when
  the potion is administered. Treat the person who applies this potion as the
  controller of the undead.
* (5 Points) [Potion of Soul Snare]{#potion-of-soul-snare}: Recipient's soul is
  returned to them as if the spell [Call the Soul](#call-the-soul) was
  successfully cast on them and their soul was called. The EH or MM must be
  present for this potion to be used.
* (5 Points) [Potion of Resistance]{#potion-of-resistance}: Gain one call of
  [Resist Magic](#resist-magic) as per the spell.
* (7 points) [Potion of Séance]{#potion-of-seance}: The recipient can cast a
  usage of [Séance](#seance) as per the spell. If the [Séance](#seance) fails,
  the potion is used up.

This does not grant any points to alchemy.

### Transformation (6th Circle) ### {#transformation}

__Uses:__ 3 - __Material:__ A pair of Transformation Claws - __Caveats:__
[Regeneration](#regeneration-caveat), [Advanced
Regeneration](#advanced-regeneration), [OOC Calls](#ooc-calls),
[Precast](#precast), [Suspension](#suspension)

The spellcaster releases their inner nature. The effect is a transformation.
Each individual spellcaster may have a different form, but that form is
consistent to the spellcaster (i.e. Matt's altered form is different than
Sally's, but Matt's altered form is always the same any time he shifts to it).
The spell must be unlearned to alter that form.

A complete description of the altered form must be in the spell description. The
spellcaster must alter their appearance when in the transformed state. They must
wear a different tabard, makeup, prosthetics, mask, or some other major
signifying indicator that they are "not quite right." Details must also be
listed in the spellcaster's spellbook.

Any potions or other spells with a lingering effect cast by the spellcaster are
suspended when they transform. The suspension ends when the spellcaster reverts
to their common form.

All altered forms have the following advantages/disadvantages:

* Has the use of Florentine claws, 18" maximum. Cannot use any other weapons.
  Claws must be of matched length. Cannot use or manipulate anything except with
  those claws (after all, they're the end of their hands). These claws do not
  break the spellcaster's weapon length restriction.
* The claws are physical representations for the spell, and are not really there
  until the spell is cast. They cannot be used by anyone other than the
  spellcaster, and they may only use them when under the effects of the spell.
  The claws must be clearly labeled with the phrase "Transformation Claws" and
  the spellcaster's name. The claws are not considered weapons for the purpose
  of Spell Failure. Furthermore, they are unbreakable and immune to the effect
  of a boulder, though the spellcaster is killed as normal if struck in the
  claws.
* Cannot cast any spells while in altered form, except those granted as part of
  the Transformation.
* The spellcaster may revert back to their common form, ending the spell at any
  time.

A spellcaster has 7 points to construct their altered form:

Armor Options:

* _Natural Armor:_ 1 point. Grants light armor on all hit locations. Can only be
  purchased once. Cannot be used in conjunction with Regenerating Armor.
* _Regenerating Armor:_ 3 points. Grants light armor on all hit locations, which
  can be regenerated. Can only be purchased once. To activate the regenerating
  effect, the spellcaster must kneel without attacking or blocking or lie on
  their back. Once so positioned, the spellcaster must remain still for 10
  seconds per damaged hit location (repairing every hit location would take 70
  seconds). If interrupted before all damaged hit locations are repaired, the
  effect fails with no armor points regenerated. Cannot be used in conjunction
  with Natural Armor.

Both armor options may be repaired by casting [Repair Item](#repair-item).

Weapon Upgrades:

* 1 point for 2' claws.
* 2 points for 2'6" claws.
* 3 points for 3' claws.
* 4 points for 3'6" claws.
* _Poison Weapons:_ 4 points. The spellcaster may call "[Poison](#poison)" each
  time they swing their claws.
* _Armor-Piercing Weapons:_ 5 points. The spellcaster may call
  "[Piercing](#piercing)" each time they swing their claws.
* _Axe, Hammer, or Mace claws:_ 1 point. The spellcaster may call "Axe,"
  "Hammer," or "Mace" each time they swing their claws. The specific call must
  be chosen when the form is initially created. This option may only be taken
  once. The spellcaster is encouraged to make the claws reflect the nature of
  the call chosen.

Miscellaneous Abilities:

* _Limited Regeneration:_ 2 points for one use. All of the spellcaster's limbs
  regenerate after 30 seconds, as an unlimited effect. Cannot be in combat while
  healing limbs in this fashion. The spellcaster has a single [advanced
  regeneration](#advanced-regeneration). 1 point for each additional extra life
  beyond the first.
* _[Death Watch](#death-watch):_ 2 points. As per the spell.
* _[Immunity to Poison](#immunity-to-poison):_ 1 point. May only cast on self,
  otherwise as per the spell.
* _[Cure Disease](#cure-disease):_ 1 point. May only cast on self, otherwise as
  per the spell.
* _[Heartiness](#heartiness):_ 1 point. As per the spell.

Spells gained through transformation are as per the spell specified, including
number of castings. Each time you transform, the uses reset. Spells gained this
way may only be cast while transformed.

### Transmute Weapon (1st Circle) ### {#transmute-weapon}

__Uses:__ 3 - __Verbal:__ 10 words - __Active:__ Hold the target weapon with 2
hands - __Caveats:__ [Enchanted Items](#enchanted-items), [OOC
Calls](#ooc-calls), [Weapon Calls](#weapon-calls)

This spell gives the spellcaster the ability to modify the shape and properties
of a melee weapon temporarily. After preparing the weapon with the spell, the
weapon user must call one of the following weapon types the next 3 times the
weapon is swung: "Axe", "Mace", "Hammer", "Spear", or "Dagger". The spellcaster
chooses an option upon casting the spell and must inform the recipient which
option the weapon has been transmuted to. These calls are expended and the
casting is spent whether the user scores a successful hit or not, unlike other
spells with the Weapon Calls caveat. Also unlike those other spells, anyone may
wield the transmuted weapon to make the calls.

### Vision (5th Circle) ### {#vision}

__Uses:__ 1

This spell allows the spellcaster to ask the EH or MM a question. The EH or MM
will then reveal to the spellcaster as complete a description as they are
willing to give, giving them a vision relating to it.

### Ward: Enchanted Beings (5th Circle) ### {#ward-enchanted-beings}

__Uses:__ Unlimited - __Verbal:__ 20 words, repeated continuously, stating
purpose of spell - __Material:__ Focus - __Caveats:__ [Chanting](#chanting),
[Suspension](#suspension), [Wards](#wards)

This spell prevents [Enchanted Beings](#enchanted-beings) from attacking the
spellcaster while it is active.

### Ward: Undead (2nd Circle) ### {#ward-undead}

__Uses:__ Unlimited - __Verbal:__ 10 words, repeated continuously, stating
purpose of spell - __Material:__ Focus - __Caveats:__ [Chanting](#chanting),
[Suspension](#suspension), [Wards](#wards)

This spell prevents [Undead](#undead) from attacking the spellcaster while it is
active.

### Zombie Walk (1st Circle) ### {#zombie-walk}

__Uses:__ Unlimited, 1 at a time - __Verbal:__ 10 words, plus an explanation -
__Active:__ Touch the target - __Caveats:__ [Undead](#undead), [Walking
Dead](#walking-dead)

This spell allows the spellcaster to animate a corpse, making it follow them for
as long as they concentrate on the spell. If the spellcaster engages in combat
either by attacking or by being struck, the spell ends and the corpse falls to
the ground. In order to cast this spell, the spellcaster must recite the verbal
and give the player of the corpse a brief explanation of what they should do,
making sure they know when to fall down.

# Rules for Event Holders # {#rules-for-event-holders}

## Becoming A Realms Event-Holder ## {#becoming-a-realms-event-holder}

To attempt to become an Event Holder you must first throw an event with your
name listed as an Event Holder. To be a Realms legal event, the following
conditions must apply:

* It must be open to the public.
* It needs to have been advertised with full event information at least two
  weeks before the date of the event on RealmsNet. If RealmsNet is unavailable,
  advertisement must include at least one of the following means instead:
  Publication in The *View from Valehaven* or *Creathorne Chronicles*, or sent
  through a mass mailing or electronic mailing, which must include the known
  addresses of all Realms EHs who have held Realms events in the previous year.
* The location and the price of the event must be announced at least two weeks
  before the event is scheduled to take place. Any significant rule changes,
  especially those affecting normally legal powers and abilities of PCs, must be
  announced in the advertisement.
* The names of the EHs, at most one per day, must be announced at least 5 days
  before the event is scheduled to take place.
* A site must be confirmed before an event can be advertised as a legal Realms
  event.
* The event must be at least six hours long.
* At least 30 people (including marshals and NPCs) must attend and the EH must
  be able to prove attendance through signatures.
* Players must have the option of playing their normal PCs, within such bounds
  and restrictions as previously advertised by the EH. Players who do so,
  despite any such advertised rule changes which may adversely affect their PC,
  are presumed to freely accept such changes or restrictions.
* The rules must be based either on the current Omnibus to the Realms or on
  another rule system approved at the most recent Event Holders' Council. All
  events must uphold the [Code of Conduct](#code-of-conduct) at a minimum. Event
  Holders may add additional rules to the Code of Conduct for their event, but
  they may not remove them.
* If the EH(s) are under the age of 18, they must have an adult(s) either as a
  co-EH(s) or as a Listed Contact(s) in the event description. One of the said
  adults must be on-site for the event.
* An EH's PC may not benefit in any way from their event. Their PC may not take
  possession of any non-currency stealable item released at their event for more
  than one day. They may not learn any spells as a result of attending their own
  event.
* If any individuals are not allowed to attend, the event announcement may
  include a link to any or all of their names in a separate list. This list must
  only be accessible with a RealmsNet account.

As an Event Holder you must:

* Place the safety and welfare of players above all else.
* Show concern and caution towards sick and injured players.
* Be impartial, consistent, objective, and courteous when making decisions.
* Accept responsibility for your actions and decisions, as well as those of your
  staff.
* Respect the individuality of other players and event holders.
* Avoid any situations which may lead to or be construed as a conflict of
  interest.
* Always be respectful to other members of the community.
* Respect the land you are using, follow any site rule imposed by the land
  owner, and leave the site in better condition than you found it.
* Keep up to date with the latest rules of the game and principles of their
  application.
* Refrain from any form of personal abuse or harassment towards players or other
  Event Holders.
* Respect the rights, dignity, and worth of all people involved in the game
  regardless of their race, gender, sexual orientation, ability, or cultural
  background.

Throwing a legal event gives an EH certain privileges and responsibilities above
and beyond a normal player.

* To validate your event you must confirm it electronically with the official
  [Event List Administrator](#event-list-administer) within one month of the
  date of the event.
* Also within one month, if any instances of rules violations were reported, you
  must send to the [Complaint Review Board](#complaint-review-board) a summary
  of those reports, conclusions of investigations into them, and consequences
  levied. EHs and the Complaint Review Board have the responsibility to maintain
  the privacy of those involved in reports to the extent possible.
* If you release a [Realms Magic Item](#magic-item-rules) at your event you must
  submit the name, physical description, and magical description to the
  [Minister of Magic Items](#minister-of-magic-items) within one month of that
  event. The actual prop for the item does not need to be distributed to players
  at that time, but an EH that does not designate a magic item for an event
  within one month forever loses the opportunity to [release an item for the
  event](#magic-item-rules).
* As an EH you qualify to attend [the Event Holders' Council
  meeting](#the-event-holders-council) for the year following your event.

## Magic Item Rules ## {#magic-item-rules}

Each EH is permitted to create one non-event specific magic item (a "[Realms
Magic Item](#magic-item-rules)") at each event for which they receive EH credit.
Only one magic item per Event Holder may be released for each event. Such magic
items will retain their powers and abilities at all subsequent events at which
they are pre-registered/checked in, and approved. Magic items are released on
the date of the event the EH is creating them for and must be submitted to the
[Minister of Magic Items](#minister-of-magic-items) within one month
of that event.

If an EH has not held a legal event during a calendar year, any items that they
back are still considered backed for an additional year, plus the time before
the next scheduled EHC (typically in late January or early February). This means 
if an EH decided to take a calendar year off from throwing an event they can 
throw an event during the next calendar year and their items will retain their
backing. However, if an EH does not hold a legal event during that following
calendar year, any magic items that they released will be considered
effectively destroyed as of the EH Council immediately following that year.

All magic items must have the following written on them or on an accompanying
tag: the item's name; the responsible EH's name; the date it was released; the
labels "Magic" and "Stealable". The [list of legal Magic
Items](https://www.realmsnet.net/magic_items) is available on Realmsnet.

## Challenging an Event ## {#challenging-an-event}

Questions on the validity of an event must be submitted to the Administrative
Meeting Organizer(s) no later than one week prior to the Players' Meeting. The
organizer(s) are then responsible for requesting from the EH(s) a list of
players present at the event, and for attempting to contact at least 10 of them
for a statement on if they felt the event met the legal requirements or not, and
why. This information must then be presented during the administration section
of the Event Holders' Council where all EHs present will vote on the legality of
the event.

If the EH Council finds that the event was not legal, any magic items issued
must be pulled, any ticks issued are removed, and the EH(s) who held the event
lose their Event Holder status for the year in question. The EHC may impose
additional sanctions, including suspension of Event Holding privileges, as they
see fit.

# Rules Changes and Administrative Meetings #{#rules-changes-and-administrative-meetings}

## Proposals ## {#proposals}

Any player may propose a change to the Omnibus, [Standing
Policies](./StandingPolicies.html){.compatibility}, or [Complaint Review Board
policies](./ComplaintReviewBoard.html){.compatibility} to potentially be 
considered during the next series of Administrative Meetings. These are the 
Players' Meeting and Event Holders' Council. The purpose of these meetings is to
vote on modifications to the rules and administrative positions for the 
following year.

Beginning January 1st of the year proposals will be voted on, they may advance
to the Players' Meeting with the registered support of at least 30 players 
eligible to vote at the next Players' Meeting. Eligible players should be able
to add support and comments even after a proposal meets its required threshold
for meeting inclusion. This support and the proposal must be submitted to the
organizer(s) of the Players' Meeting before discussion on any tabled or other
day-of proposals.

Proposals may bypass the Players' Meeting if they gain enough Event Holder
support. For this bypassing, at least 10 EHs legally able to attend and vote at
the Event Holders' Council must support the proposal. Eligible EHs should be 
able to add support and comments even after a proposal meets its required 
threshold for meeting inclusion. This support and the proposal must be 
submitted to the organizer(s) of the Event Holders' Council at least one week 
prior to the Players' Meeting.

Proposals may continue to be submitted online up to the day before the Players'
Meeting, and in person the day of the Players' Meeting. No new proposals will be
accepted after the Players' Meeting.

Proposals must meet certain requirements to be voted on at Administrative
Meetings.

A proposal must contain the following:

* The submitter's name and a way to contact them;
* A title and brief summary of the changes, including the intended impact of the
  proposal;
* The exact wording to be added, removed, or modified within the Omnibus or
  Standing Policies, along with the section to be modified.

A proposals may also contain:

* Multiple options to be voted between if the proposal passes, such as a
  proposal seeking to clarify a rule in one of two ways;
* The name and contact information of a proxy player, to accept amendments to
  the proposal at the Players' Meeting.

## Standing Policies ## {#standing-policies}

Standing Policies for the Realms are official but temporary rules or procedures.
They are as binding as rules within the Omnibus. Each Standing Policy must
either state a specific expiration date within one year of its most recent
update or be considered in effect until further notice.

In addition to the proposal process described above, Standing Policies can be
added, amended, or removed at an Emergency EH Meeting or at the EHC by a
two-thirds majority vote of yes over no, with abstentions not being
counted.

At each EHC, Standing Policies in effect without specified expiration dates must
be voted on for renewal. Any successful vote to add or amend a Standing Policy
during that meeting counts as renewal for that Standing Policy. Standing
Policies are renewed by a simple majority of votes.

## Players' Meeting ## {#players-meeting}

Any player who has attended a legal Realms Event in the previous calendar year
can attend the Players' Meeting. The meeting is usually held several weeks prior
to the Event Holders' Council. This meeting is a forum for feedback on rule
proposals, rule changes, etc. If two-thirds of those voting on a proposal
support it, the proposal will be included as an official proposal at the Event
Holders' Council.

Those who attend the Players' Meeting shall be permitted to select two
representatives (neither of whom are already eligible to vote at the next EHC)
to attend the Event Holders' Council. One of these representatives will have the
same voting ability at the meeting as an EH, and may accept amendments to player
proposals submitted at the Players' Meeting. One representative shall be
designated the primary, and the other will be the backup. Only the primary will
be able to cast votes and accept amendments on behalf of an author, but both
representatives may contribute to the discussion of proposals. If only one is
present at any given time, they have they powers to vote and to accept
amendments.

It is the Players' Representative's responsibility to:

* Unbiasedly collect player feedback on proposals,
* Be organized and responsive,
* Vote as the majority of player feedback indicated.

Suggested Examples:

* Create a form with all proposals and a rating system, and email it to the
  announcements mailing list;
* Go to Realms Gatherings and be an open forum;
* Obtain direct quotes from players to strengthen your arguments.

## The Event-Holders' Council ## {#the-event-holders-council}

Any EH who has thrown an event between January 1 and December 31 (inclusive) of
the previous year may attend the annual Event Holders' Council.

Although any EH may attend the meeting, there are certain criteria that you must
meet in order to vote there. You must have hit at least six events, including
your own, in the same year that you held your event. Additionally only one EH
per day of the event will be eligible to vote. For the purposes of this rule, a
day runs from sunrise that calendar day to sunrise the next calendar day, and an
event day must be at least six hours long. For example, an event starting at
9:00 pm on Friday and ending more than six hours after sunrise on Sunday may
have three EHs.

If an EH cannot or does not attend their own event (due to real life
emergencies, extenuating circumstances, etc.), they may still attend the EH
meeting, but their eligibility to vote and back magic items will be reviewed by
the remaining members of the Event Holders' Council to determine their
eligibility on a case-by-case basis.

No proxies will be accepted at the Event Holders' Council. Voting at the Event
Holders' Council will be by majority rule - a two-thirds majority of yes over
no, with abstentions not being counted, equals a majority. No guests shall be
permitted to attend the Event Holders' Council. The Moderator, Co-Moderator, and
Secretary will be considered staff and not guests. Expert witnesses may be
allowed if necessary, with an appropriate vote.

The Players' Representative selected at the Players' Meeting will have 5 minutes
before the first vote to introduce themselves to the council, to communicate
perspectives that were particularly important to the players they represent, and
to share any other information they or the players they represent consider
relevant and significant to the council for the upcoming votes.

Each year at the Event Holders' Council several marshals are elected to help
streamline the administration involved with running the Realms. You can
read more about the process and the positions under [Realms Administrative
Positions](#realms-administrative-positions).

At the end of the Event Holders' Council, the EHs will discuss and vote on
whether to and how to allow [changes to spellbook
conversion](#converting-spellbooks) for the upcoming year based on the changes
made to the Omnibus, such as allowing spellcasters to update their Spell Mastery
when spells change names or allowing players additional freedom in changing
their spellbooks.

## Emergency EH Meetings ## {#emergency-eh-meetings}

Sometimes, a serious issue may arise that needs to be handled before the next
EHC. For these issues, Emergency EH Meetings can be held. Attendance at these
meetings is open to every EH who held a legal event in the previous calendar
year, every EH who held a legal event in the current calendar year, and the
Players' Representatives elected at the most recent Players' Meeting. To be
eligible to vote, EHs must also have attended at least 6 events (including their
own) between the current and prior calendar years. Only the primary Players'
Representative may vote, unless only the backup is present.

Any serious issue(s) may be brought to the attention of the Event Holders' List.
Anyone may request that an Emergency EH Meeting be convened to address such
issues. The person requesting the meeting is allowed to attend the meeting if it
happens, regardless of their status as an Event Holder. If within two weeks of
the original request at least 50% of the EHs eligible to attend an Emergency EH
Meeting agree that a meeting is required to discuss the issue(s), then an
Emergency EH Meeting must, to the greatest reasonable extent, be organized and
set to occur no later than eight weeks after the initial request. Quorum for
voting at this meeting is 50% of those eligible to vote. The person calling for
the meeting will work with the current Administrative Meeting Organizer(s).
Emergency EH Meetings may be held remotely via commonly available conferencing
software.

The proceedings of an Emergency EH Meeting should remain close to the topic(s)
of the original issue(s). However, if additional topics related to the original
matter are identified prior to or during the course of the meeting, the scope of
the meeting may be expanded to include the new issues at the discretion of the
assembled Event Holders.

Emergency EH meetings may add, remove, or change [Standing
Policies](./StandingPolicies.html){.compatibility}, and may adjust CRB decisions
and policies not codified in their Rules and Operating Procedures. They may not
make changes to the Omnibus or to the [Rules and Operating Procedures of the
Complaint Review Board](./ComplaintReviewBoard.html){.compatibility}.

An Emergency EH Meeting must be advertised in the same way as an official Realms
Event (see [Becoming an Event Holder](#becoming-a-realms-event-holder)).

## Adjudication of Complaints ## {#adjudication-of-complaints}

The Complaint Review Board is responsible for receiving and investigating
Complaints in accordance with the [Rules and Operating Procedures for the
Complaint Review Board webpage](./ComplaintReviewBoard.html){.compatibility}.

Upon receipt of an investigation report from the Complaint Review Board, the EHs
will decide if the matter is serious enough to warrant an Emergency EH Meeting.
No sooner than 48 hours and no later than two weeks after the report has been
distributed, the Administrative Meeting Organizer will solicit support for an
emergency EH meeting. The purpose of this delay is to allow the EHs time to
fully process the investigation report. If this results in an Emergency EH
Meeting, then the Complaint will be adjudicated at that meeting. Otherwise, the
Complaint will be adjudicated at the next regular session of the EHC.

Regardless of whether it happens at an Emergency EH Meeting or a regular EHC,
adjudication of Complaints investigated by the CRB is governed by the policies
and procedures on the Rules and Operating Procedures for the Complaint Review
Board webpage.

# Realms Administrative Positions # {#realms-administrative-positions}

## List of Positions ## {#list-of-positions}

The administrative positions elected each year are:

* [Administrative Meeting Organizers](#meeting-organizers)
* [Complaint Review Board](#complaint-review-board)
* [Death Marshal](#death-marshal)
* [Event List Administrator](#event-list-administer)
* [Facebook Moderation Team](#facebook-moderation-team)
* [Minister of Magic Items](#minister-of-magic-items)
* [Omnibus Editorial Committee](#omnibus-editorial-committee)

## Elections ## {#elections}

For each of these administrative positions, people are elected at the Event
Holders' Council every year. To be eligible to be elected, someone must either
volunteer for that position or be nominated by someone else and then accept the
nomination. If someone wishes to run for a position but will not be able to
attend the EHC, they may tell a meeting organizer or staff member ahead of time
about their intentions, and the organizers will have the power to accept
nominations on their behalf. For the positions that are not on committees or
boards, some people may choose to run as a pair, in which case those two people
are treated together as one candidate. Both members in a pair must agree to this
to be eligible to run together.

Once candidates have been identified for a position, the voting members of the
EHC will vote by [Approval Voting](https://en.wikipedia.org/wiki/Approval_voting)
to elect a candidate. For positions with backups, the backup will be the person
with the second-most votes. For committees and boards, once a slate of
candidates has been selected, there will be an additional confirmation vote.
This is to give the EHC the opportunity to evaluate aspects such as diversity of
the committee or board and how well the members will work together. A 2/3 vote
of "yes" over "no" is needed to confirm a committee or board. If the
confirmation vote for a committee or board fails, the EHC will have discussion
to identify why the vote failed and to pick a new slate. Once a new slate has
been chosen, a new confirmation vote will be held.

## Resignation ## {#resignation}

If one or more individuals who hold one or more of these Administrative
Positions wish to resign from their position(s), they must notify the EH List of
their intention to do so. The resigning individual(s) shall not be prevented
from vacating their positions and are not required to justify their
departure.

## Removal ## {#removal}

If one or more individuals who hold one or more of these Administrative
Positions are thought to be unable or unfit to continue in their respective
role(s), an Emergency EH Meeting may be called to discuss the position(s) in
question. A meeting called for this reason must follow the rules laid out in the
section on Emergency EH Meetings. If an Emergency EH Meeting is successfully
organized (i.e. a sufficient number of EHs agree that the meeting is necessary),
the individual(s) in question shall be temporarily restricted from performing
the duties assigned to their position(s) until the conclusion of the
meeting.

During the meeting, the party who called for the meeting presents their
evidence supporting their case for removal of the individual(s) in question and
those individual(s) will have the opportunity to defend their position. A vote
shall then be held for each individual in question. A 2/3rds majority (not
including abstentions) is required to remove an individual from their position.

## Filling Vacancies ## {#filling-vacancies}

If at any time one or more Administrative Positions become vacant, and the
vacancy(ies) prevent the remaining Administrative Positions from performing
their duties (e.g. there is not a well-defined line of succession for the
position(s) in question, or the position(s) is/are part of a committee which
will not be able to function with one or more vacancies), then the vacant
position(s) must be filled by election or appointment at an Emergency EH
Meeting. The listed backup, if availalbe, will fill this position until the
Emergency EH Meeting is held. If the position(s) is/are vacant as the result of
removal, then the process to fill the vacated position(s) may be held at the
same Emergency EH Meeting at which the removal vote was called. If the
vacancy(ies) do not prevent the remaining Administrative Positions from
performing their duties, the position(s) may be filled by the listed backup. If
the listed backup is unwilling or unavailable to serve, the position may remain
vacant until the next regular election.

## Transition Period for Roles ## {#transition-period-for-roles}

If any outgoing position or committee has unfinished business when elections for
new positions are held, the outgoing individuals may finish their work if
granted permission to do so from the EHC. A vote must be called for each
relevant position; a simple majority of votes in favor (excluding abstentions)
will allow the outgoing individuals to finish their work. Regardless of the
outcome of the vote, the outgoing individuals may not start new work after this
vote is held (unless they were re-elected to the position), and the newly
elected individual or committee is responsible for all new business submitted
after their election.

If an outgoing individual or committee is allowed to finish their work, they
must do so within 30 days. If the outgoing individual or committee is unable to
conclude their work after that time, the newly elected individual or committee
shall immediately take responsibility for the business. Within 15 days (counted
from the end of the previous 30-day period) the outgoing individual or committee
must brief the newly elected individuals on the situation and transfer all
relevant documents, evidence, testimony, etc. to the new parties.

## Position Descriptions ## {#position-descriptions}

### Administrative Meeting Organizers ### {#meeting-organizers}

The Administrative Meeting Organizers are responsible for organizing,
announcing, and running both the Players' Meeting and the Event Holders' Council
for the following calendar year.

The current Administrative Meeting Organizer is Dani Lacasse. The backup
Administrative Meeting Organizer is Crystal Welch. They can be reached at
<ehc@admin.realmsnet.net>.

### Complaint Review Board ### {#complaint-review-board}

The Complaint Review Board (henceforth referred to as the CRB) is a group of
five primary members and three alternate members tasked with receiving
Complaints of potential violations of Realms rules (including the [Code of
Conduct]{#code-of-conduct} and the [Rules for Event
Holders]{#rules-for-event-holders}) and investigating their validity. Following
the investigation, they will present the matter to the Event Holders of the
Realms. Additional information regarding the scope and processes of the CRB,
including how to submit a Complaint to the CRB, can be found on the [Rules and
Operating Procedures of the Complaint Review Board web
page](./ComplaintReviewBoard.html){.compatibility}.

The primary members are responsible for conducting the regular business of the
CRB. However, if at any point a primary member becomes unavailable, through
recusal or through other means, they will be replaced by one of the alternate
members. It is for this reason that the alternate members will be included in
all of the proceedings of the CRB, although they are not granted the authority
to vote until they are activated.

The CRB must always have at least five members available to conduct its
business. If at any time fewer than five members are available, the CRB must
halt the current process and call for an [Emergency EH
Meeting]{#emergency-eh-meetings}. A meeting called for this reason bypasses the
requirement that a certain number of EHs need to agree to hold the meeting and
must either elect enough temporary members to the CRB to reach the required five
members or require that the Complaint shall be addressed by all present EHs not
involved in the Complaint, rather than the CRB.

Immediately following the election of CRB membership, the EHC will elect one of
the primary members as the Chairperson of the CRB and a second member as Vice
Chairperson. The EHC must also establish the order in which alternate members
will be activated.

The players elected to the Complaint Review Board are Hillary Fotino, Nataliya
Kostenko, Dani Lacasse, Tucker Noyes, and Paul Tilton. The three alternates, in
order of activation, are Emily Murphy, Steve Nelson, and Crystal Welch.

The current Chairperson of the Complaint Review Board is Tucker Noyes, and the
current Vice Chairperson is Nataliya Kostenko.

The Complaint Review Board can be reached at <crb@admin.realmsnet.net>.

### Death Marshal ### {#death-marshal}

The Death Marshal is responsible for tracking and maintaining ticks that are
reported by EHs from their event for a character, or reported by a character's
player for their own character.

The current Death Marshal is Matt Butler. The backup Death Marshal is Brianna
Meisser. They can be reached at <death@admin.realmsnet.net>.

### Event List Administrator ### {#event-list-administer}

The Event List Administrator is responsible for maintaining an official list of
events. The list is available online at <http://www.realmsnet.net>.

The current Event List Administrator is Ian Pushee.
The backup Event List Administrator is Neil S. Tozier.
They can be reached at <events@admin.realmsnet.net>.

### Facebook Moderation Team ### {#facebook-moderation-team}

There is an official Facebook group for the Realms community. It can be found at
<https://www.facebook.com/groups/therealmslarp/>. The moderators are responsible
for maintaining the group and ensuring that all posted and commented content
follows the Realms Code of Conduct.

The 5 people elected by the EHC to moderate the Official Realms Facebook Group
are Becky Baron, Sean Finn, Sarah Fournier, Dani Lacasse, and Emily Murphy.

The Facebook Moderation Team can be reached at facebook@admin.realmsnet.net.

### Minister of Magic Items ### {#minister-of-magic-items}

The Minister of Magic Items is responsible for maintaining an official list of
[Realms Magic Items](#magic-item-rules). The list is available to EHs online at
<http://www.realmsnet.net>.

The current Minister of Magic Items is Neil S. Tozier.
The backup Minister of Magic Items is Ian Pushee.
They can be reached at <magic_items@admin.realmsnet.net>.

### Omnibus Editorial Committee ### {#omnibus-editorial-committee}

Each year, the Event Holders' Council nominates and elects a five-person
committee to format and maintain a master copy of the rulebook. During the
editing process, the committee has the power to make the following minor changes
to the rulebook:

* Spelling errors
* Grammar errors, such as punctuation, pluralization, etc.
* Spells mentioned that no longer exist in the Omnibus
* Spells that have had their names changed
* Text that was intended to be altered, added, or removed as part of a proposal
  voted in by the EHC, but was omitted from the proposal text and therefore
  creates an inconsistency within the Omnibus. These edits cannot
  substantially change the impact of the proposal
* Rearranging the orders of tables and lists, as long as these rearrangements
  don't change how any rules work
* Updating Section 9 Realms Administrative Positions to reflect the results of
  recent resignations, elections, and/or appointments.

Changes made to the document must be made by unanimous decision by the
committee, who must notify the Event Holders' List of changes not specifically
approved. All copies of the Omnibus produced for public use must be derived from
the master text, which is published on RealmsNET, although formatting and
printing process are at the discretion of the individual publisher. A digital
copy of the Omnibus text will be provided to any member of the community on
request.

Editorial changes as outlined above may continue to be made throughout the year
after the master text is published. The Omnibus Editorial Committee is
responsible for submitting changes to the Event Holders' List. If no objections
are made, the change is automatically accepted and the master text will be
updated. If any objections are made, the proposed change must be submitted
through the official proposal submission process outlined in the Omnibus and
voted upon at the following year's Event Holders' Council.

Alongside the rulebook, the committee will maintain a summary of rules changes.
While not a word-for-word reproduction of what has changed, the summary is
intended to help players identify what has changed each year, and to let players
know where to look within the Omnibus to see new rules. As this document is a
summary, rather than official rules, the committee has full discretion over what
to include each year. That said, the committee is encouraged to talk with
authors of proposals for guidance about the intent of proposals.

The Omnibus Editorial Committee for the 2025 Omnibus is Pi Fisher, Sarah
Fournier, Cameron Huneke, Dani Lacasse, and Kelly Perfetto. The Omnibus
Editorial Committee can be reached at <oec@admin.realmsnet.net>.