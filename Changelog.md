---
pagetitle: 'Changelog'
documentclass: book
geometry: margin=1in
pagestyle: headings
papersize: letter
fontsize: 12pt
toc-depth: 1
lang: en-us
...

# Changes approved February 17, 2024, effective March 29, 2024 # {#changes-2024 -}

## Safety & Marshaling ## {#safety-and-marshaling-2024 -}

* Firearms are now included in the [list](/Realms/year/2024/Omnibus.html#the-rules-we-fight-by){.compatibility} of "real steel" items disallowed from combat or potential combat situations.
* Updated the last sentence under [Playing Dead](/Realms/year/2024/Omnibus.html#playing-dead){.compatibility} to clarify weapon use when ensuring suspected-living bodies are dead.
* Under [Realms Thieves](/Realms/year/2024/Omnibus.html#realms-thieves){.compatibility} a clarification was added to remove "magic or silver weapons" and instead include "labeled as Stealable or Event-Stealable" in reference to things that can be stolen In-Game.

## Combat ## {#combat-2024 -}

* Increased the [maximum arrow length](/Realms/year/2024/Omnibus.html#arrows-javelins-and-other-missile-weapons){.compatibility} from 28" to 29".

## Magic ## {#magic-2024 -}

* Instructions for spellcaster characters are unified under [Spellcaster Basics](/Realms/year/2024/Omnibus.html#spellcaster-basics){.compatibility} for easier reference. There remains a small [Being a Spellcaster](/Realms/year/2024/Omnibus.html#being-a-spellcaster){.compatibility} section in [Creating a Character](/Realms/year/2024/Omnibus.html#creating-a-character){.compatibility} that provides a bit of information before linking to the new full section.
* If spellbook conversion differs from the Omnibus-default procedure for a given year, the overriding rule will be found in [Standing Policies](/Realms/year/2024/StandingPolicies.html){.compatibility}.
* Unified similar but different Active Components for the spells [Armored Cloak](/Realms/year/2024/Omnibus.html#armored-cloak){.compatibility}, [Aura of Protection](/Realms/year/2024/Omnibus.html#aura-of-protection){.compatibility}, [Protection from Missile](/Realms/year/2024/Omnibus.html#protection-from-missile){.compatibility}, [Purity](/Realms/year/2024/Omnibus.html#purity){.compatibility}, and [Resist Death](/Realms/year/2024/Omnibus.html#resist-death){.compatibility} to “Kneel with no weapons or shields in hands, or lie on back”.
* Defined what occurs when a [spell is reset](/Realms/year/2024/Omnibus.html#resetting-a-spell){.compatibility}, and that by default when a player receives a spell reset it resets all their spells.
* Added text to both [Animal Companion](/Realms/year/2024/Omnibus.html#animal-companion){.compatibility} and [Familiar](/Realms/year/2024/Omnibus.html#familiar){.compatibility} to explicitly allow these spells to use the same prop.
* Reduced the [alchemy](/Realms/year/2024/Omnibus.html#alchemy){.compatibility} point cost of the following potions: [Acid](/Realms/year/2024/Omnibus.html#potion-of-acid){.compatibility}, [Mending](/Realms/year/2024/Omnibus.html#potion-of-mending){.compatibility}, [Seance](/Realms/year/2024/Omnibus.html#potion-of-seance){.compatibility}, [Combat Raise Dead](/Realms/year/2024/Omnibus.html#potion-of-combat-raise-dead){.compatibility}. Changed Potion of Cure Disease to [Panacea](/Realms/year/2024/Omnibus.html#panacea){.compatibility}. Increased alchemy points gained from a 6th circle slot.

## Spells ## {#spells-2024 -}

### [Animal Companion](/Realms/year/2024/Omnibus.html#animal-companion){.compatibility} ### {#animal-companion-2024 -}

* Can now use the same MC as Familiar.

### [Armored Cloak](/Realms/year/2024/Omnibus.html#armored-cloak){.compatibility} ### {#armored-cloak-2024 -}

* AC changed to "Kneel with no weapons or shields in hands, or lie on back".

### [Aura of Protection](/Realms/year/2024/Omnibus.html#aura-of-protection){.compatibility} ### {#aura-of-protection-2024 -}

* AC and related spell text changed to "Kneel with no weapons or shields in hands, or lie on back".

### [Circle of Healing](/Realms/year/2024/Omnibus.html#circle-of-healing){.compatibility} ### {#circle-of-healing-2024 -}

* Removed the disposable MC related text within the spell.

### [Cure Disease](/Realms/year/2024/Omnibus.html#cure-disease){.compatibility} ### {#cure-disease-2024 -}

* Removed the disposable MC and added an AC to touch the recipient.

### [Create Poison](/Realms/year/2024/Omnibus.html#create-poison){.compatibility} ### {#create-poison-2024 -}

* Clarified that only Sleep and Truth poisons can be resisted by Protect the Soul.

### [Familiar](/Realms/year/2024/Omnibus.html#familiar){.compatibility} ### {#familiar-2024 -}

* Can now use the same MC as Animal Companion.

### [Gentle Repose](/Realms/year/2024/Omnibus.html#gentle-repose){.compatibility} ### {#gentle-repose-2024 -}

* Added the spell Gentle Repose, a first circle spell that protects the target dead body temporarily as per the spell Protect the Soul until the target is returned to life.

### [Immunity to Poison](/Realms/year/2024/Omnibus.html#immunity-to-poison){.compatibility} ### {#immunity-to-poison-2024 -}

* Removed the disposable MC and related spell text and added an AC to touch the recipient.

### [Magic Missile](/Realms/year/2024/Omnibus.html#magic-missile){.compatibility} ### {#magic-missile-2024 -}

* Increases the Magic Missile props to 3 rather than 2.

### [Merge/Transmute Self](/Realms/year/2024/Omnibus.html#merge){.compatibility} ### {#merge-2024 -}

* Changed the name of the spell “Transmute Self” to “Merge”.
* Moved Merge from 4th circle to 3rd.
* The spellcaster no longer needs to have their eyes closed or remain “perfectly” still.
* The spellcaster is able to Merge with trees, stone, and earth, choosing one each time they cast the spell.

### [Mystic Forge](/Realms/year/2024/Omnibus.html#mystic-forge){.compatibility} ### {#mystic-forge-2024 -}

* Increased to 2 uses per learning.

### [Prophecy](/Realms/year/2024/Omnibus.html#prophecy){.compatibility} ### {#prophecy-2024 -}

* Added that players may ask a follow up question every two hours to the EH or MM related to the initial question.

### [Protection from Missile](/Realms/year/2024/Omnibus.html#protection-from-missile){.compatibility} ### {#protection-from-missile-2024 -}

* AC and related spell text changed to "Kneel with no weapons or shields in hand, or lie on back".

### [Purity/Purity to Disease/Purity to Poison](/Realms/year/2024/Omnibus.html#purity){.compatibility} ### {#purity-2024 -}

* AC changed to "Kneel with no weapons or shields in hand, or lie on back".
* Combined Purity to Disease and Purity to Poison into one spell. The spellcaster may choose persistent immunity to either disease or poison when casting the spell.

### [Reanimate Limb](/Realms/year/2024/Omnibus.html#reanimate-limb){.compatibility} ### {#reanimate-limb-2024 -}

* Reintroduced the spell Reanimate Limb.

### [Repair Armor](/Realms/year/2024/Omnibus.html#repair-armor){.compatibility} ### {#repair-armor-2024 -}

* Removed the disposable MC related text within the spell.

### [Resist Death](/Realms/year/2024/Omnibus.html#resist-death){.compatibility} ### {#resist-death-2024 -}

* AC changed to "Kneel with no weapons or shields in hands, or lie on back".

### [Seed of Life](/Realms/year/2024/Omnibus.html#seed-of-life){.compatibility} ### {#seed-of-life-2024 -}

* Clarifies that the spell fails if the target is soulless.

### [Strange Brew](/Realms/year/2024/Omnibus.html#strange-brew){.compatibility} ### {#strange-brew-2024 -}

* Added 3 more options to Strange Brew. 
  * Poison Potion is two points and creates a Death, Sleep, or Truth Poison as per Create poison.
  * Weapon Oil is four points and when applied to a weapon for 60 seconds with a spell sash, the weapon swings Silver or Magic.
  * Potion of Resistance is five points and allows the user to gain one call of Resist Magic as per the spell.

### [Transformation](/Realms/year/2024/Omnibus.html#transformation){.compatibility} ### {#transformation-2024 -}

* Allows for kneeling while regenerating armor.
* Increased uses from 2 to 3.

### [Transmute Weapon](/Realms/year/2024/Omnibus.html#transmute-weapon){.compatibility} ### {#transmute-weapon-2024 -}

* Added the new Transmute Weapon spell, a first circle spell that gives the spellcaster the ability to modify the shape and properties of a melee weapon temporarily. The spellcaster can choose from “Axe”, “Mace”, “Hammer”, “Spear”, or “Dagger”. The next three swings with this weapon use the specified call, whether or not there was a successful hit.

## Administrative ## {#administrative-2024 -}

* Added the Facebook Moderation Team into sections [9.1](/Realms/year/2024/Omnibus.html#list-of-positions){.compatibility} and [9.7](/Realms/year/2024/Omnibus.html#position-descriptions){.compatibility} with the other elected positions and removed the related expiring Standing Policy.
* Removed the reference to “Realms of Wonder” from [What is the Realms?](/Realms/year/2024/Omnibus.html#what-is-the-realms){.compatibility}.
* Updated the year to “2024” in all relevant locations. 
* Added the new [Standing Policy](/Realms/year/2024/StandingPolicies.html#make-the-ehc-virtual-going-forward){.compatibility} (ongoing) encouraging that the Event Holders Council meeting has a hybrid virtual option going forward.
* Added the new [Standing Policy](/Realms/year/2024/StandingPolicies.html#proposals-require-community-support-for-players-meeting-discussion){.compatibility} (expiring next year) that proposals may only advance to the Players' Meeting with the registered support of at least 30 players eligible to vote at that Players' Meeting.
* Updated the [COVID-19 Standing Policy](/Realms/year/2024/StandingPolicies.html#for-events-and-attendees){.compatibility} to allow negative tests to permit event attendance after positive test results.
* Removed ongoing Standing Policies that did not get renewal approval. 
* Added a [Standing Policy](/Realms/year/2024/StandingPolicies.html#spellbook-conversion-2024){.compatibility} to reflect the 2024 Spellbook Conversion policy.


# Changes approved February 18, 2023, effective April 4, 2023 # {#changes-2023 -}

## Standing Polices ## {#standing-policies-2023 -}

* EHs must announce that the
  “[Hold](/Realms/year/2023/Omnibus.html#the-rules-we-play-by){.compatibility}” call has changed
  (as noted in the [Safety & Marshaling](#safety-and-marshaling-2023){.compatibility} category
  below).
* The pandemic Standing Policies were updated as follows:
  * The negative COVID-19 test requirement was updated to the day of arrival on
    site for event attendees not presenting evidence of current vaccination.
  * The positive COVID-19 test disqualification has been changed from a positive
    test in the 10 days before the event to a positive test in the 5 days before
    the event. 
  * Events are once again required to have 30 attendees to be considered legal.
  * EHC attendance and voting rules have reverted to pre-pandemic policy.
  * Magic items backed as of 2022 are still backed. EHs no longer get to carry
    forward their magic items without holding events, and items will begin to
    fall off of the list starting at the EHC in 2025.

## Safety & Marshaling ## {#safety-and-marshaling-2023 -}

* The call for a [hold](/Realms/year/2023/Omnibus.html#the-rules-we-play-by){.compatibility} is
  now the word “Hold” three times, rather than a single, drawn-out “Hoooold”.
  Additionally, if you see someone making the time-out sign (like a capital T)
  above their head, that’s a non-verbal way to call a hold, and you should shout
  “Hold” three times as if you heard a hold called.
* Anyone may now use a “[Torch](/Realms/year/2023/Omnibus.html#torches){.compatibility}”,
  represented by an active red chemical light stick “or equivalent”. It is a
  one-handed item while being used as a light source. Torches may be disrupted
  by [Disrupt Light](/Realms/year/2023/Omnibus.html#disrupt-light){.compatibility}.
* You are no longer required to carry around a soul token. When a body is
  destroyed, it is still the responsibility of the player who destroyed the body
  to inform a marshal of this.
* The call of “armor-piercing” was renamed to
  “[piercing](/Realms/year/2023/Omnibus.html#piercing){.compatibility}”, so that combat calls
  can better keep up with the speed of combat.

## Combat ## {#combat-2023 -}

* Armor may now be made of any materials, so long as it takes the appearance of
  the type of armor one is trying to call, such as quilting or light leather for
  light armor. Anyone may wear the garb that appears to be armor, but the PC
  will only be able to call armor based on their restrictions. This was a
  standing policy in 2022.
* For one paths, the previous Single Weapon or Shield maximum was 4’6”. It is
  now 5’ maximum.
* For one paths, the previous Florentine, Weapon & Shield maximum was 5’. It is
  now 5’2” maximum.
* Technically, fighters are now limited to shields up to 8’ long. However, the
  real limit is still what can be safely wielded, as 8’ is almost certainly far
  beyond that limit.

## Magic ## {#magic-2023 -}

* Previously, spellcasters could only give themselves one spell signature from
  their spell mastery per event. Now, they can give themselves one spell
  signature per spell if it is in their spell mastery.
* Previously, spells would have to be unlearned and relearned in order to switch
  their path and circle. A player may now change the path and circle where
  spells sit in their spellbook as a part of replacing their spellbook, so long
  as the spell build remains legal. Regional and alchemy can also move between
  paths, but the circle must remain the same. You may do this at the beginning
  of an event by showing your replaced spellbook to the magic marshall with the
  desired change(s).
* [Regional and Alchemy](/Realms/year/2023/Omnibus.html#alternatives-to-spells){.compatibility}
  are now explicitly allowed within your spell mastery, which is probably how
  most people were already playing.
* Certain spells can be ‘pre-cast’ before the start of the event, with
  corresponding rules explained in the new
  "[Precast](/Realms/year/2023/Omnibus.html#precast){.compatibility}" caveat. Precasting uses up
  one use of the spell. Components must still be present, and this can only be
  done by the spellcaster to the spellcaster. The spells with this new caveat
  are:
  
  ::: {.columns-list}
  * [Armored Cloak](/Realms/year/2023/Omnibus.html#armored-cloak){.compatibility}
  * [Assassin's Blade](/Realms/year/2023/Omnibus.html#assassins-blade){.compatibility}
  * [Aura of Protection](/Realms/year/2023/Omnibus.html#aura-of-protection){.compatibility}
  * [Death Watch](/Realms/year/2023/Omnibus.html#death-watch){.compatibility}
  * [Deep Pockets](/Realms/year/2023/Omnibus.html#deep-pockets){.compatibility}
  * [Embrace Death](/Realms/year/2023/Omnibus.html#embrace-death){.compatibility}
  * [Enchant Armor](/Realms/year/2023/Omnibus.html#enchant-armor){.compatibility}
  * [Heartiness](/Realms/year/2023/Omnibus.html#heartiness){.compatibility}
  * [Masterwork Hammer](/Realms/year/2023/Omnibus.html#masterwork-hammer){.compatibility}
  * [Protection from Boulder](/Realms/year/2023/Omnibus.html#protection-from-boulder){.compatibility}
  * [Protect the Soul](/Realms/year/2023/Omnibus.html#protect-the-soul){.compatibility}
  * [Protection from Missile](/Realms/year/2023/Omnibus.html#protection-from-missile){.compatibility}
  * [Purity to Disease](/Realms/year/2023/Omnibus.html#purity-to-disease){.compatibility}
  * [Purity To Poison](/Realms/year/2023/Omnibus.html#purity-to-poison){.compatibility}
  * [Regenerate the Soul](/Realms/year/2023/Omnibus.html#regenerate-the-soul){.compatibility}
  * [Regeneration](/Realms/year/2023/Omnibus.html#regeneration-spell){.compatibility}
  * [Resist Death](/Realms/year/2023/Omnibus.html#resist-death){.compatibility}
  * [Resist Magic](/Realms/year/2023/Omnibus.html#resist-magic){.compatibility}
  * [Transformation](/Realms/year/2023/Omnibus.html#transformation){.compatibility}
  :::

* Unified the restrictions for what commands undead can disobey. A disobeyed
  order is simply ignored now.
* Reanimating an undead no longer requires a new MC if the target already has an
  appropriate one.
* Being animated as undead no longer causes a character to remember what happens
  while dead.

## Spells ## {#spells-2023 -}

### [Animate Undead](/Realms/year/2023/Omnibus.html#animate-undead){.compatibility} ### {#animate-undead-2023 -}

* Animate Undead works to reanimate an Undead General.

### [Animate Undead General](/Realms/year/2023/Omnibus.html#animate-undead-general){.compatibility} ### {#animate-undead-general-2023 -}

* Animate Undead works to reanimate an Undead General.
* Closed a loophole that allowed [Embrace
  Death](/Realms/year/2023/Omnibus.html#embrace-death) and Animate Undead
  General to both be active on one person at the same time. This was a mistake
  from the original wording of Animate Undead General.

### [Assassin's Blade](/Realms/year/2023/Omnibus.html#assassins-blade){.compatibility} ### {#assassins-blade-2023 -}

* Assassin’s Blade props no longer need to be Event-Stealable.

### [Circle of Healing](/Realms/year/2023/Omnibus.html#circle-of-healing){.compatibility} ### {#circle-of-healing-2023 -}

* Clarified that the spellcaster does not need to have the named spells in their
  spellbook.

### [Circle of Protection](/Realms/year/2023/Omnibus.html#circle-of-protection){.compatibility} ### {#circle-of-protection-2023 -}

* Made explicit that no magic items can pass over a Circle of Protection, even
  if carried by the spellcaster.

### [Dream](/Realms/year/2023/Omnibus.html#dream){.compatibility} ### {#dream-2023 -}

* Dream, a new spell, is similar to a single-use
  [Precognition](/Realms/year/2023/Omnibus.html#precognition){.compatibility}. Precognition can
  no longer be pre-registered in order to receive information at the start of an
  event, but Dream can. Dream had been removed when Precognition was added, but
  the flavor went away and people wanted Dream back.

### [Embrace Death](/Realms/year/2023/Omnibus.html#embrace-death){.compatibility} ### {#embrace-death-2023 -}

* The appearance of Embrace Death’s MC can now be changed between events.
* If a spellcaster has their Embrace Death MC on their person and the spell is
  still in effect at the end of the event, they are returned to life, so that
  they do not get a tick for ending the event soulless.
* If the body of someone under the effects of Embrace Death is destroyed, they
  may be reanimated.
* Closed a loophole that allowed Embrace Death and [Animate Undead
  General](/Realms/year/2023/Omnibus.html#animate-undead-general) to both be
  active on one person at the same time. This was a mistake from the original
  wording of Animate Undead General.
* Embraced characters can now cross their own [Circles of
  Protection](/Realms/year/2023/Omnibus.html#circle-of-protection).

### [Enchant Weapon](/Realms/year/2023/Omnibus.html#enchant-weapon){.compatibility} ### {#enchant-weapon-2023 -}

* Removes arrows from the Enchant Weapon spell. Previously, an enchanted arrow
  would have the special call once, but an enchanted bow could use the call
  three times. “Weapons” in the description has been clarified to “melee
  weapons”, to make it clear that the spell cannot be used with javelins.

### [Light](/Realms/year/2023/Omnibus.html#light){.compatibility} ### {#light-2023 -}

* The Light spell may no longer use red light props, since Light props can be
  active without being wielded and there was concern about people with
  [torches](/Realms/year/2023/Omnibus.html#torches){.compatibility} being marshalable.

### [Masterwork Hammer](/Realms/year/2023/Omnibus.html#masterwork-hammer){.compatibility} ### {#masterwork-hammer-2023 -}

* Masterwork Hammer props no longer need to be Event-Stealable.

### [Mentor](/Realms/year/2023/Omnibus.html#mentor){.compatibility} ### {#mentor-2023 -}

* Mentor now has no verbal component or use limit.

### [Mystic Forge](/Realms/year/2023/Omnibus.html#mystic-forge){.compatibility} ### {#mystic-forge-2023 -}

* Clarified that the spellcaster does not need to have the named spells in their
  spellbook.

### [Precognition](/Realms/year/2023/Omnibus.html#precognition){.compatibility} ### {#precognition-2023 -}

* Precognition may no longer be pre-registered in order to receive information
  at the start of an event. Instead, a new spell,
  [Dream](/Realms/year/2023/Omnibus.html#dream){.compatibility}, takes over this effect.

### [Second Chance](/Realms/year/2023/Omnibus.html#second-chance){.compatibility} ### {#second-chance-2023 -}

* Second Chance no longer has a material component. Rather than giving a token
  with their name on it to the EH or MM, spellcasters now only must speak to the
  EH or MM.

## Administrative ## {#administrative-2023 -}

:::{.new}
* Previously, new or altered text in the Omnibus and Standing Policies was
  underlined or colored red, by tradition. This did not communicate any changes
  made by removing text. Now, new or altered text is not formatted differently.
  Instead, there is a changelog summarizing the changes to the rules written by
  the Omnibus Editorial Council. That would be [this document](#) you’re reading
  right now!
:::